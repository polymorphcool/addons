extends Spatial

func _process(delta):
	$plane.translation.z = $cam.unit_distance
	$plane.scale = Vector3( $cam.scan_ratio, 1, 1 )
	$plane.material_override.uv1_scale = Vector3( $cam.scan_ratio, 1, 1 )
	print( $plane.translation.z )
