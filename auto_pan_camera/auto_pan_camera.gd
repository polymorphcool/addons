extends Camera

export(Vector2) var scan_ratio_div:Vector2 = Vector2(16,9) setget set_scan_ratio_div

onready var scan_ratio:float = scan_ratio_div.x/scan_ratio_div.y
var unit_distance:float = 1

export(float) var scan_x_speed:float = 20
export(float) var scan_y_speed:float = 20

var scan_x_range:float = 0
var scan_y_range:float = 0
var scan_x_accum:float = 0
var scan_y_accum:float = 0

func set_scan_ratio_div(v2:Vector2) -> void:
	scan_ratio_div = v2
	scan_ratio = scan_ratio_div.x/scan_ratio_div.y

func detect_offscreen() -> void:
	var fov_rad:float = fov * TAU / 360
	var h_fov:float = -1
	var v_fov:float = -1
	var vps:Vector2 = get_viewport().size
	var screen_ratio:float = vps.x/vps.y
	var screen_width:float = 0
	var screen_height:float = 0
	var scan_width:float = -1
	var scan_height:float = -1
	# https://en.wikipedia.org/wiki/Field_of_view_in_video_games#Field_of_view_scaling_methods
	if keep_aspect == Camera.KEEP_WIDTH:
		# horizontal fov, Vert- scaling > expand vertically
		screen_width = tan(fov_rad*0.5)
		screen_height = screen_width/screen_ratio
		h_fov = fov_rad
		v_fov = atan(screen_height)*2
	else:
		# vertical fov, Hor+ scaling > expand horizontally
		screen_height = tan(fov_rad*0.5)
		screen_width = screen_height * screen_ratio
		h_fov = atan(screen_width)*2
		v_fov = fov_rad
	# depending on the fov scaling method, computation of width OR height
	if keep_aspect == Camera.KEEP_WIDTH:
		unit_distance = -0.5 / screen_height
		scan_width = screen_height*scan_ratio
		scan_height = -1
	else:
		unit_distance = -0.5 * scan_ratio / screen_width
		scan_width = -1
		scan_height = screen_width/scan_ratio
	# computing range on X
	if scan_height <= 0:
		scan_x_range = 0
	else:
		scan_x_range = (atan(scan_height)*2 - v_fov) * 0.5
	# computing range on Y
	if scan_width <= 0:
		scan_y_range = 0
	else:
		scan_y_range = (atan(scan_width)*2 - h_fov) * 0.5

func _ready():
	get_viewport().connect( "size_changed", self, "detect_offscreen" )
	detect_offscreen()

func _process(delta):
	scan_x_accum += delta * scan_x_speed * TAU / 360
	scan_y_accum += delta * scan_y_speed * TAU / 360
	if scan_x_range > 0:
		rotation.x = scan_x_range * sin( scan_x_accum )
	else:
		rotation.x = 0
	if scan_y_range > 0:
		rotation.y = scan_y_range * sin( scan_y_accum )
	else:
		rotation.y = 0
