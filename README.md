# polymorph tools for godot & pozzo

set of tools (mainly gdscript running in the editor) for godot engne and pozzo engine

## description

to use any of the addon, go to the folder and open the `.tscn` with the same name as folder

### addons/polymorph/animation_baker

a blendshape list to textures baker, used in [boids](https://gitlab.com/polymorphcool/godot_module_boids) module to animate particles

quite complex to configure, documentation will arrive in the folder

### addons/polymorph/shader_vault

bits and pieces of shaders

### addons/polymorph/texture_generator

generation of textures

- **gradient.tscn** : high-definition 2D gradients with a reader gradient to sample the texture
- **noise.tscn** : precise random texture based on primes
- **text_panel.tscn** : images with text

## installation

**if you don't have a 'addons' folder yet**

create one:

```
mkdir addons
```

**if you already have a 'addons' folder**

```
cd addons
git clone git@gitlab.com:polymorphcool/addons.git polymorph
```
