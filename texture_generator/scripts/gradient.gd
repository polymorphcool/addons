# references
# - how to generate random numbers with primes https://www.youtube.com/watch?v=SxP30euw3-0
# - lists of primes: https://primes.utm.edu/lists/

tool

extends Sprite

const target:String = "res://addons/polymorph/texture_generator/export/"

enum eformat {
	_8bits,
	_float
}

export(eformat) var format:int = eformat._8bits
export(float,1,1000) var maximum:float = 1
export(int,1,12) var size:int = 9

export var texture_mipmaps:bool = false
export var texture_repeat:bool = true
export var texture_filter:bool = false
export var texture_anisotropic:bool = false
export var texture_mirror:bool = false

export var generate:bool = false setget set_generate
export var clear:bool = false setget set_clear

func set_generate(b:bool) -> void:
	generate = false
	if b:
		initialised = false

func set_clear(b:bool) -> void:
	clear = false
	if b:
		while get_child_count() > 1:
			remove_child( get_child( get_child_count()-1 ) )
		texture = null
		$reader.texture = null
		editor_description = ''

var initialised: = true

func _process(delta):
	
	if not initialised:
		
		initialised = true
		set_clear(true)
		
		var tex_size = pow(2,size)
		
		# selecting the right image format
		var imf:int = -1
		var texture_path:String  = target+'gradient_' + str(OS.get_unix_time())+'_'+str(delta)+'_'
		
		match format:
			eformat._8bits:
				imf = Image.FORMAT_R8
				texture_path += "R8"
			eformat._float:
				imf = Image.FORMAT_RF
				texture_path += "RF"
		texture_path += '.res'
		
		if imf == -1:
			texture = null
			$reader.texture = null
			return
		
		# generation of a new image and writing data
		var im:Image = Image.new()
		im.create(tex_size,tex_size,false,imf)
		im.lock()
		var pnum:int = tex_size*tex_size
		for y in range(0,tex_size):
			for x in range(0,tex_size):
				var id:int = x + y * tex_size
				var v:float = (id*maximum)/pnum
				im.set_pixel(x,y,Color( v, 0.0, 0.0, 1.0 ))
		im.unlock()
		# generation of a new texture
		var it:ImageTexture = ImageTexture.new()
		# flags
		var itf:int = 0
		if texture_mipmaps:
			itf |= Texture.FLAG_MIPMAPS
		if texture_repeat:
			itf |= Texture.FLAG_REPEAT
		if texture_filter:
			itf |= Texture.FLAG_FILTER
		if texture_anisotropic:
			itf |= Texture.FLAG_ANISOTROPIC_FILTER
		if texture_mirror:
			itf |= Texture.FLAG_MIRRORED_REPEAT
		it.create_from_image( im, itf )
		
		# info
		var info:String = ''
		info += 'size: '+str(tex_size)+'x'+str(tex_size)+'\n'
		info += 'output:\n'
		info += '\t' + texture_path
		editor_description = info
		
		# saving output
		var d:Directory = Directory.new()
		d.make_dir_recursive(target)
		ResourceSaver.save(texture_path, it, ResourceSaver.FLAG_CHANGE_PATH|ResourceSaver.FLAG_REPLACE_SUBRESOURCE_PATHS)
		var res:Resource = load(texture_path)
		
		# display output
		texture = res
		$reader.texture = res
		var ts:Vector2 = res.get_size()
		$reader.position = ts * Vector2.RIGHT + Vector2(32,0)
		$reader.material.set_shader_param( "texture_size", res.get_size() )
		if ts.x > 256 or ts.y > 256:
			$reader.scale = Vector2(.5,.5)
		else:
			var sc:Vector2 = Vector2.ONE
			while sc.x < 256 or sc.y < 256:
				sc *= 2
			$reader.scale = sc / ts
