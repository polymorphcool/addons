# references
# - how to generate random numbers with primes https://www.youtube.com/watch?v=SxP30euw3-0
# - lists of primes: https://primes.utm.edu/lists/

tool

extends Sprite

const target:String = "res://addons/polymorph/texture_generator/export/"

enum etype {
	PRIMES,
	#BLUE_NOISE
}

enum echannels {
	R,
	RG,
	RGB,
	RGBA
}

enum eformat {
	_8bits,
	_float
}

enum dchannel {
	ALL,
	RED,
	GREEN,
	BLUE,
	ALPHA
}

export(etype) var type:int = etype.PRIMES
export(int,0,6) var prime_set:int = 0
export(echannels) var channels:int = echannels.RGB
export(eformat) var format:int = eformat._8bits
export(int,1,10) var size:int = 9
export var _seed:int = 2453817

export var texture_mipmaps:bool = false
export var texture_repeat:bool = true
export var texture_filter:bool = false
export var texture_anisotropic:bool = false
export var texture_mirror:bool = false

export var export_png:bool = false

export var generate:bool = false setget set_generate
export var clear:bool = false setget set_clear
# enable to test computation inconsistancies too big primes
#export var test:bool = false setget set_test

export(dchannel) var display_channel = 0 setget set_display_channel

var prime_sets:Array = [
	[104393791, 86028997],
	[104391853, 86029751],
	[104388127, 86030683],
	[104392549, 86033993],
	[104390137, 86034593],
	[104383879, 86150159],
	[104392501, 86150159]
]

# closest prime for resolution
var modulos:Array = [
	5, 			# pow(2,1) -> 2x2: 4 pixels
	17, 		# pow(2,2) -> 4x4: 16 pixels
	257, 		# pow(2,3) -> 8x8: 64 pixels
	257, 		# pow(2,4) -> 16x16: 256 pixels
	1031, 		# pow(2,5) -> 32x32: 1024 pixels
	4099, 		# pow(2,6) -> 64x64: 4096 pixels
	16411, 		# pow(2,7) -> 128x128: 16384 pixels
	65537, 		# pow(2,8) -> 256x256: 65536 pixels
	262147, 	# pow(2,9) -> 512x512: 262144 pixels
	1048583, 	# pow(2,10) -> 1024x1024: 1048576 pixels
]

var initialised: = true

func set_test(b:bool) -> void:
	if b:	
		var i:int = 0
		var f:float = 0
		print('\ntesting sets')
		for ps in prime_sets:
			print('############ ', ps)
			i = int(ps[0])*int(ps[1])
			f = float(ps[0])*float(ps[1])
			print('int:\t', i)
			print('float:\t', f)
			print('diff:\t', int(f)-i)
			print('diff:\t', f-float(i))

func set_generate(b:bool) -> void:
	generate = false
	if b:
		initialised = false

func set_clear(b:bool) -> void:
	clear = false
	if b:
		while get_child_count() > 1:
			remove_child( get_child( get_child_count()-1 ) )
		texture = null
		editor_description = ''

func set_display_channel(i:int) -> void:
	display_channel = i
	if is_inside_tree():
		material.set_shader_param("channel", display_channel-1)

func compute_random( num:int, start:int ) -> Array:
	var out:Array = []
	out.resize(num)
	var a:int = prime_sets[prime_set][0]
	var b:int = prime_sets[prime_set][1]
	var m:int = modulos[size-1]
	start = start%m
	for i in range(0,num):
		out[i] = float(start)/(m-1)
		start = abs((a*start + b)%m)
	return out

func _process(delta):
	
	if not initialised:
		
		initialised = true
		set_clear(true)
		
		var text_size = pow(2,size)
		
		# selecting the right image format
		var imf:int = -1
		var channel_num:int = -1
		var d:Directory = Directory.new()
		d.make_dir_recursive(target)
		var texture_path:String  = target+'noise_' + str(OS.get_unix_time())+'_'+str(_seed)+'_'
		
		match format:
			eformat._8bits:
				match channels:
					echannels.R:
						imf = Image.FORMAT_R8
						texture_path += "R8"
						channel_num = 1
					echannels.RG:
						imf = Image.FORMAT_RG8
						texture_path += "RG8"
						channel_num = 2
					echannels.RGB:
						imf = Image.FORMAT_RGB8
						texture_path += "RGB8"
						channel_num = 3
					echannels.RGBA:
						imf = Image.FORMAT_RGBA8
						texture_path += "RGBA8"
						channel_num = 4
			eformat._float:
				match channels:
					echannels.R:
						imf = Image.FORMAT_RF
						texture_path += "RF"
						channel_num = 1
					echannels.RG:
						imf = Image.FORMAT_RGF
						texture_path += "RGF"
						channel_num = 2
					echannels.RGB:
						imf = Image.FORMAT_RGBF
						texture_path += "RGBF"
						channel_num = 3
					echannels.RGBA:
						imf = Image.FORMAT_RGBAF
						texture_path += "RGBAF"
						channel_num = 4
		texture_path += '.res'
		
		if imf == -1 or channel_num == -1:
			texture = null
			return
		
		var data = []
		for i in range(0,channel_num):
			data.append( compute_random( text_size*text_size, _seed+i ) )
		
		# generation of a new image and writing data
		var im:Image = Image.new()
		im.create(text_size,text_size,false,imf)
		im.lock()
		for y in range(0,text_size):
			for x in range(0,text_size):
				var id:int = x + y * text_size
				var c:Array = [0,0,0,0]
				for i in range(0,channel_num):
					c[i] = data[i][id]
				im.set_pixel(x,y,Color(c[0],c[1],c[2],c[3]))
		im.unlock()
		# generation of a new texture
		var it:ImageTexture = ImageTexture.new()
		# flags
		var itf:int = 0
		if texture_mipmaps:
			itf |= Texture.FLAG_MIPMAPS
		if texture_repeat:
			itf |= Texture.FLAG_REPEAT
		if texture_filter:
			itf |= Texture.FLAG_FILTER
		if texture_anisotropic:
			itf |= Texture.FLAG_ANISOTROPIC_FILTER
		if texture_mirror:
			itf |= Texture.FLAG_MIRRORED_REPEAT
		it.create_from_image( im, itf )
		
		# info
		var info:String = ''
		info += 'texture:\n'
		info += '\t- width: ' + str(pow(2,size)) + '\n'
		info += '\t- pixels: ' + str(pow(pow(2,size),2)) + '\n'
		info += 'primes used:\n'
		info += '\t- a: ' + str(prime_sets[prime_set][0]) + '\n'
		info += '\t- b: ' + str(prime_sets[prime_set][1]) + '\n'
		info += '\t- m: ' + str(modulos[size-1]) + '\n'
		info += 'output:\n'
		info += '\t' + texture_path
		editor_description = info
		
		# saving output
		if export_png:
			var png_path = texture_path.replace('.res','.png')
			im.save_png( png_path )
		
		ResourceSaver.save(texture_path, it, ResourceSaver.FLAG_CHANGE_PATH|ResourceSaver.FLAG_REPLACE_SUBRESOURCE_PATHS)
		var res:Resource = load(texture_path)
		
		# display output
		texture = res
		set_display_channel( dchannel.ALL )
