tool

extends Node2D

const target:String = "res://addons/polymorph/texture_generator/export/"

export var color0:Color
export var color1:Color
export var color2:Color

export var text:String = 'label' setget set_text
export var render:bool = false setget set_render
export var clear:bool = false setget set_clear

var do_process:bool = false

func set_text(s:String) -> void:
	text = s
	if is_inside_tree():
		$vpc/vp/bg/cntr/lbl.text = text

func set_render(b:bool) -> void:
	render = false
	if b:
		do_process = true

func set_clear(b:bool) -> void:
	clear = false
	if b:
		$render.texture = null
		editor_description = ''

func _process(delta):
	if do_process:
		var ipath:String = target + $vpc/vp/bg/cntr/lbl.text+'.res'
		ipath = ipath.replace(' ', '_')
		var tex:ViewportTexture = $vpc/vp.get_texture()
		var i:Image = tex.get_data()
		i.flip_y()
		var it:ImageTexture = ImageTexture.new()
		it.create_from_image( i, Texture.FLAGS_DEFAULT|Texture.FLAG_ANISOTROPIC_FILTER)
		var d:Directory = Directory.new()
		d.make_dir_recursive(target)
		ResourceSaver.save( ipath, it, ResourceSaver.FLAG_CHANGE_PATH|ResourceSaver.FLAG_REPLACE_SUBRESOURCE_PATHS)
		$render.texture = load(ipath)
		do_process = false
