# references
# - how to generate random numbers with primes https://www.youtube.com/watch?v=SxP30euw3-0
# - lists of primes: https://primes.utm.edu/lists/

tool

extends Sprite

const primes_path:String = "res://addons/polymorph/texture_generator/assets/primes.json"
const target:String = "res://addons/polymorph/texture_generator/export/"

export(int,0,6) var prime_set:int = 0
export(int,1,10) var size:int = 9
export(int,2,10000) var value_range:int = 100
export(int,1,10000) var repeat_min:int = 5
export(int,1,10000) var repeat_max:int = 20
export var _seed:int = 0

export var texture_mipmaps:bool = false
export var texture_repeat:bool = true
export var texture_filter:bool = false
export var texture_anisotropic:bool = false
export var texture_mirror:bool = false

export var export_png:bool = false

export var generate:bool = false setget set_generate
export var clear:bool = false setget set_clear
export var dev:bool = false setget set_dev

var prime_sets:Array = [
	[104393791, 86028997],
	[104391853, 86029751],
	[104388127, 86030683],
	[104392549, 86033993],
	[104390137, 86034593],
	[104383879, 86150159],
	[104392501, 86150159]
]

# closest prime for resolution
var primes:Array = []

var initialised: = true

func set_dev(b:bool) -> void:
	
	if b:
		
		load_primes()
		
		var max_value:int = 100
		var pixel_count:int = 10000
		var per_value_count:int = ceil(pixel_count/max_value)
		
		# generation of random for repetition
		var rs:Array = compute_random( max_value, OS.get_ticks_msec() )
		var repeats:Array = []
		var total = 0
		var ri:int = 0
		var missing:int = 0
		while total < per_value_count:
			var r:int = repeat_min + round(rs[ri] * (repeat_max-repeat_min))
			ri += 1
			total += r
			if total > per_value_count:
				r -= total - per_value_count
				total = per_value_count
			# if the last value is smaller than repeat_min,
			# we'll have to fix the list in an extra pass
			if r >= repeat_min:
				repeats.append(r)
			else:
				missing = r
		
		print( '\ndev' )
		
		if missing > 0:
			print( 'missing: ', missing )
			# diffusion of the error accross the set
			var i = randi()
			while missing > 0:
				i %= repeats.size()
				if repeats[i] < repeat_max:
					repeats[i] += 1
					missing -= 1
					print( 'after: ', repeats )
				i = randi()
			total = 0
			for r in repeats:
				total += r
		
		print( 'per_value_count: ', per_value_count )
		print( 'repeat_min: ', repeat_min )
		print( 'repeat_max: ', repeat_max )
		print( 'total: ', total )
		print( 'repeats: ', repeats )

func set_generate(b:bool) -> void:
	generate = false
	if b:
		initialised = false

func set_clear(b:bool) -> void:
	clear = false
	if b:
		while get_child_count() > 1:
			remove_child( get_child( get_child_count()-1 ) )
		texture = null
		$reader.texture = null
		editor_description = ''

func load_primes() -> void:
	if not primes.empty():
		return
	var f:File = File.new()
	if f.open( primes_path, File.READ ) == OK:
		var res:JSONParseResult = JSON.parse(f.get_as_text())
		if res.error == OK:
			primes = res.result

func get_closest_prime( value:int ) -> int:
	var out:int = -1
	for i in range( 0, primes.size() ):
		if primes[i] > value:
			out = primes[i]
			break
	return out

func compute_repeats( count:int, start:int ) -> Array:
	
	var rs:Array = compute_random( count, start )
	if rs.empty():
		return []
	var repeats:Array = []
	var total = 0
	var ri:int = 0
	var missing:int = 0
	while total < count:
		var r:int = repeat_min + round( rs[ri] * (repeat_max-repeat_min) )
		ri += 1
		total += r
		if total > count:
			r -= total - count
			total = count
		# if the last value is smaller than repeat_min,
		# we'll have to fix the list in an extra pass
		if r >= repeat_min:
			repeats.append(r)
		else:
			missing = r
	if missing > 0:
		# diffusion of the error accross the set
		var i = randi()
		while missing > 0:
			i %= repeats.size()
			if repeats[i] < repeat_max:
				repeats[i] += 1
				missing -= 1
			i = randi()
		total = 0
		for r in repeats:
			total += r
	return repeats

func compute_random( num:int, start:int ) -> Array:
	var out:Array = []
	out.resize(num)
	var a:int = prime_sets[prime_set][0]
	var b:int = prime_sets[prime_set][1]
	var m:int = get_closest_prime( num )
	if m == -1:
		printerr( 'not enough primes to reach ', num )
		return []
	start = start%m
	for i in range(0,num):
		out[i] = float(start)/(m-1)
		start = abs((a*start + b)%m)
	return out

func _process(delta):
	
	if not initialised:
		
		initialised = true
		
		load_primes()
		set_clear(true)
		
		if repeat_min >= repeat_max:
			printerr( "minimum repeat MUST be smaller than maximum" )
			return
		
		var text_size:int = pow(2,size)
		var pixel_count:int = pow(text_size,2)
		
		# selecting the right image format
		var imf:int = Image.FORMAT_RF
		var channel_num:int = 1
		
		var d:Directory = Directory.new()
		d.make_dir_recursive(target)
		var texture_path:String  = target+'random_repeat_' + str(OS.get_unix_time())+'_'+str(_seed)
		texture_path += '.res'
		
		# first of all, let's generate a serie of repeat count for each value
		# we store this in a huge array of pairs: 
		# - first is the value, 
		# - second is the number of time it must be repeated
		# the total exceeds the available number of pixels
		var pairs:Array = []
		var repeat_total:int = 0
		var count_per_value:int = ceil( pixel_count / value_range )
		for i in range( 0, value_range ):
			var rs:Array = compute_repeats( count_per_value, _seed + i )
			if rs.empty():
				printerr( "error while computing repeats ", i )
				pairs = []
				return
			for r in rs:
				pairs.append( [i,r] )
				repeat_total += r
		
		# now we generates a serie of random to shuffle pairs 
		var pair_count:int = pairs.size()
		var pair_index:Array = compute_random( pair_count, _seed + value_range )
		
		# and we can generate the image
		var im:Image = Image.new()
		im.create(text_size,text_size,false,imf)
		im.lock()
		var do_continue:bool = true
		var pairi:int = 0
		var pixeli:int = 0
		while do_continue:
			
			var pairid:int = int( pair_index[pairi] * pair_count ) % pair_count
			var pair:Array = pairs[ pairid ]
			
			for i in range( 0, pair[1] ):
				if pixeli >= pixel_count:
					do_continue = false
					break
				var x:int = pixeli % text_size
				var y:int = pixeli / text_size
				im.set_pixel( x, y, Color( float(pair[0])/value_range,0,0,1) )
				pixeli += 1
			
			pairi += 1
			if pairi >= pairs.size():
				do_continue = false
				break
			
		im.unlock()

		# generation of a new texture
		var it:ImageTexture = ImageTexture.new()
		# flags
		var itf:int = 0
		if texture_mipmaps:
			itf |= Texture.FLAG_MIPMAPS
		if texture_repeat:
			itf |= Texture.FLAG_REPEAT
		if texture_filter:
			itf |= Texture.FLAG_FILTER
		if texture_anisotropic:
			itf |= Texture.FLAG_ANISOTROPIC_FILTER
		if texture_mirror:
			itf |= Texture.FLAG_MIRRORED_REPEAT
		it.create_from_image( im, itf )
		
		# saving output
		if export_png:
			var png_path = texture_path.replace('.res','.png')
			im.save_png( png_path )

		ResourceSaver.save(texture_path, it, ResourceSaver.FLAG_CHANGE_PATH|ResourceSaver.FLAG_REPLACE_SUBRESOURCE_PATHS)
		var res:Resource = load(texture_path)
		
		# display output
		texture = res
		$reader.position = Vector2( im.get_width()+32, 0 )
		$reader.texture = res
		$reader.material.set_shader_param( "texture_size", im.get_size() )
		
				# info
		var info:String = ''
		info += 'texture:\n'
		info += '\t- width: ' + str(pow(2,size)) + '\n'
		info += '\t- pixels: ' + str(pow(pow(2,size),2)) + '\n'
		info += 'primes used:\n'
		info += '\t- a: ' + str(prime_sets[prime_set][0]) + '\n'
		info += '\t- b: ' + str(prime_sets[prime_set][1]) + '\n'
		info += 'pairs:\n'
		info += '\t- ' + str(pairs.size()) + '\n'
		info += 'repeat per value:\n'
		info += '\t- ' + str(count_per_value) + '\n'
		info += 'output:\n'
		info += '\t' + texture_path
		editor_description = info
