shader_type canvas_item;
render_mode blend_mix;

const float M_PI = 3.1415926535897932384626433832795;

uniform float pixel_time = 1;
uniform vec2 texture_size = vec2(512);
uniform float transition = 0.5;
uniform float transition_variation = 0.5;

// util to hsv to rgb, hsv range: [0,1], with red at [0.,1.,1.]
// src: https://github.com/hughsk/glsl-hsv2rgb/blob/master/index.glsl
vec3 hsv2rgb(vec3 c) {
  vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
  vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
  return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

// extraction of values from a repeated random texture
void extract_random( 
	inout vec3 values, 
	in sampler2D tex, 
	in vec2 t_size, 
	in float r_transition, 
	in float r_variation, 
	in float global_t, 
	in float pixel_t 
	) {
	// texture pixel count
	int tw = int(t_size.x);
	int tt = tw * int(t_size.y);
	// local times
	float t0 = global_t / pixel_t;
	values.z = t0 - float( int(t0) );
	// pixel ids
	int pid0 = int(t0) % tt;
	int pid1 = int(t0+1.0) % tt;
	int pid2 = int(t0+t_size.x) % tt;
	// exctracting RED values out of texture
	values.x = texture( tex, vec2( float(pid0%tw), float(pid0/tw) ) / t_size).r;
	values.y = texture( tex, vec2( float(pid1%tw), float(pid1/tw) ) / t_size).r;
	float tv = 0.5 - texture( tex, vec2( float(pid2%tw), float(pid2/tw) ) / t_size).r;
	// mapping transition
	float rt = max( 0.0, min( 0.999,  								// avoiding division by 0!
		r_transition + tv * r_variation 
		) );
	values.z = 
		( sin( M_PI*-.5 + M_PI *									// sinus interpolation
		max( 0.0, min( 1.0, ( values.z - rt ) / ( 1.0 - rt ) ) )	// normalisaton between 0 and 1
		) + 1.0 ) * .5;
}

void fragment() {
	
	vec3 randoms;
	extract_random( randoms, TEXTURE, texture_size, transition, transition_variation, TIME, pixel_time );
	
	int tw = int(texture_size.x);
	int th = int(texture_size.y);
	int tt = tw * th;
	
	if (UV.y < 0.2) {
		// current and next value
		if (UV.x > 0.5) {
			COLOR = vec4( hsv2rgb(vec3(randoms.y,1.0,1.0)), 1.0 );
		} else {
			COLOR = vec4( hsv2rgb(vec3(randoms.x,1.0,1.0)), 1.0 );
		}
	} else if (UV.y < 0.205) {
		// spacer
		COLOR = vec4(vec3(0.0),1.0);
	} else if (UV.y < 0.415) {
		// curtain
		if ( UV.x < 1.0-randoms.z ) {
			COLOR = vec4( hsv2rgb(vec3(randoms.x,1.0,1.0)), 1.0 );
		} else {
			COLOR = vec4( hsv2rgb(vec3(randoms.y,1.0,1.0)), 1.0 );
		}
	} else if (UV.y < 0.42) {
		// spacer
		COLOR = vec4(vec3(0.0),1.0);
	} else {
		// mixed values
		COLOR = vec4( mix( hsv2rgb(vec3(randoms.x,1.0,1.0)), hsv2rgb(vec3(randoms.y,1.0,1.0)), randoms.z), 1.0 );
	}
	
}