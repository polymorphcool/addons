shader_type canvas_item;
render_mode blend_mix;

uniform int channel = -1;

void fragment() {
	
	if ( channel == 0 ) {
		COLOR = vec4(vec3(texture(TEXTURE,UV).r),1.0);
	} else if ( channel == 1 ) {
		COLOR = vec4(vec3(texture(TEXTURE,UV).g),1.0);
	} else if ( channel == 2 ) {
		COLOR = vec4(vec3(texture(TEXTURE,UV).b),1.0);
	} else if ( channel == 3 ) {
		COLOR = vec4(vec3(texture(TEXTURE,UV).a),1.0);
	} else {
		COLOR = texture(TEXTURE,UV);
	}
	
}