shader_type canvas_item;
render_mode blend_mix;

uniform float time_divider = 1000;
uniform vec2 texture_size = vec2(512);
uniform float pixel_offset : hint_range(0,5000);

void fragment() {
	
	int tw = int(texture_size.x);
	int th = int(texture_size.y);
	int tt = tw * th;
	
	float t = TIME / time_divider;
	if (UV.x > 0.5) {
		t += pixel_offset/float(tt);
	}
	t -= float(int(t));
	t *= float(tt);
	int pixelid = int(t);
	int x = pixelid % tw;
	int y = pixelid / tw;
	COLOR = vec4( vec3( texture( TEXTURE,vec2(float(x),float(y))/texture_size).r ), 1.0 );
}