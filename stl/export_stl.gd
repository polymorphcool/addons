tool

extends Node

enum UP_AXIS {
	X_UP, Y_UP, Z_UP,
	X_DOWN, Y_DOWN, Z_DOWN
}

export(Mesh) var _mesh:Mesh = null
export(UP_AXIS) var _up_axis:int = UP_AXIS.Z_UP
export(String) var _name:String = "export"
export(String) var _path:String = "export.stl"
export(bool) var _export:bool = false setget set_export

func set_export(b:bool) -> void:
	_export = false
	if b and _mesh != null:
		var f:File = File.new()
		if f.open( _path, File.WRITE ) != OK:
			return
		f.store_string( "solid " + _name + "\n" )
		var faces:PoolVector3Array = _mesh.get_faces()
		var v3:Vector3
		var basis:Basis = Basis.IDENTITY
		match _up_axis:
			UP_AXIS.X_UP:
				basis = Quat(Vector3(0,0,1),PI*0.5)
			UP_AXIS.X_DOWN:
				basis = Quat(Vector3(0,0,1),PI*-0.5)
			UP_AXIS.Y_DOWN:
				basis = Quat(Vector3(1,0,0),PI)
			UP_AXIS.Z_UP:
				basis = Quat(Vector3(1,0,0),PI*0.5)
			UP_AXIS.Z_DOWN:
				basis = Quat(Vector3(1,0,0),PI*-0.5)
		for i in range(0,faces.size(),3):
			f.store_string( "\tfacet\n" )
			f.store_string( "\t\touter loop\n" )
			for j in range(0,3):
				v3 = faces[i+j]
				v3 = basis * v3
				f.store_string( "\t\t\tvertex " + str(v3.x) + " " + str(v3.y) + " " + str(v3.z) + "\n" )
			f.store_string( "\t\tendloop\n" )
			f.store_string( "\tendfacet\n" )
		f.store_string( "endsolid " + _name + "\n" )
		f.close()
