shader_type canvas_item;

// robust util to map value 
float map( float v, float in_min, float in_max, float out_min, float out_max ) {
	// normalisation in range[in_min,in_max]
	float norm = 0.0;
	if ( in_min < in_max ) {
		norm = min(in_max,max(in_min,v)) - in_min;
		norm /= (in_max-in_min);
	} else if ( in_min > in_max ) {
		norm = min(in_min,max(in_max,v)) - in_max;
		norm /= (in_min-in_max);
		norm = 1.0-norm;
	}
	// mapping in range [out_min,out_max]
	float result = norm * (out_max-out_min) + out_min;
	float mmin = out_min;
	float mmax = out_max;
	if ( out_min > out_max ) {
		mmin = out_max;
		mmax = out_min;
		result = (1.-norm) * (out_min-out_max) + out_max;
	}
	// clamping value
	return min(mmax,max(mmin,result));
}
