shader_type spatial;
render_mode blend_mix,depth_draw_opaque,cull_back,diffuse_burley,specular_schlick_ggx;

uniform vec4 albedo : hint_color;
uniform int display_normal : hint_range(0,1);
uniform int display_uv : hint_range(0,1);
uniform vec2 display_uv_factor = vec2(5,100);

void fragment() {
	vec2 base_uv = UV;
	vec2 debug_uv = 
		vec2( 
			float(int(UV.x * display_uv_factor.y)%int(display_uv_factor.x)) * (1.0/display_uv_factor.x), 
			float(int(UV.y * display_uv_factor.y)%int(display_uv_factor.x)) * (1.0/display_uv_factor.x) );
	ALBEDO = mix( mix( albedo.xyz, NORMAL, float(display_normal) ), vec3(debug_uv.x,debug_uv.y,0.0),float(display_uv));
	METALLIC = 0.0;
	ROUGHNESS = 1.0;
	SPECULAR = 0.5;
}
