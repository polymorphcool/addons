shader_type spatial;
render_mode blend_mix,depth_draw_opaque,cull_back,diffuse_burley,specular_schlick_ggx;
uniform vec4 albedo0 : hint_color;
uniform float metallic0;
uniform float roughness0 : hint_range(0,1);
uniform vec4 albedo1 : hint_color;
uniform float metallic1;
uniform float roughness1 : hint_range(0,1);
uniform float point_size : hint_range(0,128);
uniform vec3 uv_scale;
uniform vec3 uv_offset;

void vertex() {
	UV=UV*uv_scale.xy+uv_offset.xy;
}

void fragment() {
	int cellid = int(UV.x) + int(UV.y);
	if ( cellid % 2 == 0 ) {
		ALBEDO = albedo0.xyz;
		METALLIC = metallic0;
		ROUGHNESS = roughness0;
	} else {
		ALBEDO = albedo1.xyz;
		METALLIC = metallic1;
		ROUGHNESS = roughness1;
	}
	SPECULAR = 0.5;
}
