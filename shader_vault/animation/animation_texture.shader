shader_type spatial;

/* Set of functions to use animation textures.
 */

const uint one = uint(1);

// extraction of vertex and normal info from animation texture
void animation_extract_optimised( 
	inout vec3 		eo_vert, 			// [out] vertex offset
	inout vec3 		eo_norm,			// [out] normal
	in sampler2D 	eo_data,			// animation texture
	in vec2 		eo_size, 			// anmation texture size
	in uint 		eo_pixel_offset,	// first frame position
	in uint 		eo_vcount,			// vertex count in mesh
	in uint 		eo_fcount,			// frame count in animation
	in uint 		eo_vid,				// vertex index
	in float 		eo_t0,				// time for animation with fixed fps
	in float 		eo_t1,				// time for animation with normalised time (fps = -1)
	in float 		eo_fps				// animation fps
	) {

	uint asizex = uint(eo_size.x);

	// frame indices computation
	float loc_time = eo_t0;
	if ( eo_fps < 0.0 ) {
		loc_time = eo_t1 * float( eo_fcount );
	} else {
		loc_time *= eo_fps;
	}
	uint frame_0 = uint( loc_time );
	float y_mix = loc_time - float( frame_0 );

	// current frame
	frame_0 %= eo_fcount;
	// next frame
	uint frame_1 = ( frame_0 + uint(1) ) % eo_fcount;

	// pixel index containing vertex offset for both frames
	uint px0 = eo_pixel_offset + frame_0 * eo_vcount * uint(2) + eo_vid * uint(2);
	uint px1 = eo_pixel_offset + frame_1 * eo_vcount * uint(2) + eo_vid * uint(2);

	// extraction and mix of the vertex offsets
	eo_vert += mix(
		texture( eo_data, vec2( 0.5 + float( px0 % asizex ), 0.5 + float( px0 / asizex ) ) / eo_size ).xyz,
		texture( eo_data, vec2( 0.5 + float( px1 % asizex ), 0.5 + float( px1 / asizex ) ) / eo_size ).xyz,
		y_mix
		);

	// next pixel contains the normal
	px0 += one;
	px1 += one;
	// extraction, mix and normalisation of normals
	eo_norm = normalize( mix(
		texture( eo_data, vec2( 0.5 + float( px0 % asizex ), 0.5 + float( px0 / asizex ) ) / eo_size ).xyz,
		texture( eo_data, vec2( 0.5 + float( px1 % asizex ), 0.5 + float( px1 / asizex ) ) / eo_size ).xyz,
		y_mix
		) );

}

// extraction of pixel info 
void animation_extract_info( 
	inout uint 		ei_red, 			// [out] value of red channel
	inout uint 		ei_green, 			// [out] value of green channel
	inout float 	ei_blue,			// [out] value of blue channel
	in sampler2D 	ei_data, 			// animation texture
	in vec2 		ei_size,			// anmation texture size
	in uint 		ei_pid				// pixel index
	) {
	uint asizex = uint(ei_size.x);
	vec3 slice_info = texture( 
		ei_data, 
		vec2( 
			0.5 + float( ei_pid % asizex ), 
			0.5 + float( ei_pid / asizex ) 
		) / ei_size ).xyz;
	ei_red = 	uint(slice_info.x);
	ei_green = 	uint(slice_info.y);
	ei_blue = 	slice_info.z;
}

// extraction and merge of animation's variants
void animation_extract_variant(
	inout vec3 		ea_vert,			// [out] vertex offset
	inout vec3 		ea_norm,			// [out] normal
	in sampler2D 	ea_data,			// animation texture 
	in vec2 		ea_size, 			// anmation texture size
	in uint 		ea_pixel_offset,	// first variant position
	in uint 		ea_vcount,			// vertex count in mesh
	in uint 		ea_variant,			// variant index
	in uint 		ea_vid,				// vertex index
	in float 		ea_t0,				// time for animation with fixed fps
	in float 		ea_t1,				// time for animation with normalised time (fps = -1)
	) {
	
	// working vars
	uint a_offset, a_variants, v_offset, v_frames;
	
	float v_fps, tmp;
	animation_extract_info( 
		a_offset, a_variants, // out 
		tmp, ea_data, ea_size, ea_pixel_offset );
	
	// turning random values into valid variant IDs
	uint v = min( a_variants - one, ea_variant );
	
	// extracting offset and frame count for both variants
	animation_extract_info( 
		v_offset, v_frames, v_fps, // out
		ea_data, ea_size, a_offset + v );
	
	animation_extract_optimised( 
		ea_vert, ea_norm, // out
		ea_data, ea_size, v_offset, ea_vcount, v_frames, ea_vid, ea_t0, ea_t1, v_fps );
}

// extraction and merge of slice's animation
void animation_extract(
	inout vec3 		ea_vert, 			// [out] vertex offset
	inout vec3 		ea_norm,			// [out] normal
	in sampler2D 	ea_data,			// animation texture
	in vec2 		ea_size, 			// anmation texture size
	in uint 		ea_pixel_offset,	// first animation position
	in uint 		ea_vcount,			// vertex count in mesh
	in uint 		ea_aid,				// animation index
	in uint 		ea_vid,				// vertex index
	in vec3 		ea_variant,			// data	to control animation variants
	in float 		ea_t0,				// time for animation with fixed fps
	in float 		ea_t1,				// time for animation with normalised time (fps = -1)
	) {

	// working vars
	uint a_offset, a_variants, v, v_offset, v_frames;
	float tmp, v_fps;
	vec3 vert_v0 = vec3(0.0); 
	vec3 vert_v1 = vec3(0.0);
	vec3 norm_v0, norm_v1;

	// extracting animation info
	animation_extract_info( 
		a_offset, a_variants, tmp, // out
		ea_data, ea_size, ea_pixel_offset+ea_aid );

	//#1 extracting data for first variant
	v = uint( ea_variant.x * float( a_variants ) ) % a_variants; // avoiding id == variant_count
	// variant info
	animation_extract_info( 
		v_offset, v_frames, v_fps, // out 
		ea_data, ea_size, a_offset + v );
	// variant data
	animation_extract_optimised( 
		vert_v0, norm_v0, // out 
		ea_data, ea_size, v_offset, ea_vcount, v_frames, ea_vid, ea_t0, ea_t1, v_fps );

	//#2 extracting data for second variant
	v = uint( ea_variant.y * float( a_variants ) ) % a_variants; // avoiding id == variant_count
	// variant info
	animation_extract_info( 
		v_offset, v_frames, v_fps, // out 
		ea_data, ea_size, a_offset + v );
	// variant data
	animation_extract_optimised( 
		vert_v1, norm_v1, // out 
		ea_data, ea_size, v_offset, ea_vcount, v_frames, ea_vid, ea_t0, ea_t1, v_fps );

	ea_vert = mix( vert_v0, vert_v1, ea_variant.z );
	ea_norm = normalize( mix( norm_v0, norm_v1, ea_variant.z ) );

}

void animate(
	inout vec3 		an_vert, 			// [out] vertex offset
	inout vec3 		an_norm,			// [out] normal
	in sampler2D 	an_data,			// animation texture
	in vec2 		an_size, 			// anmation texture size
	in uint 		an_vid,				// vertex index
	in float 		an_radius,			// determines wich slices are used
	in float 		an_degree,			// detremines wich animations are used
	in vec3 		an_variant,			// data	to control animation variants
	in float 		an_t0,				// time for animation with fixed fps
	in float 		an_t1				// time for animation with normalised time (fps = -1)
	) {
	
	// extracting amount of vertex count and slices count
	vec2 anim_info = texture( an_data, vec2(.5) / an_size ).rg;
	uint vertex_count = uint( anim_info.x );
	uint slice_count = uint( anim_info.y );
	uint slice_max = slice_count - one;
	
	// locate slices
	float mol = max( 0.0, min( float(slice_max), an_radius ) );
	uint s0 = uint(mol);
	uint s1 = min( slice_max, s0 + one );
	float smix = mol - float(s0);
	if ( smix == 0.0 ) {
		s1 = s0;
	} else if ( smix == 1.0 ) {
		s0 = s1;
	}
	// preparing vars to mix slices
	uint px_offset, anim_count, a0, a1;
	vec3 vert_a0 = vec3(0.0); 
	vec3 vert_a1 = vec3(0.0); 
	vec3 vert_s0 = vec3(0.0); 
	vec3 vert_s1 = vec3(0.0); 
	vec3 norm_a0, norm_a1, norm_s0, norm_s1;
	float mor = mod( an_degree, 360.0 );
	float tmp, gap, amix;
	
	// prefectly on a slice
	if ( s0 == s1 ) {
		
		// getting animation from one slice
		animation_extract_info( 
			px_offset, anim_count, tmp, // out
			an_data, an_size, one + s0 );
		
		gap = 360.0 / float( anim_count );
		a0 = uint( mor / gap ) % anim_count;
		a1 = ( a0 + one ) % anim_count;
		amix = ( mor - float( a0 ) * gap ) / gap;

		animation_extract( 
			vert_a0, norm_a0, // out
			an_data, an_size,
			px_offset, vertex_count, a0, an_vid, an_variant, an_t0, an_t1 );
		
		animation_extract( 
			vert_a1, norm_a1, // out
			an_data, an_size,
			px_offset, vertex_count, a1, an_vid, an_variant, an_t0, an_t1 );
		
		an_vert = mix( vert_a0, vert_a1, amix );
		an_norm = normalize( mix( norm_a0, norm_a1, amix ) );
		
	} else {
		
		//#1 getting animation from first slice
		animation_extract_info( 
			px_offset, anim_count, tmp, // out
			an_data, an_size, one + s0 );
		
		gap = 360.0 / float( anim_count );
		a0 = uint( mor / gap ) % anim_count;
		a1 = ( a0 + one ) % anim_count;
		amix = ( mor - float(a0) * gap ) / gap;

		animation_extract( 
			vert_a0, norm_a0, // out
			an_data, an_size,
			px_offset, vertex_count, a0, an_vid, an_variant, an_t0, an_t1 );
		
		animation_extract( 
			vert_a1, norm_a1, // out
			an_data, an_size,
			px_offset, vertex_count, a1, an_vid, an_variant, an_t0, an_t1 );
		
		vert_s0 = mix( vert_a0, vert_a1, amix );
		norm_s0 = mix( norm_a0, norm_a1, amix );
		
		//#2 getting animation from second slice
		animation_extract_info( 
			px_offset, anim_count, tmp, // out
			an_data, an_size, one + s1 );
		
		gap = 360.0 / float( anim_count );
		a0 = uint( mor / gap ) % anim_count;
		a1 = ( a0 + one ) % anim_count;
		amix = ( mor - float(a0) * gap ) / gap;

		animation_extract( 
			vert_a0, norm_a0, // out
			an_data, an_size,
			px_offset, vertex_count, a0, an_vid, an_variant, an_t0, an_t1 );
		
		animation_extract( 
			vert_a1, norm_a1, // out
			an_data, an_size,
			px_offset, vertex_count, a1, an_vid, an_variant, an_t0, an_t1 );
		
		vert_s1 = mix( vert_a0, vert_a1, amix );
		norm_s1 = mix( norm_a0, norm_a1, amix );

		an_vert = mix( vert_s0, vert_s1, smix );
		an_norm = normalize( mix( norm_s0, norm_s1, smix ) );
		
	}
}

void animate_info(
	inout vec3 		an_info0, 			// [out] vertex offset
	inout vec3 		an_info1,			// [out] normal
	in sampler2D 	an_data,			// animation texture
	in vec2 		an_size, 			// anmation texture size
	in uint 		an_vid,				// vertex index
	in float 		an_radius,			// determines wich slices are used
	in float 		an_degree,			// detremines wich animations are used
	in float 		an_t0,				// time for animation with fixed fps
	in float 		an_t1				// time for animation with normalised time (fps = -1)
	) {
	
	// extracting amount of vertex count and slices count
	vec2 anim_info = texture( an_data, vec2(.5) / an_size ).rg;
	uint vertex_count = uint( anim_info.x );
	uint slice_count = uint( anim_info.y );
	uint slice_max = slice_count - one;
	
	// locate slices
	float mol = max( 0.0, min( float(slice_max), an_radius ) );
	uint s0 = uint(mol);
	uint s1 = min( slice_max, s0 + one );
	float smix = mol - float(s0);
	if ( smix == 0.0 ) {
		s1 = s0;
	} else if ( smix == 1.0 ) {
		s0 = s1;
	}
	// preparing vars to mix slices
	uint px_offset, anim_count, a0;
	float mor = mod( an_degree, 360.0 );
	float tmp, gap, amix;
	
	// prefectly on a slice
	if ( s0 == s1 ) {
		
		// getting animation from one slice
		animation_extract_info( 
			px_offset, anim_count, tmp, // out
			an_data, an_size, one + s0 );
		
		gap = 360.0 / float( anim_count );
		a0 = uint( mor / gap ) % anim_count;
		an_info0.x = float(a0) / float(anim_count);
		an_info0.y = float( ( a0 + one ) % anim_count ) / float(anim_count);
		an_info0.z = ( mor - float( a0 ) * gap ) / gap;
		// no info for slide0
		an_info1.z = -1.0;
		
	} else {
		
		//#1 getting animation from first slice
		animation_extract_info( 
			px_offset, anim_count, tmp, // out
			an_data, an_size, one + s0 );
		
		gap = 360.0 / float( anim_count );
		a0 = uint( mor / gap ) % anim_count;
		an_info0.x = float(a0) / float(anim_count);
		an_info0.y = float( ( a0 + one ) % anim_count ) / float(anim_count);
		an_info0.z = ( mor - float(a0) * gap ) / gap;
		
		//#2 getting animation from second slice
		animation_extract_info( 
			px_offset, anim_count, tmp, // out
			an_data, an_size, one + s1 );
		gap = 360.0 / float( anim_count );
		a0 = uint( mor / gap ) % anim_count;
		an_info1.x = float(a0) / float(anim_count);
		an_info1.y = float( ( a0 + one ) % anim_count ) / float(anim_count);
		an_info1.z = ( mor - float(a0) * gap ) / gap;
		
	}
}
