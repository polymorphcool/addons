extends HSlider

signal slider_value

func _ready():
	connect( "value_changed", self, "vchanged" )

func vchanged( f:float ):
	emit_signal( "slider_value", self, f )
