extends Button

signal smart_pressed

var data = null

func _smart_pressed() -> void:
	emit_signal( "smart_pressed", self )

func _ready():
	self.connect( "pressed", self, "_smart_pressed" )
