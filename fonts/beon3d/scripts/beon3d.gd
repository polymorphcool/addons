tool

extends Spatial

export(String) var chars:String = '01234567890abcdefghijklmnopqrstuvwxyz'
export(Vector2) var char_size:Vector2 = Vector2.ONE
export(bool) var generate:bool = false setget set_generate

func get_char(c:String) -> Array:
	match c:
		'x':
			return [
				Vector3(.286,-.5,0),
				Vector3(-.286,.5,0),
				Vector3(-.286,-.5,0),
				Vector3(-.0286,-.05,0),
				Vector3(.286,.5,0),
				Vector3(.0286,.05,0)
			]
		'y':
			return [
				Vector3(.286,.5,0),
				Vector3(0,0,0),
				Vector3(0,0,0),
				Vector3(-.286,.5,0),
				Vector3(0,-.05,0),
				Vector3(0,-.5,0)
			]
		'z':
			return [
				Vector3(-.286,.5,0),
				Vector3(.286,.5,0),
				Vector3(.2574,.45,0),
				Vector3(-.286,-.5,0),
				Vector3(-.25,-.5,0),
				Vector3(.286,-.5,0)
			]
		_:
			return []

func set_generate(b:bool) -> void:
	generate = false
	if b:
		while get_child_count() > 0:
			remove_child( get_child(0) )
		var mat:SpatialMaterial = SpatialMaterial.new()
		mat.flags_unshaded = true
		for i in range( 0, chars.length() ):
			var c:String = chars.substr(i,1)
			var verts:Array = get_char(c)
			if verts.empty():
				continue
			var _mesh:Mesh = Mesh.new()
			var _surf:SurfaceTool = SurfaceTool.new()
			_surf.begin(Mesh.PRIMITIVE_LINES)
			var sc:Vector3 = Vector3( char_size.x, char_size.y, 1 )
			for v in verts:
				_surf.add_vertex( v * sc )
			_surf.index()
			_surf.commit( _mesh )
			var cm:MeshInstance = MeshInstance.new()
			add_child( cm )
			cm.owner = self
			cm.mesh = _mesh
			cm.name = c
			cm.translation = Vector3(char_size.x * (get_child_count()-1), 0, 0 )
			cm.material_override = mat
