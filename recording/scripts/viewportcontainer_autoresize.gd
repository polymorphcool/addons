extends ViewportContainer

func _ready():
	rect_size = get_viewport().size
	if get_child_count() > 0:
		var n = get_child(0)
		if n is Viewport:
			n.size = rect_size
