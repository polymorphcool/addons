extends Spatial

export (Vector3) var angular_speed:Vector3 = Vector3(1,0.5,0.25)

var rotation_enabled:bool = true

func _process(delta) -> void:
	rotation += angular_speed * delta
