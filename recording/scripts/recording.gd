extends Node

export (int, 1, 30 ) var copies:int = 15

var recording:bool = true
var viewports:Array = []
var displays:Array = []
var recorder:Node2D = null
var player:TextureRect = null
var playback:int = 0
var playback_way:int = 1

func _ready() -> void:
	
	var vs:Vector2 = get_viewport().size
	$copy.size = vs
	viewports = [ $copy ]
	
	recorder = Node2D.new()
	add_child( recorder )
	player = TextureRect.new()
	add_child( player )
	player.rect_scale = Vector2.ONE * 0.5
	player.visible = false
	player.flip_v = true
	
	var tr:TextureRect = TextureRect.new()
	recorder.add_child( tr )
	displays = [tr]
	
	for i in range( 0, copies - 1 ):
		var cp:Viewport = $copy.duplicate()
		add_child( cp )
		viewports.append( cp )
		cp.size = vs
		tr = TextureRect.new()
		recorder.add_child( tr )
		displays.append(tr)
	
	viewports.invert()
	
	var input:Viewport = null
	for vp in viewports:
		if input == null:
			input = $source/vp
		tr = vp.get_child(0)
		tr.texture = input.get_texture()
		input = vp
	
	for i in range( 0, viewports.size() ):
		displays[i].texture = viewports[(viewports.size()-1)-i].get_texture()
		displays[i].flip_v = true
		displays[i].rect_scale = Vector2.ONE * 0.5
	
	$fps.rect_position.x = vs.x - $fps.rect_size.x

func _input(event) -> void:
	if event is InputEventKey:
		if event.scancode == KEY_SPACE and event.pressed:
			recording = !recording
			if recording:
				playback = 0
				playback_way = 1
				player.visible = false
				recorder.visible = true
				for vp in viewports:
					vp.render_target_update_mode = Viewport.UPDATE_ALWAYS
			else:
				player.visible = true
				recorder.visible = false
				for vp in viewports:
					vp.render_target_update_mode = Viewport.UPDATE_DISABLED

func _process(delta) -> void:
	
	if not recording:
		player.texture = viewports[playback].get_texture()
		playback += playback_way
		if playback >= viewports.size():
			playback = viewports.size() -1
			playback_way = -1
		elif playback < 0:
			playback = 0
			playback_way = 1
	
	$fps.text = str( ( Engine.get_frames_per_second() * 10 ) * 0.1 ) + ' fps'
