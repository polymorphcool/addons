tool

extends Node

export(NodePath) var animation_player:NodePath = ''
# tracks to purge [comma separated], empty = no effect
export(String) var to_purge:String = ''
# animation to avoid [comma separated], empty = select all
export(String) var animation_white_list:String = ''
# animation to avoid [comma separated], empty = no effect
export(String) var animation_black_list:String = ''

export(bool) var dump:bool = false setget set_dump
export(bool) var purge:bool = false setget set_purge

func set_dump( b:bool ) -> void:
	dump = false
	if b:
		set_purge( true, false )

func set_purge( b:bool, exec:bool = true ) -> void:
	
	purge = false
	if b:
		
		print( "\n######### animation track cleaner new run #########\n")
		
		var ap:AnimationPlayer = get_node( animation_player )
		if ap == null:
			return
		
		var track2purge:Array = to_purge.split(',',false)
		if track2purge.empty():
			if to_purge.lstrip( ' ' ).rstrip( ' ' ).length() > 0:
				track2purge = [to_purge]
			else:
				return
		var l:Array = []
		for tp in track2purge:
			l.append( tp.lstrip( ' ' ).rstrip( ' ' ) )
		track2purge = l
		
		var anim_wl:Array = animation_white_list.split(',',false)
		if anim_wl.empty() and animation_white_list.length() > 0:
			anim_wl = [animation_white_list]
		
		var anim_bl:Array = animation_black_list.split(',',false)
		if anim_bl.empty() and animation_black_list.length() > 0:
			anim_bl = [animation_black_list]
		
		var anims:Array = []
		for anim in ap.get_animation_list():
			if anim in anim_bl:
				continue
			if anim_wl.empty() or anim in anim_wl:
				anims.append( anim )
		
		var convicts:Array = []
		for an in anims:
			var aobj:Animation = ap.get_animation( an )
			var tcount:int = aobj.get_track_count()
			if !exec:
				print( an, ', tracks: ', tcount )
			var tconvicts:Array = []
			for ti in range( 0, tcount ):
				var n:String = aobj.track_get_path( ti )
				var found:bool = false
				for tp in track2purge:
					if n.find( tp ) != -1:
						found = true
						tconvicts.append( n )
						break
				if !exec:
					if found:
						print( '\t[PURGE]', n )
					else:
						print( '\t', n )
			convicts.append( { 'animation': aobj, 'tracks': tconvicts } )
		
		if !exec:
			print( 'track 2 purge\n\t', track2purge )
			print( 'whilelist\n\t', anim_wl )
			print( 'blacklist\n\t', anim_bl )
			print( 'convicts\n\t', convicts )
			return
		
		var track_removed:int = 0
		for convict in convicts:
			var aobj:Animation = convict.animation
			var tcount:int = aobj.get_track_count()
			for t in convict.tracks:
				for ti in range( 0, tcount ):
					if aobj.track_get_path( ti ) == t:
						aobj.remove_track( ti )
						track_removed += 1
						break
		print( 'total removed: ', track_removed )
