tool

extends MeshInstance

export(int,1,100) var vertex_count:int = 1
export(bool) var avoid_duplicates:bool = true
export(bool) var clear:bool = false setget set_clear
export(bool) var pick:bool = false setget set_pick

func set_clear(b:bool) -> void:
	clear = false
	if b:
		while $results.get_child_count() > 0:
				$results.remove_child( $results.get_child(0) )

class DistanceSorter:
	static func sort_ascending(a, b):
		if a.distance < b.distance:
			return true
		return false

func set_pick(b:bool) -> void:
	pick = false
	if b:
		set_clear( true )
		if mesh == null:
			return
		var target:Vector3 = $picker.translation
		var vertices:Array = []
		var surface_count:int = mesh.get_surface_count()
		for si in range( 0, surface_count ):
			var surface:Array = mesh.surface_get_arrays(si)
			var verts:Array = surface[Mesh.ARRAY_VERTEX]
			if avoid_duplicates:
				for vi in range( 0, verts.size() ) :
					var unique:bool = true
					for v in vertices:
						if verts[vi] == v.v:
							unique = false
							break
					if unique:
						vertices.append({ 's': si, 'i': vi, 'v': verts[vi] })
			else:
				verts.append_array( verts )
		
		var distances:Array = []
		for vi in range( 0, vertices.size() ) :
			var v:Dictionary = vertices[vi]
			distances.append( { 
				'sid': v.s, 
				'vid': v.i, 
				'pos': v.v,
				'distance': (v.v-target).length() }
				)
		distances.sort_custom(DistanceSorter, "sort_ascending")
		
		var igm:SpatialMaterial = SpatialMaterial.new()
		igm.flags_unshaded = true
		igm.albedo_color = Color( 0,1,1 )
		
		var sphr:SphereMesh = SphereMesh.new()
		sphr.radius = $picker.mesh.radius * 0.2
		sphr.height = $picker.mesh.height * 0.2
		sphr.material = $picker.material_override.duplicate()
		sphr.material.albedo_color = Color( 0,1,1 )
		
		for i in range( 0, vertex_count ):
			var d:Dictionary = distances[i]
			var p3d:MeshInstance = MeshInstance.new()
			$results.add_child(p3d)
			p3d.owner = $results.owner
			p3d.name = 'vertex_s'+str(d.sid)+'_v'+str(d.vid)
			p3d.mesh = sphr
			var ig:ImmediateGeometry = ImmediateGeometry.new()
			p3d.add_child(ig)
			ig.owner = p3d.owner
			ig.name = 'connector'
			p3d.translation = d.pos
			ig.translation = Vector3.ZERO
			ig.begin(Mesh.PRIMITIVE_LINES)
			ig.add_vertex( Vector3.ZERO )
			ig.add_vertex( target - d.pos )
			ig.end()
			ig.material_override = igm
