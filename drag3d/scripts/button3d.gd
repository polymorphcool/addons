tool

extends Spatial

signal button3d_entered
signal button3d_exited
signal button3d_pressed
signal button3d_released
signal button3d_dragged

enum eshape {
	BOX,
	SPHERE,
	CAPSULE,
	CYLINDER
}

export(eshape) var shape:int = 0 setget set_shape
export var custom_shape:bool = false

export var draggable:bool = true

export var enable_color:bool = true
export var normal_color:Color = Color.black
export var hover_color:Color = Color(.8,.2,.2)
export var pressed_color:Color = Color.white

export var debug:bool = true

var initialised:bool = false
var hover:bool = false
var pressed:bool = false

var mouse_camera:Camera = null
var mouse_distance:float = 0
var mouse_click:Vector3 = Vector3.ZERO 		# global coordinate!
var mouse_pos:Vector3 = Vector3.ZERO		# global coordinate!
var mouse_drag:Vector3 = Vector3.ZERO		# global coordinate!

func set_shape(i:int) -> void:
	shape = i
	if !custom_shape and is_inside_tree():
		var mat:SpatialMaterial = null
		if enable_color:
			mat = SpatialMaterial.new()
			mat.albedo_color = normal_color
		match shape:
			eshape.BOX:
				$area/collision.shape = BoxShape.new()
				$display.mesh = CubeMesh.new()
				$display.material_override = mat
			eshape.SPHERE:
				$area/collision.shape = SphereShape.new()
				$display.mesh = SphereMesh.new()
				$display.material_override = mat
			eshape.CAPSULE:
				$area/collision.shape = CapsuleShape.new()
				$display.mesh = CapsuleMesh.new()
				$display.material_override = mat
			eshape.CYLINDER:
				$area/collision.shape = CylinderShape.new()
				$display.mesh = CylinderMesh.new()
				$display.material_override = mat
			_:
				printerr( "invalid type of shape..." )

func apply_color() -> void:
	if !enable_color:
		return
	if pressed:
		$display.material_override.albedo_color = pressed_color
	elif hover:
		$display.material_override.albedo_color = hover_color
	else:
		$display.material_override.albedo_color = normal_color

func connect_area() -> void:
	$area.connect("input_event",self,"_area_input_event")
	$area.connect("mouse_entered",self,"_area_mouse_entered")
	$area.connect("mouse_exited",self,"_area_mouse_exited")

func prepare_debug() -> void:
	
	if !debug:
		if get_node("debug") != null:
			remove_child( get_node("debug") )
		return
	
	while $debug.get_child_count() > 0:
		$debug.remove_child( $debug.get_child(0) )
	
	# contact point
	var contact:MeshInstance = MeshInstance.new()
	contact.mesh = SphereMesh.new()
	contact.mesh.radius = 0.05
	contact.mesh.height = 0.1
	var cmat:SpatialMaterial = SpatialMaterial.new()
	cmat.albedo_color = Color(1,0,1)
	cmat.flags_unshaded = true
	contact.material_override = cmat
	$debug.add_child( contact )
	
	var amat:SpatialMaterial = SpatialMaterial.new()
	amat.flags_unshaded = true
	amat.flags_no_depth_test = true
	amat.render_priority = 100
	
	var x_axis:ImmediateGeometry = ImmediateGeometry.new()
	x_axis.begin(Mesh.PRIMITIVE_LINES)
	x_axis.add_vertex(Vector3.ZERO)
	x_axis.add_vertex(Vector3(1,0,0))
	x_axis.end()
	x_axis.material_override = amat.duplicate()
	x_axis.material_override.albedo_color = Color.red
	$debug.add_child( x_axis )
	
	var y_axis:ImmediateGeometry = ImmediateGeometry.new()
	y_axis.begin(Mesh.PRIMITIVE_LINES)
	y_axis.add_vertex(Vector3.ZERO)
	y_axis.add_vertex(Vector3(0,1,0))
	y_axis.end()
	y_axis.material_override = amat.duplicate()
	y_axis.material_override.albedo_color = Color.green
	$debug.add_child( y_axis )
	
	var z_axis:ImmediateGeometry = ImmediateGeometry.new()
	z_axis.begin(Mesh.PRIMITIVE_LINES)
	z_axis.add_vertex(Vector3.ZERO)
	z_axis.add_vertex(Vector3(0,0,1))
	z_axis.end()
	z_axis.material_override = amat.duplicate()
	z_axis.material_override.albedo_color = Color.blue
	$debug.add_child( z_axis )
	
	var drag:ImmediateGeometry = ImmediateGeometry.new()
	drag.material_override = amat.duplicate()
	drag.material_override.albedo_color = Color.black
	$debug.add_child( drag )
	
	# axis drag
	var dragx:MeshInstance = MeshInstance.new()
	dragx.mesh = CubeMesh.new()
	dragx.mesh.size = Vector3.ONE * 0.1
	dragx.material_override = amat.duplicate()
	dragx.material_override.albedo_color = Color.red
	$debug.add_child( dragx )
	
	var dragy:MeshInstance = MeshInstance.new()
	dragy.mesh = CubeMesh.new()
	dragy.mesh.size = Vector3.ONE * 0.1
	dragy.material_override = amat.duplicate()
	dragy.material_override.albedo_color = Color.green
	$debug.add_child( dragy )
	
	var dragz:MeshInstance = MeshInstance.new()
	dragz.mesh = CubeMesh.new()
	dragz.mesh.size = Vector3.ONE * 0.1
	dragz.material_override = amat.duplicate()
	dragz.material_override.albedo_color = Color.blue
	$debug.add_child( dragz )
	
	var drag_proj:ImmediateGeometry = ImmediateGeometry.new()
	drag_proj.material_override = amat.duplicate()
	drag_proj.material_override.albedo_color = Color(0,0,0,0.5)
	drag_proj.material_override.flags_transparent = true
	$debug.add_child( drag_proj )
	
	$debug.visible = false

func apply_debug() -> void:
	
	if !debug:
		return
	
	$debug.visible = hover || pressed
	
	if !hover and !pressed:
		$display.translation = Vector3.ZERO
		return
	
	var contact:MeshInstance = 		$debug.get_child(0)
	var x_axis:ImmediateGeometry = 		$debug.get_child(1)
	var y_axis:ImmediateGeometry = 		$debug.get_child(2)
	var z_axis:ImmediateGeometry = 		$debug.get_child(3)
	var drag:ImmediateGeometry = 		$debug.get_child(4)
	var dragx:MeshInstance = 			$debug.get_child(5)
	var dragy:MeshInstance = 			$debug.get_child(6)
	var dragz:MeshInstance = 			$debug.get_child(7)
	var drag_proj:ImmediateGeometry = 	$debug.get_child(8)
	
	drag.clear()
	drag_proj.clear()
	contact.scale = Vector3.ONE / self.scale
	
	if !pressed or !draggable:
		$debug.transform.origin = to_local(mouse_pos)
		$display.translation = Vector3.ZERO
		x_axis.scale = Vector3.ONE
		y_axis.scale = Vector3.ONE
		z_axis.scale = Vector3.ONE
		dragx.visible = false
		dragy.visible = false
		dragz.visible = false
	
	else:
		$debug.transform.origin = to_local(mouse_click)
		var local_drag:Vector3 = to_local(mouse_pos) - to_local(mouse_click)
		$display.translation = local_drag
		x_axis.scale = Vector3(local_drag.x,1,1)
		y_axis.scale = Vector3(1,local_drag.y,1)
		z_axis.scale = Vector3(1,1,local_drag.z)
		drag.begin(Mesh.PRIMITIVE_LINES)
		drag.add_vertex(Vector3.ZERO)
		drag.add_vertex(local_drag)
		drag.end()
		dragx.scale = Vector3.ONE / self.scale
		dragx.translation = local_drag * Vector3(1,0,0)
		dragx.visible = true
		dragy.scale = Vector3.ONE / self.scale
		dragy.translation = local_drag * Vector3(0,1,0)
		dragy.visible = true
		dragz.scale = Vector3.ONE / self.scale
		dragz.translation = local_drag * Vector3(0,0,1)
		dragz.visible = true
		drag_proj.begin(Mesh.PRIMITIVE_LINES)
		drag_proj.add_vertex(local_drag * Vector3(1,0,0))
		drag_proj.add_vertex(local_drag * Vector3(1,0,1))
		drag_proj.add_vertex(local_drag * Vector3(1,0,1))
		drag_proj.add_vertex(local_drag)
		drag_proj.add_vertex(local_drag * Vector3(0,1,0))
		drag_proj.add_vertex(local_drag * Vector3(1,1,0))
		drag_proj.add_vertex(local_drag * Vector3(1,1,0))
		drag_proj.add_vertex(local_drag)
		drag_proj.add_vertex(local_drag * Vector3(0,0,1))
		drag_proj.add_vertex(local_drag * Vector3(1,0,1))
		drag_proj.add_vertex(local_drag * Vector3(1,0,1))
		drag_proj.add_vertex(local_drag)
		drag_proj.end()

func _ready() -> void:
	
	initialised = true
	# loaing shape
	set_shape(shape)
	# connect area signals
	connect_area()
	if debug:
		prepare_debug()

func _process(delta) -> void:
	pass

func _input(ev:InputEvent) -> void:
	
	if !draggable:
		return
	
	if pressed and ev is InputEventMouseButton: 
		if ev.button_index == BUTTON_LEFT and !ev.pressed:
			pressed = false
			mouse_drag *= 0
			apply_color()
			apply_debug()
	
	elif pressed and ev is InputEventMouseMotion:
		if mouse_camera != null:
			mouse_pos = mouse_camera.project_position( ev.position, mouse_distance )
			mouse_drag = mouse_pos - mouse_click
			apply_debug()
			emit_signal("button3d_dragged", self )

func _area_input_event( cam:Camera, ev:InputEvent, pos:Vector3, norm:Vector3, shape_idx:int ) -> void:
	
	if ev is InputEventMouseButton:
		if ev.button_index == BUTTON_LEFT:
			pressed = ev.pressed
			mouse_camera = cam
			mouse_distance = (pos - cam.global_transform.origin).length()
			mouse_click = pos
			mouse_pos = mouse_click
			mouse_drag = mouse_pos - mouse_click
			apply_color()
			apply_debug()
			if pressed:
				emit_signal("button3d_pressed", self )
			else:
				emit_signal("button3d_released", self )
	
	elif !pressed or !draggable and ev is InputEventMouseMotion:
		mouse_click *= 0
		mouse_pos = pos
		mouse_drag *= 0
		apply_debug()

func _area_mouse_entered():
	hover = true
	apply_color()
	emit_signal("button3d_entered", self )

func _area_mouse_exited():
	hover = false
	if !draggable:
		pressed = false
	apply_color()
	apply_debug()
	emit_signal("button3d_exited", self )
