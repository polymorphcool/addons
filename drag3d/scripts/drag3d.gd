extends Spatial

func _ready() -> void:
	
	
	$button3d0.connect("button3d_entered", self, "entered")
	$button3d1.connect("button3d_entered", self, "entered")
	$button3d2.connect("button3d_entered", self, "entered")
	$button3d3.connect("button3d_entered", self, "entered")
	
	$button3d0.connect("button3d_pressed", self, "pressed")
	$button3d1.connect("button3d_pressed", self, "pressed")
	$button3d2.connect("button3d_pressed", self, "pressed")
	$button3d3.connect("button3d_pressed", self, "pressed")
	
	$button3d0.connect("button3d_dragged", self, "dragged")
	$button3d1.connect("button3d_dragged", self, "dragged")
	$button3d2.connect("button3d_dragged", self, "dragged")
	$button3d3.connect("button3d_dragged", self, "dragged")
	
	$button3d0.connect("button3d_exited", self, "exited")
	$button3d1.connect("button3d_exited", self, "exited")
	$button3d2.connect("button3d_exited", self, "exited")
	$button3d3.connect("button3d_exited", self, "exited")
	
	$button3d0.connect("button3d_released", self, "released")
	$button3d1.connect("button3d_released", self, "released")
	$button3d2.connect("button3d_released", self, "released")
	$button3d3.connect("button3d_released", self, "released")

func exited( s:Spatial ) -> void:
	$ui/info.text = ''

func released( s:Spatial ) -> void:
	$ui/info.text = ''

func entered( s:Spatial ) -> void:
	var txt:String = ''
	txt += "button " + s.name + " entered\n"
	$ui/info.text = txt
	
func pressed( s:Spatial ) -> void:
	
	var txt:String = ''
	txt += "button " + s.name + " pressed\n"
	txt += 'global click \n\tx: ' + str(s.mouse_click.x)+'\n\ty: ' + str(s.mouse_click.y)+'\n\tz: ' + str(s.mouse_click.z) + "\n"
	$ui/info.text = txt.replace('\t','    ')
	
func dragged( s:Spatial ) -> void:
	
	var txt:String = ''
	txt += "button " + s.name + " dragged\n"
	txt += 'global click \n\tx: ' + str(s.mouse_click.x)+'\n\ty: ' + str(s.mouse_click.y)+'\n\tz: ' + str(s.mouse_click.z) + "\n"
	txt += 'global pos \n\tx: ' + str(s.mouse_pos.x)+'\n\ty: ' + str(s.mouse_pos.y)+'\n\tz: ' + str(s.mouse_pos.z) + "\n"
	txt += 'global drag \n\tx: ' + str(s.mouse_drag.x)+'\n\ty: ' + str(s.mouse_drag.y)+'\n\tz: ' + str(s.mouse_drag.z) + "\n"
	
	var local_drag = s.to_local(s.mouse_pos) - s.to_local(s.mouse_click)
	
	txt += 'local drag \n\tx: ' + str(local_drag.x)+'\n\ty: ' + str(local_drag.y)+'\n\tz: ' + str(local_drag.z) + "\n"
	$ui/info.text = txt.replace('\t','    ')
