extends Node

signal markov_link

onready var mlink_script:Reference = load( "addons/polymorph/markov/markov_link.gd" )

export(bool) var start_link:bool =				false
export(float,0,300) var delay_min:float =		0
export(float,0,300) var delay_max:float =		0

export(NodePath) var _target:NodePath = 		''
export(Dictionary) var data:Dictionary = 		{}

export(NodePath) var link0_path:NodePath = 		''
export(float) var link0_weight:float = 			1
export(NodePath) var link1_path:NodePath = 		''
export(float) var link1_weight:float = 			1
export(NodePath) var link2_path:NodePath = 		''
export(float) var link2_weight:float = 			1
export(NodePath) var link3_path:NodePath = 		''
export(float) var link3_weight:float = 			1
export(NodePath) var link4_path:NodePath = 		''
export(float) var link4_weight:float = 			1
export(NodePath) var link5_path:NodePath = 		''
export(float) var link5_weight:float = 			1
export(NodePath) var link6_path:NodePath = 		''
export(float) var link6_weight:float = 			1
export(NodePath) var link7_path:NodePath = 		''
export(float) var link7_weight:float = 			1
export(NodePath) var link8_path:NodePath = 		''
export(float) var link8_weight:float = 			1
export(NodePath) var link9_path:NodePath = 		''
export(float) var link9_weight:float = 			1

var target:Node = null
var valid:bool = false
var links:Array = []
var wait:float = 0

func load_link( np:NodePath, w:float ) -> void:
	if np == '':
		return
	if w <= 0:
		printerr( "Link with weight <=0 are ignored" )
	var n:Node = get_node( np )
	if n == null:
		return
	if n.get_script() != mlink_script:
		printerr( "Node ", n.name, " is not a valid markov link" )
		return
	if n.get_parent() != get_parent():
		printerr( "Invalid link to ", n.name, ", markov links must have the same parent" )
		return
	links.append({
		'link': n,
		'weight': w
	})

func init_links() -> void:
	load_link( link0_path, link0_weight )
	load_link( link1_path, link1_weight )
	load_link( link2_path, link2_weight )
	load_link( link3_path, link3_weight )
	load_link( link4_path, link4_weight )
	load_link( link5_path, link4_weight )
	load_link( link6_path, link6_weight )
	load_link( link7_path, link7_weight )
	load_link( link8_path, link8_weight )
	load_link( link9_path, link9_weight )
	target = get_node( _target )
	valid = !links.empty()
	if !valid:
		return
	# weight normalisation
	var tw:float = 0
	for l in links:
		tw += l.weight
	var w:float = 0
	for l in links:
		l.weight = w + l.weight/tw
	delay_min = max(0, delay_min)
	delay_max = max(0, delay_max)
	if delay_min > delay_max:
		var tmp:float = delay_min
		delay_min = delay_max
		delay_max = tmp

func emit_markov_link() -> void:
	if !valid:
		return
	# pick link
	randomize()
	var r:float = rand_range( 0, 1 )
	for l in links:
		if r <= l.weight:
			emit_signal( "markov_link", self, l.link )
			return
	emit_signal( "markov_link", self, links[ links.size()-1 ].link )

func _ready():
	if valid:
		return
	# load and validate links
	init_links()

func _process(delta):
	if !valid:
		return
	if wait > 0:
		wait -= delta
		if wait <= 0:
			emit_markov_link()

# interactions with links
## asking for next link
func next() -> void:
	if !valid:
		return
	if delay_min == 0 and delay_max == 0:
		emit_markov_link()
	else:
		wait = rand_range( delay_min, delay_max )

## preventing to emit a signal
func stop() -> void:
	if !valid:
		return
	if wait > 0:
		wait = 0
