extends Node

signal markov_target
signal markov_data

onready var mlink_script:Reference = load( "addons/polymorph/markov/markov_link.gd" )

export(bool) var emit_target:bool = 	true
export(bool) var emit_data:bool =		true
export(bool) var play:bool =			true setget set_play

export(float,0,300) var delay_min:float =		0
export(float,0,300) var delay_max:float =		0

var chain:Array = []
var previous_link:Node = null
var current_link:Node = null
var wait:float = 0

func set_play( b:bool ) -> void:
	play = b
	if !play:
		wait = 0
	elif current_link != null:
		markov_link( previous_link, current_link )

func init_links() -> void:
	for c in get_children():
		if c.get_script() == mlink_script:
			c.init_links()
			c.connect( "markov_link", self, "markov_link" )
			if current_link == null and c.start_link:
				current_link = c
			chain.append( c )
	if chain.empty():
		return
	if current_link == null:
		current_link = chain[0]
	delay_min = max(0, delay_min)
	delay_max = max(0, delay_max)
	if delay_min > delay_max:
		var tmp:float = delay_min
		delay_min = delay_max
		delay_max = tmp
	markov_link( previous_link, current_link )
	
func markov_link( src:Node, dst:Node ) -> void:
	
	if !play:
		return
	
	if src != null and !src in chain:
		printerr( "Source link is not in this chain ", src.name, " / ", src )
	if !dst in chain:
		printerr( "Destination link is not in this chain ", src.name, " / ", src )
	
	previous_link = current_link
	current_link = dst
	
	if emit_target:
		emit_signal( "markov_target", dst.target )
	if emit_data:
		emit_signal( "markov_data", dst.data )
	if delay_min == 0 and delay_max == 0:
		# avoiding stack overflow
		wait = 1e-5
	else:
		wait = rand_range( delay_min, delay_max )

func _ready():
	init_links()

func _process(delta):
	
	if !play:
		return
	
	if current_link != null and wait > 0:
		wait -= delta
		if wait <= 0:
			current_link.next()
