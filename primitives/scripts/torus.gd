tool

extends MeshInstance

const target:String = "res://addons/polymorph/primitives/export/"

enum esmooth {
	SMOOTH_ALL,
	SMOOTH_BIG,
	FLAT
}

enum edisplay {
	STANDARD,
	NORMAL,
	UV
}

export(float,0,100) var radius_big:float = 2
export(float,0,2) var radius_small:float = 0.4

export(float,0,1) var precent_big:float = 1
export(float,0,1) var precent_small:float = 1

export(int,3,128) var definition_big:int = 36
export(int,3,128) var definition_small:int = 12

export(float,-360,360) var angle_big:float = 0
export(float,-360,360) var angle_small:float = 0

export var caps:bool = false
export var base:bool = false

export(esmooth) var smooth:int = esmooth.SMOOTH_ALL

export var generate:bool setget set_generate
export var clear:bool setget set_clear

export(edisplay) var display:int = edisplay.STANDARD setget set_display

func set_display(i:int) -> void:
	display = i
	if is_inside_tree():
		match display:
			edisplay.NORMAL:
				material_override.set_shader_param("display_normal",1)
				material_override.set_shader_param("display_uv",0)
			edisplay.UV:
				material_override.set_shader_param("display_normal",0)
				material_override.set_shader_param("display_uv",1)
			_:
				material_override.set_shader_param("display_normal",0)
				material_override.set_shader_param("display_uv",0)

func set_generate(b:bool) -> void:
	if b:
		# computing vertices, uvs and normals
		var rings:Array = []
		var uvs:Array = []
		var ring_centers:Array = []
		var bgap:float = TAU / definition_big
		var baoff:float = angle_big / 360 * TAU
		var sgap:float = TAU / definition_small
		var saoff:float = angle_small / 360 * TAU
		for b in range(0,definition_big+1):
			var ba:float = baoff + bgap * b
			var u:float = float(b)/definition_big
			var bb:Basis = Basis( Vector3.UP, -ba )
			var boff:Vector3 = Vector3( cos(ba), 0, sin(ba) ) * radius_big
			var ring:Array = []
			var uv:Array = []
			for s in range(0,definition_small+1):
				var sa:float = PI + saoff + sgap * s
				var sv:Vector3 = Vector3( cos(sa)/cos(bgap*0.5), sin(sa), 0 ) * radius_small
				var v:float = float(s)/definition_small
				ring.append( (bb*sv) + boff )
				uv.append( Vector2(u,v) )
			rings.append(ring)
			uvs.append(uv)
			ring_centers.append(boff)
		# mesh generation
		var ring_def:int = rings[0].size()
		var _mesh:Mesh = Mesh.new()
		var _surf:SurfaceTool = SurfaceTool.new()
		_surf.begin(Mesh.PRIMITIVE_TRIANGLES)
		var dbig:int = round(definition_big*precent_big)
		var dsmall:int = int((ring_def-1)*precent_small)
		for i in range(0,dbig):
			var ra:Array = rings[i]
			var uva:Array = uvs[i]
			var cntra:Vector3 = ring_centers[i]
			var rb:Array = rings[i+1]
			var uvb:Array = uvs[i+1]
			var cntrb:Vector3 = ring_centers[i+1]
			# start cap generation
			if caps and dbig < definition_big and i == 0:
				# generation of triangles for start cap
				var prev_i:int = (i+(definition_big-1))%definition_big
				var prev_uv:Array = uvs[prev_i]
				var norm:Vector3 = (ring_centers[prev_i]-cntra).normalized()
				var cntr:Vector3 = cntra
				if dsmall < ring_def:
					cntr = ra[0] + (ra[dsmall]-ra[0]) * 0.5
				for j in range(0,dsmall):
					var vi:int = j
					var vj:int = (j+1) % ring_def
					var v2:Vector3 = ra[vj]
					var v3:Vector3 = ra[vi]
					var uv2:Vector2 = uva[vj]
					var uv3:Vector2 = uva[vi]
					_surf.add_normal(norm)
					_surf.add_uv(uv3)
					_surf.add_vertex(v3)
					_surf.add_normal(norm)
					_surf.add_uv(uv2)
					_surf.add_vertex(v2)
					_surf.add_normal(norm)
					_surf.add_uv(prev_uv[vi]+(prev_uv[vj]-prev_uv[vi])*0.5)
					_surf.add_vertex(cntr)
				
			for j in range(0,dsmall):
				var vi:int = j
				var vj:int = (j+1) % ring_def
				var v0:Vector3 = rb[vi]
				var v1:Vector3 = rb[vj]
				var v2:Vector3 = ra[vj]
				var v3:Vector3 = ra[vi]
				var uv0:Vector2 = uvb[vi]
				var uv1:Vector2 = uvb[vj]
				var uv2:Vector2 = uva[vj]
				var uv3:Vector2 = uva[vi]
				var n0:Vector3 = (v0-cntrb).normalized()
				var n1:Vector3 = (v1-cntrb).normalized()
				var n2:Vector3 = (v2-cntra).normalized()
				var n3:Vector3 = (v3-cntra).normalized()
				match smooth:
					esmooth.SMOOTH_BIG:
						var v01:Vector3 = ((v0+(v1-v0)*0.5)-cntrb).normalized()
						var v23:Vector3 = ((v2+(v3-v2)*0.5)-cntra).normalized()
						n0 = v01
						n1 = v01
						n2 = v23
						n3 = v23
					esmooth.FLAT:
						var v01:Vector3 = ((v0+(v1-v0)*0.5)-cntrb)
						var v23:Vector3 = ((v2+(v3-v2)*0.5)-cntra)
						var midc:Vector3 = cntrb+(cntra-cntrb)*0.5
						var midf:Vector3 = v01+(v23-v01)*0.5
						n0 = (midf-midc).normalized()
						n1 = n0
						n2 = n0
						n3 = n0
				# quad part 1
				_surf.add_normal(n0)
				_surf.add_uv(uv0)
				_surf.add_vertex(v0)
				# V
				_surf.add_normal(n1)
				_surf.add_uv(uv1)
				_surf.add_vertex(v1)
				# V
				_surf.add_normal(n2)
				_surf.add_uv(uv2)
				_surf.add_vertex(v2)
				# quad part 2
				_surf.add_normal(n2)
				_surf.add_uv(uv2)
				_surf.add_vertex(v2)
				# V
				_surf.add_normal(n3)
				_surf.add_uv(uv3)
				_surf.add_vertex(v3)
				# V
				_surf.add_normal(n0)
				_surf.add_uv(uv0)
				_surf.add_vertex(v0)
				
				if base and dsmall < definition_small and vj == dsmall:
					v0 = rb[0]
					v3 = ra[0]
					uv0 = uvb[0]
					uv3 = uva[0]
					n0 = (v0+(v1-v0)*0.5).normalized()
					n2 = (v2+(v3-v2)*0.5).normalized()
					match smooth:
						esmooth.FLAT:
							n0 = (n0+(n2-n0)*0.5).normalized()
							n2 = n0
					n1 = n0
					n3 = n2
					# quad part 1
					_surf.add_normal(n1)
					_surf.add_uv(uv1)
					_surf.add_vertex(v1)
					# V
					_surf.add_normal(n0)
					_surf.add_uv(uv0)
					_surf.add_vertex(v0)
					# V
					_surf.add_normal(n2)
					_surf.add_uv(uv2)
					_surf.add_vertex(v2)
					# quad part 2
					_surf.add_normal(n3)
					_surf.add_uv(uv3)
					_surf.add_vertex(v3)
					# V
					_surf.add_normal(n2)
					_surf.add_uv(uv2)
					_surf.add_vertex(v2)
					# V
					_surf.add_normal(n0)
					_surf.add_uv(uv0)
					_surf.add_vertex(v0)
			
			# start cap generation
			if caps and dbig < definition_big and i == dbig-1:
				# generation of triangles for start cap
				var next_i:int = (i+2)%definition_big
				var next_uv:Array = uvs[next_i]
				var norm:Vector3 = (ring_centers[next_i]-cntrb).normalized()
				var cntr:Vector3 = cntrb
				if dsmall < ring_def:
					cntr = rb[0] + (rb[dsmall]-rb[0]) * 0.5
				for j in range(0,dsmall):
					var vi:int = j
					var vj:int = (j+1) % ring_def
					var v0:Vector3 = rb[vj]
					var v1:Vector3 = rb[vi]
					var uv0:Vector2 = uvb[vj]
					var uv1:Vector2 = uvb[vi]
					_surf.add_normal(norm)
					_surf.add_uv(uv0)
					_surf.add_vertex(v0)
					_surf.add_normal(norm)
					_surf.add_uv(uv1)
					_surf.add_vertex(v1)
					_surf.add_normal(norm)
					_surf.add_uv(next_uv[vi]+(next_uv[vj]-next_uv[vi])*0.5)
					_surf.add_vertex(cntr)
				
		_surf.index()
		_surf.commit( _mesh )
		
		var d:Directory = Directory.new()
		d.make_dir_recursive(target)
		var mesh_path:String  = target+'torus_'+str(definition_big)+'x'+str(definition_small)+'_' + str(OS.get_unix_time())+'.res'
		ResourceSaver.save( mesh_path, _mesh, ResourceSaver.FLAG_CHANGE_PATH|ResourceSaver.FLAG_REPLACE_SUBRESOURCE_PATHS)
		mesh = load( mesh_path )
		set_display(display)

func set_clear(b:bool) -> void:
	mesh = null

func _process(delta):
	pass
