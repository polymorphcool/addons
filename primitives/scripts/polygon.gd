tool

extends MeshInstance

const target:String = "res://addons/polymorph/primitives/export/"

enum eplane {
	XZ,
	XY,
	YZ
}

enum euv_unwrap {
	PROJECTION_FLOOR,
	RADIAL
}

enum edisplay {
	STANDARD,
	NORMAL,
	UV
}

export(eplane) var plane:int = eplane.XZ
export(float,0,10) var radius_outer:float = 1
export(float,0,10) var radius_inner:float = 0
export var center:Vector3 = Vector3.ZERO

export(int,3,720) var definition:int = 36
export(int,0,720) var dash_line:int = 0
export(int,0,720) var dash_space:int = 0

export(float,-180,180) var start_angle:float = 0
export(float,0,360) var angle:float = 0

export(euv_unwrap) var uv_unwrap:int = euv_unwrap.PROJECTION_FLOOR
export var smooth:bool = true
export var fill:bool = false
export var caps:bool = false
export var _export:bool = true

export var generate:bool setget set_generate
export var clear:bool setget set_clear

export(edisplay) var display:int = edisplay.UV setget set_display

func set_display(i:int) -> void:
	display = i
	if is_inside_tree():
		match display:
			edisplay.NORMAL:
				material_override.set_shader_param("display_normal",1)
				material_override.set_shader_param("display_uv",0)
			edisplay.UV:
				material_override.set_shader_param("display_normal",0)
				material_override.set_shader_param("display_uv",1)
			_:
				material_override.set_shader_param("display_normal",0)
				material_override.set_shader_param("display_uv",0)

func set_generate(b:bool) -> void:
	
	if b and radius_outer > 0:
		
		var radius_ratio:float = radius_inner/radius_outer
		var inner_offset:Vector3 = Vector3.ZERO
		if radius_inner != 0:
			inner_offset = center * (1-radius_ratio)
		
		var rot:Basis = Basis.IDENTITY
		match plane:
			eplane.XY:
				rot = Basis( Vector3(1,0,0), -PI*.5 )
			eplane.YZ:
				rot = Basis( Vector3(0,0,1), -PI*.5 )
		
		# generation of perimeter vertices
		var rad:float = TAU
		var agap:float = rad / definition
		if angle != 0:
			rad = angle / 360.0 * TAU
			agap = rad / (definition-1)
		var rad_offset:float = start_angle / 360.0 * TAU
		var vertices:Array = []
		var vmin:Vector3 = Vector3.ZERO
		var vmax:Vector3 = Vector3.ZERO
		for i in range(0,definition):
			var a0:float = rad_offset + agap * i
			var vert:Vector3 = Vector3(cos(a0),0,sin(a0))
			if i == 0:
				vmin = vert
				vmax = vert
			else:
				for j in range(0,3):
					if vert[j] < vmin[j]:
						vmin[j] = vert[j]
					if vert[j] > vmax[j]:
						vmax[j] = vert[j]
			vertices.append( vert )
		var area:Vector3 = vmax - vmin
		
		var _mesh:Mesh = Mesh.new()
		var _surf:SurfaceTool = SurfaceTool.new()
		
		if fill:
			_surf.begin(Mesh.PRIMITIVE_TRIANGLES)
		else:
			_surf.begin(Mesh.PRIMITIVE_LINES)
		
		for i in range(0,definition):
			
			if angle != 0 and i == definition-1:
				break
			
			var j:int = (i+1)%definition
			var v0:Vector3 = vertices[i] * radius_outer
			var v1:Vector3 = vertices[j] * radius_outer
			var v2:Vector3 = inner_offset + vertices[j] * radius_inner
			var v3:Vector3 = inner_offset + vertices[i] * radius_inner
			
			var norm0:Vector3 = Vector3.UP
			var norm1:Vector3 = Vector3.UP
			var norm2:Vector3 = Vector3.UP
			var norm3:Vector3 = Vector3.UP
			var norm_cntr:Vector3 = Vector3.UP
			
			var uv0:Vector2 = ( Vector2(v0.x/radius_outer,v0.z/radius_outer) + Vector2.ONE ) * 0.5
			var uv1:Vector2 = ( Vector2(v1.x/radius_outer,v1.z/radius_outer) + Vector2.ONE ) * 0.5
			var uv2:Vector2 = ( Vector2(v2.x/radius_outer,v2.z/radius_outer) + Vector2.ONE ) * 0.5
			var uv3:Vector2 = ( Vector2(v3.x/radius_outer,v3.z/radius_outer) + Vector2.ONE ) * 0.5
			var uv_cntr:Vector2 = ( Vector2(center.x/radius_outer,center.z/radius_outer) + Vector2.ONE ) * 0.5

			match uv_unwrap:
				euv_unwrap.RADIAL:
					uv_cntr = Vector2((i+j)*0.5/definition,0)
					uv0 = Vector2(i*1.0/definition,1)
					uv1 = Vector2(j*1.0/definition,1)
					uv2 = Vector2(j*1.0/definition,0)
					uv3 = Vector2(i*1.0/definition,0)
			
			v0 = rot * v0
			v1 = rot * v1
			v2 = rot * v2
			v3 = rot * v3
			
			if fill:
				
				if radius_inner == 0:
					_surf.add_normal(norm0)
					_surf.add_uv(uv0)
					_surf.add_vertex(v0)
					# V
					_surf.add_normal(norm1)
					_surf.add_uv(uv1)
					_surf.add_vertex(v1)
					# V
					_surf.add_normal(norm_cntr)
					_surf.add_uv(uv_cntr)
					_surf.add_vertex(center)
					
					if center.y != 0 and caps:
						var n:Vector3 = Vector3.UP
						if center.y < 0:
							n *= -1
						_surf.add_normal(n)
						_surf.add_uv(uv1)
						_surf.add_vertex(v1)
						# V
						_surf.add_normal(n)
						_surf.add_uv(uv0)
						_surf.add_vertex(v0)
						# V
						_surf.add_normal(n)
						_surf.add_uv(uv_cntr)
						_surf.add_vertex(center*Vector3(1,0,1))
						
				
				else:
					_surf.add_normal(norm0)
					_surf.add_uv(uv0)
					_surf.add_vertex(v0)
					# V
					_surf.add_normal(norm1)
					_surf.add_uv(uv1)
					_surf.add_vertex(v1)
					# V
					_surf.add_normal(norm2)
					_surf.add_uv(uv2)
					_surf.add_vertex(v2)
					
					_surf.add_normal(norm2)
					_surf.add_uv(uv2)
					_surf.add_vertex(v2)
					# V
					_surf.add_normal(norm3)
					_surf.add_uv(uv3)
					_surf.add_vertex(v3)
					# V
					_surf.add_normal(norm0)
					_surf.add_uv(uv0)
					_surf.add_vertex(v0)
					
					if caps:
						_surf.add_normal(norm0)
						_surf.add_uv(uv0)
						_surf.add_vertex(v0)
						# V
						_surf.add_normal(norm1)
						_surf.add_uv(uv1)
						_surf.add_vertex(v1)
						# V
						_surf.add_normal(norm_cntr)
						_surf.add_uv(uv_cntr)
						_surf.add_vertex(center)
					
			else:
				
				if dash_line > 0 and dash_space > 0:
					if i % (dash_line+dash_space) >= dash_line:
						continue
				
				_surf.add_normal(norm0)
				_surf.add_uv(uv0)
				_surf.add_vertex(v0)
					# V
				_surf.add_normal(norm1)
				_surf.add_uv(uv1)
				_surf.add_vertex(v1)
				
				if radius_inner != 0:
					_surf.add_normal(norm3)
					_surf.add_uv(uv3)
					_surf.add_vertex(v3)
					# V
					_surf.add_normal(norm2)
					_surf.add_uv(uv2)
					_surf.add_vertex(v2)
			
		_surf.index()
		_surf.commit( _mesh )
		
		if _export:
			var d:Directory = Directory.new()
			d.make_dir_recursive(target)
			var mesh_path:String  = target+'polygon_'+str(definition)+'_'
			if dash_line > 0 and dash_space > 0:
				mesh_path += str(dash_line)+'x'+str(dash_space)+'_'
			mesh_path += str(OS.get_unix_time())+'.res'
			ResourceSaver.save( mesh_path, _mesh, ResourceSaver.FLAG_CHANGE_PATH|ResourceSaver.FLAG_REPLACE_SUBRESOURCE_PATHS)
			mesh = load( mesh_path )
		else:
			mesh = _mesh
		set_display(display)

func set_clear(b:bool) -> void:
	mesh = null

func _process(delta):
	pass
