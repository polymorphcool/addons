tool

extends Spatial

const target:String = "res://addons/polymorph/primitives/export/"

enum efont {
	BASIC,
	BEON
}

export(float,0,10) var axis_size:float = 1
export var arrow:bool = true
export(float,0,2) var arrow_height:float = 0.1
export(float,0,2) var arrow_width:float = 0.04

export var signs:bool = true
export(efont) var sign_font:int = efont.BASIC
export var sign_size:float = 0.35
export var sign_offset:float = 0.1

export var generate:bool setget set_generate
export var clear:bool setget set_clear

func axis_material(c:Color) -> SpatialMaterial:
	var mat:SpatialMaterial = SpatialMaterial.new()
	mat.flags_unshaded = true
	mat.albedo_color = c
	return mat

func sign_material(c:Color) -> SpatialMaterial:
	var mat:SpatialMaterial = SpatialMaterial.new()
	mat.flags_unshaded = true
	mat.params_billboard_mode = SpatialMaterial.BILLBOARD_PARTICLES
	mat.albedo_color = c
	return mat

func new_axis( dir:Vector3, mat:SpatialMaterial ) -> MeshInstance:
	var _axis:MeshInstance = MeshInstance.new()
	var _mesh:Mesh = Mesh.new()
	var _surf:SurfaceTool = SurfaceTool.new()
	_surf.begin(Mesh.PRIMITIVE_LINES)
	_surf.add_vertex(Vector3.ZERO)
	_surf.add_vertex(dir*axis_size)
	# arrow
	if arrow:
		var side:Vector3 = Vector3(0,1,0)
		var front:Vector3 = Vector3(0,0,1)
		if dir.y == 1:
			side = Vector3(1,0,0)
		if dir.z == 1:
			front = Vector3(1,0,0)
		var basis:Vector3 = dir * (axis_size-arrow_height)
		_surf.add_vertex( basis + (side+front) * 0.5 * arrow_width )
		_surf.add_vertex(dir*axis_size)
		_surf.add_vertex( basis + (side+front) * 0.5 * arrow_width )
		_surf.add_vertex( basis + (side-front) * 0.5 * arrow_width )
		_surf.add_vertex( basis + (side-front) * 0.5 * arrow_width )
		_surf.add_vertex(dir*axis_size)
		_surf.add_vertex( basis + (side-front) * 0.5 * arrow_width )
		_surf.add_vertex( basis + (-side-front) * 0.5 * arrow_width )
		_surf.add_vertex( basis + (-side-front) * 0.5 * arrow_width )
		_surf.add_vertex(dir*axis_size)
		_surf.add_vertex( basis + (-side-front) * 0.5 * arrow_width )
		_surf.add_vertex( basis + (-side+front) * 0.5 * arrow_width )
		_surf.add_vertex( basis + (-side+front) * 0.5 * arrow_width )
		_surf.add_vertex(dir*axis_size)
		_surf.add_vertex( basis + (-side+front) * 0.5 * arrow_width )
		_surf.add_vertex( basis + (side+front) * 0.5 * arrow_width )
	_surf.index()
	_surf.commit( _mesh )
	_mesh.surface_set_material(0,mat)
	_axis.mesh = _mesh
	var d:Vector3 = dir.normalized()
	_axis.name = str(d.x)+str(d.y)+str(d.z)
	return _axis

func get_char(c:String) -> Array:
	
	if c != 'x' and c != 'y' and c != 'z':
		return []
	
	match sign_font:
		efont.BASIC:
			match c:
				'x':
					return [
						Vector3(-.286,-.5,0),
						Vector3(.286,.5,0),
						Vector3(.286,-.5,0),
						Vector3(-.286,.5,0)
					]
				'y':
					return [
						Vector3(-.286,-.5,0),
						Vector3(.286,.5,0),
						Vector3(-.286,.5,0),
						Vector3(0,0,0)
					]
				'z':
					return [
						Vector3(.286,-.5,0),
						Vector3(-.286,-.5,0),
						Vector3(-.286,-.5,0),
						Vector3(.286,.5,0),
						Vector3(.286,.5,0),
						Vector3(-.286,.5,0)
					]
		efont.BEON:
			match c:
				'x':
					return [
						Vector3(.286,-.5,0),
						Vector3(-.286,.5,0),
						Vector3(-.286,-.5,0),
						Vector3(-.0286,-.05,0),
						Vector3(.286,.5,0),
						Vector3(.0286,.05,0)
					]
				'y':
					return [
						Vector3(.286,.5,0),
						Vector3(0,0,0),
						Vector3(0,0,0),
						Vector3(-.286,.5,0),
						Vector3(0,-.05,0),
						Vector3(0,-.5,0)
					]
				'z':
					return [
						Vector3(-.286,.5,0),
						Vector3(.286,.5,0),
						Vector3(.2574,.45,0),
						Vector3(-.286,-.5,0),
						Vector3(-.25,-.5,0),
						Vector3(.286,-.5,0)
					]
	
	return []

func sign_x() -> MeshInstance:
	var _sign:MeshInstance = MeshInstance.new()
	var _mesh:Mesh = Mesh.new()
	var _surf:SurfaceTool = SurfaceTool.new()
	_surf.begin(Mesh.PRIMITIVE_LINES)
	var vs:Array = get_char('x')
	for v in vs:
		_surf.add_vertex(v * sign_size)
	_surf.index()
	_surf.commit( _mesh )
	_mesh.surface_set_material(0, sign_material(Color(1,0,0)))
	_sign.mesh = _mesh
	_sign.translation = Vector3(1,0,0) * axis_size + Vector3(sign_offset,0,0)
	_sign.name = 'x'
	return _sign

func sign_y() -> MeshInstance:
	var _sign:MeshInstance = MeshInstance.new()
	var _mesh:Mesh = Mesh.new()
	var _surf:SurfaceTool = SurfaceTool.new()
	_surf.begin(Mesh.PRIMITIVE_LINES)
	var vs:Array = get_char('y')
	for v in vs:
		_surf.add_vertex(v * sign_size)
	_surf.index()
	_surf.commit( _mesh )
	_mesh.surface_set_material(0, sign_material(Color(0,1,0)))
	_sign.mesh = _mesh
	_sign.translation = Vector3(0,1,0) * axis_size + Vector3(0,sign_offset,0)
	_sign.name = 'y'
	return _sign

func sign_z() -> MeshInstance:
	var _sign:MeshInstance = MeshInstance.new()
	var _mesh:Mesh = Mesh.new()
	var _surf:SurfaceTool = SurfaceTool.new()
	_surf.begin(Mesh.PRIMITIVE_LINES)
	var vs:Array = get_char('z')
	for v in vs:
		_surf.add_vertex(v * sign_size)
	_surf.index()
	_surf.commit( _mesh )
	_mesh.surface_set_material(0, sign_material(Color(0,0,1)))
	_sign.mesh = _mesh
	_sign.translation = Vector3(0,0,1) * axis_size + Vector3(0,0,sign_offset)
	_sign.name = 'z'
	return _sign

func set_generate(b:bool) -> void:
	if b:
		set_clear(true)
		
		var axis:Spatial = Spatial.new()
		axis.name = 'axis'
		var x_axis:MeshInstance = MeshInstance.new()
		axis.add_child(new_axis( Vector3(1,0,0), axis_material(Color.red) ))
		axis.add_child(new_axis( Vector3(0,1,0), axis_material(Color.green) ))
		axis.add_child(new_axis( Vector3(0,0,1), axis_material(Color.blue) ))
		
		if signs:
			axis.add_child(sign_x())
			axis.add_child(sign_y())
			axis.add_child(sign_z())
		
		for c in axis.get_children():
			c.owner = axis
		
		var ps:PackedScene = PackedScene.new()
		var result = ps.pack(axis)
		if result == OK:
			var d:Directory = Directory.new()
			d.make_dir_recursive(target)
			var axis_path:String  = target+'axis_'+str(OS.get_unix_time())+'.res'
			ResourceSaver.save( axis_path, ps, ResourceSaver.FLAG_CHANGE_PATH|ResourceSaver.FLAG_REPLACE_SUBRESOURCE_PATHS)
			add_child( ps.instance() )
		
func set_clear(b:bool) -> void:
	if b:
		while get_child_count() > 0:
			remove_child(get_child(0))
