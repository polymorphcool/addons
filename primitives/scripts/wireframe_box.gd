tool

extends MeshInstance

const target:String = "res://addons/polymorph/primitives/export/"

enum estyle {
	FULL,
	ONE_LINE,
	TIE_FGHTER,
	ROOF_CEILING
}

export var dimension:Vector3 = Vector3.ONE
export(float,0,10) var gap:float = 0
export(estyle) var style:int = estyle.FULL

export var generate:bool setget set_generate
export var clear:bool setget set_clear

var _surf:SurfaceTool = null

func add_edge( v0:Vector3, v1:Vector3 ) -> void:
	if gap == 0:
		_surf.add_vertex(v0)
		_surf.add_vertex(v1)
	else:
		_surf.add_vertex(v0)
		_surf.add_vertex(v0+(v1-v0).normalized()*0.5*(1-gap))
		_surf.add_vertex(v1+(v0-v1).normalized()*0.5*(1-gap))
		_surf.add_vertex(v1)

func set_generate(b:bool) -> void:
	
	if b:
		
		var m:Vector3 = dimension*.5
		var _mesh:Mesh = Mesh.new()
		_surf = SurfaceTool.new()
		_surf.begin(Mesh.PRIMITIVE_LINES)
		
		var style_name:String = ''
		
		match style:
			estyle.FULL:
				style_name = "full"
				add_edge( Vector3(1,	1,	1) * m, 	Vector3(-1,	1,	1) * m )
				add_edge(  Vector3(1,	-1,	1) * m, 	Vector3(-1,	-1,	1) * m )
				
				add_edge( Vector3(1,	1,	-1) * m,	Vector3(-1,	1,	-1) * m )
				add_edge( Vector3(1,	-1,	-1) * m,	Vector3(-1,	-1,	-1) * m )
				
				add_edge( Vector3(1,	1,	1) * m,		Vector3(1,	-1,	1) * m )
				add_edge( Vector3(-1,	1,	1) * m,		Vector3(-1,	-1,	1) * m )
				
				add_edge( Vector3(1,	1,	-1) * m,	Vector3(1,	-1,	-1) * m )
				add_edge( Vector3(-1,	1,	-1) * m ,	Vector3(-1,	-1,	-1) * m )
				
				add_edge( Vector3(1,	1,	1) * m, 	Vector3(1,	1,	-1) * m )
				add_edge( Vector3(-1,	1,	1) * m,		Vector3(-1,	1,	-1) * m )
				
				add_edge( Vector3(1,	-1,	1) * m,		Vector3(1,	-1,	-1) * m )
				add_edge( Vector3(-1,	-1,	1) * m,		Vector3(-1,	-1,	-1) * m )
			
			estyle.ONE_LINE:
				style_name = "one_line"
				add_edge( Vector3(1,	1,	1) * m,		Vector3(-1,	1,	1) * m )
				add_edge( Vector3(1,	-1,	1) * m,		Vector3(-1,	-1,	1) * m )
				
				add_edge( Vector3(1,	1,	-1) * m,	Vector3(-1,	1,	-1) * m )
				add_edge( Vector3(1,	-1,	-1) * m,	Vector3(-1,	-1,	-1) * m )
				
				add_edge( Vector3(1,	1,	1) * m,		Vector3(1,	-1,	1) * m )

				add_edge( Vector3(1,	1,	-1) * m,	Vector3(1,	-1,	-1) * m )
				
				add_edge( Vector3(-1,	1,	1) * m,		Vector3(-1,	1,	-1) * m )

				add_edge( Vector3(-1,	-1,	1) * m,		Vector3(-1,	-1,	-1) * m )
			
			estyle.TIE_FGHTER:
				style_name = "tie_fighter"
				add_edge( Vector3(1,	1,	1) * m,		Vector3(1,	-1,	1) * m )
				add_edge( Vector3(-1,	1,	1) * m,		Vector3(-1,	-1,	1) * m )
				
				add_edge( Vector3(1,	1,	-1) * m,	Vector3(1,	-1,	-1) * m )
				add_edge( Vector3(-1,	1,	-1) * m,	Vector3(-1,	-1,	-1) * m )
				
				add_edge( Vector3(1,	1,	1) * m,		Vector3(1,	1,	-1) * m )
				add_edge( Vector3(-1,	1,	1) * m,		Vector3(-1,	1,	-1) * m )
				
				add_edge( Vector3(1,	-1,	1) * m,		Vector3(1,	-1,	-1) * m )
				add_edge( Vector3(-1,	-1,	1) * m,		Vector3(-1,	-1,	-1) * m )
			
			estyle.ROOF_CEILING:
				style_name = "roof_ceiling"
				add_edge( Vector3(1,	1,	1) * m,		Vector3(-1,	1,	1) * m )
				add_edge( Vector3(1,	-1,	1) * m,		Vector3(-1,	-1,	1) * m )

				add_edge( Vector3(1,	1,	-1) * m,	Vector3(-1,	1,	-1) * m )
				add_edge( Vector3(1,	-1,	-1) * m,	Vector3(-1,	-1,	-1) * m )
				
				add_edge( Vector3(1,	1,	1) * m,		Vector3(1,	1,	-1) * m )
				add_edge( Vector3(-1,	1,	1) * m,		Vector3(-1,	1,	-1) * m )
				
				add_edge( Vector3(1,	-1,	1) * m,		Vector3(1,	-1,	-1) * m )
				add_edge( Vector3(-1,	-1,	1) * m,		Vector3(-1,	-1,	-1) * m )
		
		_surf.index()
		_surf.commit( _mesh )
		
		var d:Directory = Directory.new()
		d.make_dir_recursive(target)
		var mesh_path:String  = target+'box_'+style_name+'_' + str(OS.get_unix_time())+'.res'
		ResourceSaver.save( mesh_path, _mesh, ResourceSaver.FLAG_CHANGE_PATH|ResourceSaver.FLAG_REPLACE_SUBRESOURCE_PATHS)
		mesh = load( mesh_path )

func set_clear(b:bool) -> void:
	if b:
		mesh = null
