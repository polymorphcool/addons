tool

extends Spatial

enum BAKE_TYPE {
	NONE,
	ALL,
	MOVE_STANDARD,
	MOVE_FIGHTING,
	FLAG_STANDARD,
	SPECIALS
}

const target:String = "res://addons/polymorph/animation_baker/export/"
const preview_variant_shader:String = "res://addons/polymorph/animation_baker/shaders/preview_variant_player.shader"
const preview_animation_shader:String = "res://addons/polymorph/animation_baker/shaders/preview_animation_player.shader"
const preview_slice_shader:String = "res://addons/polymorph/animation_baker/shaders/preview_slice_player.shader"
const preview_full_shader:String = "res://addons/polymorph/animation_baker/shaders/preview_full_player.shader"
const random_texture:String = "res://addons/polymorph/animation_baker/textures/random.res"

export(BAKE_TYPE) var type:int = 0
export var source:PackedScene = null setget set_source
# process controls
export var merge_uvs:bool = false
export var merge_normals:bool = true
export var export_full:bool = true
export var export_slices:bool = false
export var export_animations:bool = false
export var export_variants:bool = false
export var export_flat:bool = false
export var export_exr:bool = false
export var export_avatar:bool = true
export var bake:bool = false setget set_bake
export var clear:bool = false setget set_clear
# animation controls
export var speed:float = 1 setget set_speed
export var fps:float = 25 setget set_fps

var full_texture:Dictionary = {}
var slices:Array = []
# json
var info:Dictionary = {}

var process_mesh:Mesh = null
var optimised_mesh:Mesh = null
var optimised_mesh_path:String = ''
var unique_vertices:Array = []
var unique_target:String = ''
var unique_target_export_exr:String = ''
var unique_target_flat:String = ''
var unique_target_opt:String = ''

func set_source(r:PackedScene) -> void:
	source = r
	if is_inside_tree():
		set_clear(true)

func set_bake(b:bool) -> void:
	bake = false
	if b:
		set_source(source)
		bake_all()

func set_clear(b:bool) -> void:
	clear = false
	if b:
		process_mesh = null
		$preview.mesh = process_mesh
		clear_bake()

func set_speed( f:float ) -> void:
	speed = f
	if is_inside_tree() and $display.get_child_count() > 0:
		for c in $display/avatars.get_children():
			if c is MeshInstance and c.material_override is ShaderMaterial:
				c.material_override.set_shader_param( "speed", speed )
		if $preview.material_override is ShaderMaterial:
			$preview.material_override.set_shader_param( "speed", speed )

func set_fps( f:float ) -> void:
	fps = f
	if is_inside_tree() and $display.get_child_count() > 0:
		for c in $display/avatars.get_children():
			if c is MeshInstance and c.material_override is ShaderMaterial:
				c.material_override.set_shader_param( "fps", fps )
		if $preview.material_override is ShaderMaterial:
			$preview.material_override.set_shader_param( "fps", fps )

### PROCESS

func clear_bake() -> void:
	info = {}
	full_texture = {}
	slices = []
	$tester.material_override.set_shader_param( "animation", null )
	$tester.material_override.set_shader_param( "animation_size", Vector2.ONE )
	$tester.mesh = null
	$preview.reset()
	$preview.material_override = SpatialMaterial.new()
	while $display.get_child_count() != 0:
		$display.remove_child($display.get_child(0))

func get_closest_pow2(f:float) -> float:
	var out:float = 2
	while out < f:
		out *= 2
	return out

func get_smallest_size(num:int) -> Vector2:
	var out:Vector2 = Vector2(2,2)
	var t:int = out.x*out.y
	while t < num:
		if out.x == out.y:
			out.x *= 2
		else:
			out.y *= 2
		t = out.x*out.y
	out.y = ceil( num / out.x );
	return out

func seek_process_mesh( n:Node ) -> void:
	if n is MeshInstance:
		var m:Mesh  = n.mesh
		if m != null:
			if m.get_blend_shape_count() > 0:
				process_mesh = m
				return
	for c in n.get_children():
		seek_process_mesh(c)

func new_vertex() -> Dictionary:
	return {
		'id': 0,
		'indices': [],
		'vertex': null,
		'normal': null,
		'uv': null,
		'tangent': null,
		'color': null,
	}

func add_vertex(surface:Array,vi:int) -> void:
	
	var vertex:Vector3 = surface[Mesh.ARRAY_VERTEX][vi]
	var normal = null
	if surface[Mesh.ARRAY_NORMAL] != null:
		normal = surface[Mesh.ARRAY_NORMAL][vi]
	var uv = null
	if surface[Mesh.ARRAY_TEX_UV] != null:
		uv = surface[Mesh.ARRAY_TEX_UV][vi]
	
	var vid:int = -1
	for i in range(0,unique_vertices.size()):
		var v:Dictionary = unique_vertices[i]
		if v.vertex == vertex:
			vid = i
			if !merge_normals and normal != null and v.normal != normal:
				vid = -1
			if !merge_uvs and uv != null and v.uv != uv:
				vid = -1
			break
	
	if vid == -1:
		var vs:Dictionary = new_vertex()
		vs.id = unique_vertices.size()
		vs.indices.append(vi)
		vs.vertex = vertex
		vs.normal = normal
		vs.uv = uv
		if surface[Mesh.ARRAY_TANGENT] != null:
			vs.tangent = [
				surface[Mesh.ARRAY_TANGENT][vi*4],
				surface[Mesh.ARRAY_TANGENT][vi*4+1],
				surface[Mesh.ARRAY_TANGENT][vi*4+2],
				surface[Mesh.ARRAY_TANGENT][vi*4+3] ]
		if surface[Mesh.ARRAY_COLOR] != null:
			vs.color = surface[Mesh.ARRAY_COLOR][vi]
		unique_vertices.append(vs)
	else:
		unique_vertices[vid].indices.append(vi)
		if normal != null:
			unique_vertices[vid].normal += normal

func new_animation_variant( name:String, filename:String ) -> Dictionary:
	return {
		'name': name,
		'filename': filename,
		'list': [],
		'arrays': [],
		'fps': 0,
		'total_pixels':0
	}

func new_animation( slice:String, node ) -> Dictionary:
	
	var a_info = {
		'node': null,
		'name': '',
		'variants': [],
		'fps': fps,
		'custom_fps': {},
		'clone': false,
		'total_pixels':0
	}
	
	# creation of an empty slice
	if node is String:
		a_info.name = node
		for s in slices:
			if s.name == slice:
				s.animations.append(a_info)
				return a_info
		slices.append({
			'name': slice,
			'animations': [a_info]
		})
		return a_info
	
	# creation of a slide based on template
	elif node is Node:
		a_info.node = node
		a_info.name = node.name
		for p in node.get_property_list():
			if p.name == 'variants_fps':
				a_info.fps = node.variants_fps
			elif p.name == 'variants_custom_fps':
				a_info.custom_fps = node.variants_custom_fps
			elif p.name == 'clone':
				a_info.node = node.get_node( node.clone )
				if a_info.node == null:
					printerr( "Invalid cloning path in ", node.name, ", clone path: ", node.clone )
					return {}
				a_info.name = a_info.node.name
				a_info.clone = true
				# just to be certain a clone can not be used as a common animation
				a_info.variants = null
				a_info.fps = null
				a_info.custom_fps = null
				a_info.total_pixels = null
				
		for s in slices:
			if s.name == slice:
				s.animations.append(a_info)
				return a_info
		slices.append({
			'name': slice,
			'animations': [a_info]
		})
		return a_info
	
	else:
		return {}

func parse_mesh() -> void:
	
	if process_mesh == null:
		return
	var surface:Array = process_mesh.surface_get_arrays(0)
	
	# loading unique vertices
	unique_vertices = []
	for i in range(0,surface[Mesh.ARRAY_VERTEX].size()):
		add_vertex(surface,i)
	
	info.mesh = {
		"vertex": unique_vertices.size(),
		"blendshapes" : {}
	}
	
	info.mesh.blendshapes.used = []
	info.mesh.blendshapes.unused = []
	
	# searching shape keys
	for i in process_mesh.get_blend_shape_count():
		info.mesh.blendshapes.unused.append(process_mesh.get_blend_shape_name(i))
	
	info.mesh.blendshapes.valids = info.mesh.blendshapes.used.size()
	info.mesh.blendshapes.total = process_mesh.get_blend_shape_count()

func load_slices() -> void:
	
	# searching shape keys
	var unused:Array = []
	for bsn in info.mesh.blendshapes.unused:
		var trgt:Dictionary = {}
		for slice in slices:
			for anim in slice.animations:
				# avoiding clones
				if anim.clone:
					continue
				# searching the best match
				if bsn.find(anim.name) != -1:
					if trgt.empty() or len(anim.name) > len(trgt.name):
						trgt = anim
		if !trgt.empty():
			# creation of variants
			var basis:String = bsn.substr(bsn.find(trgt.name))
			basis = basis.substr(0,len(basis)-4)
			var variant:Dictionary = {}
			for v in trgt.variants:
				if v.name == basis:
					variant = v
					break
			if variant.empty():
				var vname:String = bsn.substr(0,len(bsn)-5)
				variant = new_animation_variant( basis, vname )
				variant.fps = trgt.fps
				if vname in trgt.custom_fps.keys():
					variant.fps = trgt.custom_fps[vname]
				trgt.variants.append(variant)
			variant.list.append(bsn)
			info.mesh.blendshapes.used.append(bsn)
		else:
			unused.append(bsn)

	info.mesh.blendshapes.valids = info.mesh.blendshapes.used.size()
	info.mesh.blendshapes.unused = unused
	
	# time to retrieve all the animation data
	load_animations()

func group_blendshapes() -> void:
	
	if info.mesh.blendshapes.total == 0:
		return
	
	info.mesh.blendshapes.unused.sort()
	
	var basename:String = ''
	var anim:Dictionary = {}
	var variant:Dictionary = {}
	for bsn in info.mesh.blendshapes.unused:
		var base:String = bsn.substr(0,len(bsn)-5)
		var nonum:String = bsn.substr(0,len(bsn)-4)
		if base != basename:
			anim = new_animation( 'all', base )
			variant = new_animation_variant( base, base )
			anim.variants.append(variant)
			basename = base
		variant.list.append( bsn )
	
	info.mesh.blendshapes.used = info.mesh.blendshapes.unused
	info.mesh.blendshapes.valids = info.mesh.blendshapes.used.size()
	info.mesh.blendshapes.unused = []
	
	# time to retrieve all the animation data
	load_animations()
	
func load_animations() -> void:
	
	var all_animations:Array = process_mesh.surface_get_blend_shape_arrays(0)
	# loading animations and validation of slices with content 
	var valid_slices:Array = []
	for slice in slices:
		var valid_animations:Array = []
		for anim in slice.animations:
			# avoiding clones
			if anim.clone:
				valid_animations.append( anim )
				continue
			# this animation will not be baked...
			if anim.variants.size() == 0:
				continue
			for v in anim.variants:
				v.list.sort()
				# load blendshape data
				for bsname in v.list:
					for i in process_mesh.get_blend_shape_count():
						var bsn:String = process_mesh.get_blend_shape_name(i)
						if bsn == bsname:
							var vv:PoolVector3Array = all_animations[i][Mesh.ARRAY_VERTEX]
							var vn:PoolVector3Array = all_animations[i][Mesh.ARRAY_NORMAL]
							v.arrays.append({
								'vertex':vv,
								'normal':vn
							})
							v.total_pixels += vv.size() + vn.size()
							break
				anim.total_pixels += v.total_pixels
			if anim.total_pixels > 0:
				valid_animations.append( anim )
		if !valid_animations.empty():
			slice.animations = valid_animations
			valid_slices.append( slice )
	slices = valid_slices
	
	# second validation: need to verify that clones are ok
	valid_slices = []
	for slice in slices:
		var valid_animations:Array = []
		for anim in slice.animations:
			if anim.clone:
				var ref:Dictionary = get_animation( anim.node )
				if !ref.empty():
					valid_animations.append(anim)
			else:
				valid_animations.append(anim)
		if !valid_animations.empty():
			slice.animations = valid_animations
			valid_slices.append( slice )
	slices = valid_slices
#	for s in slices:
#		print( 'slice: ', s.name )
#		for a in s.animations:
#			if a.clone:
#				var refs:Dictionary = get_slice( a.node )
#				var refa:Dictionary = get_animation( a.node )
#				if refs != s:
#					print( '\t', 'animation: ', a.name, ' [cloned, other slice!]' )
#				else:
#					print( '\t', 'animation: ', a.name, ' [cloned, same slice]' )
#				print( '\t\t', 'total variants: ', refa.variants.size() )
#				print( '\t\t', 'total frames: ', refa.total_pixels * 0.5 / unique_vertices.size() )
#				print( '\t\t', 'total pixels: ', refa.total_pixels )
#			else:
#				print( '\t', 'animation: ', a.name )
#				print( '\t\t', 'total variants: ', a.variants.size() )
#				print( '\t\t', 'total frames: ', a.total_pixels * 0.5 / unique_vertices.size() )
#				print( '\t\t', 'total pixels: ', a.total_pixels )
	pass

func get_slice( node ) -> Dictionary:
	for s in slices:
		for a in s.animations:
			if a.clone:
				continue
			if a.node == node:
				return s
	return {}

func get_animation( node ) -> Dictionary:
	for s in slices:
		for a in s.animations:
			if a.clone:
				continue
			if a.node == node:
				return a
	return {}

func build_mesh() -> void:
	
	var st:SurfaceTool = SurfaceTool.new()
	st.begin(Mesh.PRIMITIVE_TRIANGLES)
	# storing vertices
	for v in unique_vertices:
		if v.normal != null:
			st.add_normal(v.normal.normalized())
		if v.tangent != null:
			st.add_tangent(Plane(v.tangent[0],v.tangent[1],v.tangent[2],v.tangent[3]))
		if v.color != null:
			st.add_color(v.color)
		if v.uv != null:
			st.add_uv(v.uv)
		st.add_uv2(
			Vector2(
			v.id*1.0/get_closest_pow2(unique_vertices.size()),		# flat UV
			v.id*1.0												# opt UV
			))
		st.add_vertex(v.vertex)
	# rebuilding triangles
	var surface:Array = process_mesh.surface_get_arrays(0)
	for i in surface[Mesh.ARRAY_INDEX]:
		for v in unique_vertices:
			if i in v.indices:
				st.add_index( v.id )
	# finishing the mesh
	var newm:Mesh = Mesh.new()
	st.commit( newm )
	var arrays = newm.surface_get_arrays(0)
	optimised_mesh_path = unique_target+'mesh.res'
	ResourceSaver.save(optimised_mesh_path, newm, ResourceSaver.FLAG_CHANGE_PATH|ResourceSaver.FLAG_REPLACE_SUBRESOURCE_PATHS)
	var res:Resource = load(optimised_mesh_path)
	res.emit_changed()
	$preview.mesh = load(optimised_mesh_path)
	info.mesh.path = optimised_mesh_path

func build_animations() -> void:
	
	info.slices = []
	var baked_variants:int = 0
	var total_variants:int = 0
	var baked_animations:int = 0
	var total_animations:int = 0
	var baked_slices:int = 0
	var total_slices:int = 0
	var ox:int = 0
	var oy:int = 0
	
	var flat_width:int = get_closest_pow2(unique_vertices.size())
	
	# full animation sheet
	var full_total_pixels:int = 0
	var full_opt_size:Vector2 = Vector2.ZERO
	var full_opt_index:int = 0
	var full_opt:Image = null
	
	for slice in slices:
		var valid_animations:Array = []
		for anim in slice.animations:
			if !anim.clone:
				full_total_pixels += anim.total_pixels
				total_variants += anim.variants.size()
			total_animations += 1
	total_slices = slices.size()
	
	if export_full:
		full_total_pixels += total_slices # << space to store slice info
		full_total_pixels += total_animations # << space to store animations info
		full_total_pixels += total_variants # << space to store variants info
		# generation of a full texture
		full_opt_size = get_smallest_size( full_total_pixels )
		full_opt = Image.new()
		full_opt.create( full_opt_size.x, full_opt_size.y, false, Image.FORMAT_RGBF )
		full_opt.lock()
		# creation of texture summary
		# first pixel 0,0
		# RED: total vertex count
		# GREEN: slices count
		full_opt.set_pixel( 0,0, Color( 
			unique_vertices.size(), 
			total_slices, 
			0 ) )
		full_opt_index += 1
		# creation of the full texture summary
		var full_animation_offset:int = 1 + total_slices
		var full_variant_offset:int = 1 + total_slices + total_animations
		var full_frame_offset:int = 1 + total_slices + total_animations + total_variants
		var full_pixels:Array = []
		for slice in slices:
			# slice info
			# RED: offset for slice animations
			# GREEN: animations count
			full_pixels.append( { 
				'index': full_opt_index, 
				'color': Color( full_animation_offset, slice.animations.size(), 0 ),
				'node': null
				} )
			full_opt_index += 1
			for anim in slice.animations:
				var pixel:Dictionary = { 
					'index': full_animation_offset, 
					'color': null,
					'node': anim.node
				}
				if !anim.clone:
					# animation info
					# RED: offset for animation variants
					# GREEN: variants count
					pixel.color = Color( full_variant_offset, anim.variants.size(), 0 )
				full_pixels.append( pixel )
				full_animation_offset += 1
				if anim.clone:
					continue
				for variant in anim.variants:
					# variant info
					# RED: offset for variant frames
					# GREEN: frames count
					full_pixels.append( { 
						'index': full_variant_offset, 
						'color': Color( full_frame_offset, variant.arrays.size(), variant.fps ),
						'node': null
						} )
					full_variant_offset += 1
					full_frame_offset += variant.total_pixels
		
		# getting color for clones & pushing summary in texture
		for full_pixel in full_pixels:
			if full_pixel.color == null:
				for fp in full_pixels:
					if full_pixel.node == fp.node:
						full_pixel.color = fp.color
			ox = full_pixel.index % int(full_opt_size.x)
			oy = full_pixel.index / int(full_opt_size.x)
			full_opt.set_pixel( ox, oy, full_pixel.color )
		
		# positioning index to first frame pixel
		full_opt_index = 1 + total_slices + total_animations + total_variants
		info.full = {
			'size': full_opt_size,
			'slices': total_slices,
			'animations': total_animations,
			'variants': total_variants
		}
	
	for slice in slices:
		
		var slice_info:Dictionary = {
			'name': slice.name,
			'animations': [],
			'animation_count': 0,
			'variant_count': 0,
		}
		
		# generation of a texture for this slice
		var slice_prefix:String = '' # first 4 chars of the first variant
		var slice_total_pixels:int = 1 # << pixel to store slice info
		var slice_opt_index:int = 0
		var slice_opt_size:Vector2 = Vector2.ZERO
		var slice_opt:Image = null
		var slice_variant_offset:int = 0
		
		if export_slices:
			# creation of the slice texture summary
			var slice_animation_count:int = 0
			for anim in slice.animations:
				if anim.clone:
					continue
				slice_total_pixels += 1 # << space to store animation info
				slice_animation_count += 1
				slice_total_pixels += anim.total_pixels
				for variant in anim.variants:
					if slice_prefix == '':
						slice_prefix += variant.filename.substr(0,4)
					slice_total_pixels += 1 # << space to store variant info
			
			slice_opt_size = get_smallest_size( slice_total_pixels )
			slice_opt = Image.new()
			slice_opt.create( slice_opt_size.x, slice_opt_size.y, false, Image.FORMAT_RGBF )
			slice_opt.lock()
			# creation of texture summary
			# first pixel 0,0
			# RED: total vertex count
			# GREEN: animations count
			slice_opt.set_pixel( 0,0, Color( 
				unique_vertices.size(), 
				slice_animation_count, 
				0 ) )
			slice_opt_index += 1
			slice_variant_offset = 1 + slice_animation_count
			for anim in slice.animations:
				if anim.clone:
					continue
				ox = slice_opt_index % int(slice_opt_size.x)
				oy = slice_opt_index / int(slice_opt_size.x)
				# RED: 		pixel offset
				# GREEN: 	variants count
				slice_opt.set_pixel( ox,oy, Color( 
					slice_variant_offset,
					anim.variants.size(),
					0 ) )
				slice_variant_offset += anim.variants.size()
				slice_opt_index += 1
			# storing variants info
			for anim in slice.animations:
				if anim.clone:
					continue
				for variant in anim.variants:
					ox = slice_opt_index % int(slice_opt_size.x)
					oy = slice_opt_index / int(slice_opt_size.x)
					# RED: 		pixel offset
					# GREEN: 	frame count
					slice_opt.set_pixel( ox, oy, Color( 
						slice_variant_offset,
						variant.arrays.size(),
						variant.fps ) )
					slice_variant_offset += variant.total_pixels
					slice_opt_index += 1
			# slice info are now stored in texture
		
		for anim in slice.animations:
			
			slice_info.animation_count += 1
			
			if anim.clone:
				continue
			
			var anim_total_pixels:int = 1
			var anim_opt_index:int = 0
			var anim_opt_size:Vector2 = Vector2.ZERO
			var anim_opt:Image = null
			var anim_opt_px_offset:int = 0
			
			if export_animations:
				# generation of a texture with all variants
				for variant in anim.variants:
					anim_total_pixels += variant.total_pixels
					anim_total_pixels += 1 # space to store variant info
				anim_opt_size = get_smallest_size( anim_total_pixels )
				anim_opt = Image.new()
				anim_opt.create( anim_opt_size.x, anim_opt_size.y, false, Image.FORMAT_RGBF )
				anim_opt.lock()
				# creation of texture summary
				# first pixel 0,0
				# RED: total vertex count
				# GREEN: variant count in
				anim_opt.set_pixel( 0,0, Color( 
					unique_vertices.size(), 
					anim.variants.size(), 
					0 ) )
				# starting at pixel 1
				anim_opt_index += 1
				# computing pixel offset where each variant begins
				anim_opt_px_offset = 1 + anim.variants.size()
				for variant in anim.variants:
					ox = anim_opt_index % int(anim_opt_size.x)
					oy = anim_opt_index / int(anim_opt_size.x)
					# pixel 1 + variants index
					# RED: 		pixel offset
					# GREEN: 	frame count
					anim_opt.set_pixel( ox, oy, Color( 
						anim_opt_px_offset, 
						variant.arrays.size(),
						variant.fps ) )
					# each variant will store vertex and normal for all vertices
					anim_opt_px_offset += variant.total_pixels
					# next pixel
					anim_opt_index += 1
			
			var single_flat:Image = null
			var single_flat_size:Vector2 = Vector2.ZERO
			var single_flat_path:String = ''
			
			var single_opt:Image = null
			var single_opt_size:Vector2 = Vector2.ZERO
			var single_opt_path:String = ''
			var single_opt_index:int = 0
			var variants_info:Array = []
			
			# generation of each variant texture
			for variant in anim.variants:
				
				slice_info.variant_count += 1
				
				if export_variants:
					# searching for best texture size
					single_opt_size = get_smallest_size( variant.total_pixels + 1 )
					single_opt = Image.new()
					single_opt.create( single_opt_size.x, single_opt_size.y, false, Image.FORMAT_RGBF )
					single_opt.lock()
					# total vertex count in RED, frame count in GREEN
					single_opt.set_pixel( 0,0, Color( unique_vertices.size(), variant.arrays.size(), variant.fps ) )
					single_opt_index = 1
					# flat texture
					if export_flat:
						single_flat_size = Vector2( flat_width, variant.arrays.size()*2 )
						single_flat = Image.new()
						single_flat.create( single_flat_size.x, single_flat_size.y, false, Image.FORMAT_RGBF )
						single_flat.lock()
				
				for y in range(0,variant.arrays.size()):
					
					var data:Dictionary = variant.arrays[y]
					
					for x in range(0,unique_vertices.size()):
						
						var uniquev:Dictionary = unique_vertices[x]
						var vertex:Vector3 = data.vertex[uniquev.indices[0]] - uniquev.vertex
						var normal:Vector3 = data.normal[uniquev.indices[0]]
						
						if export_variants:
							# pushing variant optimised pixels
							ox = single_opt_index % int(single_opt_size.x)
							oy = single_opt_index / int(single_opt_size.x)
							single_opt.set_pixel( ox, oy, Color(vertex.x,vertex.y,vertex.z) )
							single_opt_index += 1
							ox = single_opt_index % int(single_opt_size.x)
							oy = single_opt_index / int(single_opt_size.x)
							single_opt.set_pixel( ox, oy, Color(normal.x,normal.y,normal.z) )
							single_opt_index += 1
							if export_flat:
								# pushing variant flat pixels
								var yy = y*2
								single_flat.set_pixel( x, yy, Color(vertex.x,vertex.y,vertex.z) )
								single_flat.set_pixel( x, yy+1, Color(normal.x,normal.y,normal.z) )
						
						if export_animations:
							# pushing animation optimised pixels
							ox = anim_opt_index % int(anim_opt_size.x)
							oy = anim_opt_index / int(anim_opt_size.x)
							anim_opt.set_pixel( ox, oy, Color(vertex.x,vertex.y,vertex.z) )
							anim_opt_index += 1
							ox = anim_opt_index % int(anim_opt_size.x)
							oy = anim_opt_index / int(anim_opt_size.x)
							anim_opt.set_pixel( ox, oy, Color(normal.x,normal.y,normal.z) )
							anim_opt_index += 1
						
						if export_slices:
							# pushing slice optimised pixels
							ox = slice_opt_index % int(slice_opt_size.x)
							oy = slice_opt_index / int(slice_opt_size.x)
							slice_opt.set_pixel( ox, oy, Color(vertex.x,vertex.y,vertex.z) )
							slice_opt_index += 1
							ox = slice_opt_index % int(slice_opt_size.x)
							oy = slice_opt_index / int(slice_opt_size.x)
							slice_opt.set_pixel( ox, oy, Color(normal.x,normal.y,normal.z) )
							slice_opt_index += 1
						
						if export_full:
							# pushing full optimised pixels
							ox = full_opt_index % int(full_opt_size.x)
							oy = full_opt_index / int(full_opt_size.x)
							full_opt.set_pixel( ox, oy, Color(vertex.x,vertex.y,vertex.z) )
							full_opt_index += 1
							ox = full_opt_index % int(full_opt_size.x)
							oy = full_opt_index / int(full_opt_size.x)
							full_opt.set_pixel( ox, oy, Color(normal.x,normal.y,normal.z) )
							full_opt_index += 1
				
				var vinfo:Dictionary = {
					'name' : variant.name,
					'frames': variant.arrays.size()
				}
				
				if export_variants:
					single_opt.unlock()
					single_opt_path = unique_target_opt + variant.filename + '.opt.res'
					variant.optimised = {
						'path': single_opt_path,
						'size': single_opt_size
					}
					vinfo.optimised = {
						'path': single_opt_path,
						'size': single_opt_size
					}
					var oit:ImageTexture = ImageTexture.new()
					oit.create_from_image( single_opt, 0 )
					ResourceSaver.save( single_opt_path, oit )
					if export_exr:
						var export_exrp:String = unique_target_export_exr + variant.filename + '.opt.exr'
						single_opt.save_exr( export_exrp )
						variant.optimised.exr = export_exrp
						vinfo.optimised.exr = export_exrp
					if export_flat:
						single_flat.unlock()
						single_flat_path = unique_target_flat + variant.filename + '.flat.res'
						variant.flat = {
							'path': single_opt_path,
							'size': single_opt_size
						}
						vinfo.flat = {
							'path': single_flat_path,
							'size': single_flat_size
						}
						var it:ImageTexture = ImageTexture.new()
						it.create_from_image( single_flat, Texture.FLAG_REPEAT )
						ResourceSaver.save( single_flat_path, it )
						if export_exr:
							var export_exrp:String = unique_target_export_exr + variant.filename + '.flat.exr'
							single_flat.save_exr( export_exrp )
							variant.flat.exr = export_exrp
							vinfo.flat.exr = export_exrp

				variants_info.append(vinfo)
				
				# releasing memory
				variant.arrays = null
				
				baked_variants += 1
				print( baked_variants, '/', total_variants, ' variant(s) baked' )
			
			var anim_info = {
				'name' : anim.name,
				'variant_count' : anim.variants.size(),
				'variants' : variants_info
			}
			
			if export_animations:
				var anim_opt_path:String = unique_target_opt + anim.variants[0].filename.substr(0,len(anim.variants[0].filename)-3) + '.variants.opt.res'
				anim_info.size = anim_opt_size
				anim_info.path = anim_opt_path
				var aoit:ImageTexture = ImageTexture.new()
				aoit.create_from_image( anim_opt, 0 )
				ResourceSaver.save( anim_opt_path, aoit )
				if export_exr:
					var export_exrp:String = unique_target_export_exr + anim.variants[0].filename.substr(0,len(anim.variants[0].filename)-3) + '.variants.exr'
					anim_opt.save_exr( export_exrp )
					anim_info.exr = export_exrp
				# updating animation for preview
				anim.size = anim_opt_size
				anim.path = anim_opt_path
			
			slice_info.animations.append( anim_info )
			
			baked_animations +=1
			print( baked_animations, '/', total_animations, ' animation(s) baked' )
		
		if export_slices:
			slice_opt.unlock()
			var slice_opt_path = unique_target_opt + slice_prefix + slice.name + '.slice.opt.res'
			var soit:ImageTexture = ImageTexture.new()
			soit.create_from_image( slice_opt, 0 )
			ResourceSaver.save( slice_opt_path, soit )
			if export_exr:
				var export_exrp:String = unique_target_export_exr + slice_prefix + slice.name + '.slice.opt.exr'
				slice_opt.save_exr( export_exrp )
				slice_info.exr = export_exrp
			# pushing in info
			slice_info.size = slice_opt_size
			slice_info.path = slice_opt_path
			# updating slice for preview
			slice.size = slice_opt_size
			slice.path = slice_opt_path
		
		baked_slices += 1
		print( baked_slices, '/', total_slices, ' slice(s) baked' )
		
		info.slices.append( slice_info )
	
	if export_full:
		full_opt.unlock()
		var full_opt_path = unique_target_opt + 'full.opt.res'
		var foit:ImageTexture = ImageTexture.new()
		foit.create_from_image( full_opt, 0 )
		ResourceSaver.save( full_opt_path, foit )
		info.full.path = full_opt_path
		if export_exr:
			var export_exrp:String = unique_target_export_exr + 'full.opt.exr'
			full_opt.save_exr( export_exrp )
			info.full.exr = export_exrp
		full_texture = {
			'path': full_opt_path,
			'size': full_opt_size
		}

func _ready() -> void:
	set_clear(true)

func bake_all() -> void:
	
	clear_bake()
	
	if type == 0:
		return
	
	unique_target = target + str(OS.get_unix_time()) + '/'
	unique_target_opt = unique_target + 'opt/'
	var d:Directory = Directory.new()
	d.make_dir_recursive(unique_target)
	if export_flat:
		unique_target_flat = unique_target + 'flat/'
		d.make_dir_recursive(unique_target_flat)
	if export_exr:
		unique_target_export_exr = unique_target + 'exr/'
		d.make_dir_recursive(unique_target_export_exr)
	d.make_dir_recursive(unique_target_opt)
	
	var ci:int = type
	var bake_all:bool = ci == BAKE_TYPE.ALL
	var dname:String = ''
	match ci:
		BAKE_TYPE.MOVE_STANDARD:
			dname = 'MOVE_STANDARD'
		BAKE_TYPE.MOVE_FIGHTING:
			dname = 'MOVE_FIGHTING'
		BAKE_TYPE.FLAG_STANDARD:
			dname = 'FLAG_STANDARD'
		BAKE_TYPE.SPECIALS:
			dname = 'SPECIALS'
	if !bake_all and dname == '':
		return
	
	info = {}
	process_mesh = null
	seek_process_mesh(source.instance())
	
	if process_mesh == null:
		return
	
	# analyse mesh
	parse_mesh()
	print( 'mesh parsing done' ) 
	print( '\tunique vertices: ', unique_vertices.size() )
	
	full_texture = {}
	slices = []
	
	var template:Spatial = null
	if !bake_all:
		template = $types.get_node(dname).duplicate()
		$display.add_child( template )
		template.visible = true
		template.owner = $display.owner
		var ks:Spatial = template.get_node("slices")
		if ks == null:
			editor_description = "failed to load node 'slices'"
			return
		for slice in ks.get_children():
			if slice.name.begins_with( 'slice_' ):
				for c in slice.get_children():
					new_animation( slice.name.substr(6), c )
		#make connections with type
		load_slices()
	
	else:
		# just grouping blendshapes together
		group_blendshapes()
	
	# recompose a new mesh
	build_mesh()
	print( 'mesh reconstruction done' ) 
	
	# generate all shapekeys animations
	build_animations()
	print( 'animations generated' )
	
	var finfo:File = File.new()
	finfo.open( unique_target + 'info.json', File.WRITE )
	finfo.store_string( JSON.print( info ) )
	finfo.close()
	
	match type:
		BAKE_TYPE.ALL:
			visualise_baking_all()
		_:
			visualise_baking_tmpl( template )

func visualise_baking_all() -> void:
	
	var vinfos:Array = []
	for slice in slices:
		for anim in slice.animations:
			for variant in anim.variants:
				print( variant )
				if 'optimised' in variant:
					var vi:Dictionary = variant.optimised.duplicate(true)
					vi.name = variant.name
					vinfos.append( vi )
	
	var tile_size:int = 2
	var offset:Vector3 = Vector3( ( vinfos.size() - 1 ) * tile_size * -0.5, 0, 0 )
	
	var final_mesh:Mesh = load( info.mesh.path )
	var avatars:Spatial = Spatial.new()
	avatars.name = "avatars"
	$display.add_child( avatars )
	avatars.owner = $display.owner
	
	var sm:ShaderMaterial = ShaderMaterial.new()
	sm.shader = load( preview_variant_shader )
	sm.set_shader_param( "albedo", Color.white )
	sm.set_shader_param( "fps", fps )
	
	var x:int = 0
	for v in vinfos:
		var mi:MeshInstance = MeshInstance.new()
		mi.name = v.name
		avatars.add_child( mi )
		mi.owner = avatars.owner
		mi.global_transform.origin = offset + Vector3( x * tile_size, 0,  0 )
		x += 1
		mi.mesh = final_mesh
		mi.material_override = sm.duplicate()
		mi.material_override.set_shader_param( "animation", load(v.path) )
		mi.material_override.set_shader_param( "animation_size", v.size )

func visualise_baking_tmpl( template:Node )  -> void:
	
	var final_mesh:Mesh = load( info.mesh.path )
	
	var avatars:Spatial = Spatial.new()
	avatars.name = "avatars"
	$display.add_child( avatars )
	avatars.owner = $display.owner
	# common to all materials
	var _rt:ImageTexture = load( random_texture )
	var _rs:Vector2 = _rt.get_size()
	for slice in slices:
		var _ai:ImageTexture = null
		var _as:Vector2 = Vector2.ZERO
		if 'path' in slice:
			_ai = load( slice.path )
			_as = slice.size
		var sgap:float = 0.0
		var r2l:float = 0.0
		if _ai != null and slice.animations.size() > 1:
			sgap = 2.0 / ( slice.animations.size() -1 )
			r2l = -1.0
		for anim in slice.animations:
			if anim.clone:
				continue
			# if no texture available, then nothing to de
			if _ai == null and not 'path' in anim:
				continue
			var mi:MeshInstance = MeshInstance.new()
			mi.name = anim.name
			avatars.add_child( mi )
			mi.owner = avatars.owner
			if anim.node != null:
				mi.global_transform.origin = anim.node.global_transform.origin
			mi.mesh = final_mesh
			var sm:ShaderMaterial = ShaderMaterial.new()
			# smart swap
			if 'path' in anim:
				sm.shader = load( preview_animation_shader )
				sm.set_shader_param( "albedo", Color.white )
				sm.set_shader_param( "animation", load(anim.path) )
				sm.set_shader_param( "animation_size", anim.size )
				sm.set_shader_param( "variant_id", -1 )
				sm.set_shader_param( "random_tex", _rt )
				sm.set_shader_param( "random_size", _rs )
				sm.set_shader_param( "speed", speed )
				sm.set_shader_param( "fps", fps )
			elif _ai != null:
				sm.shader = load( preview_slice_shader )
				sm.set_shader_param( "albedo", Color.white )
				sm.set_shader_param( "animation", _ai )
				sm.set_shader_param( "animation_size", _as )
				sm.set_shader_param( "right_2_left", r2l )
				sm.set_shader_param( "variant_id", -1 )
				sm.set_shader_param( "random_tex", _rt )
				sm.set_shader_param( "random_size", _rs )
				sm.set_shader_param( "speed", speed )
				sm.set_shader_param( "fps", fps )
				r2l += sgap
			# good to go
			mi.material_override = sm
	
	if not full_texture.empty():
		
		var fit:ImageTexture = load(full_texture.path)
		var sm:ShaderMaterial = ShaderMaterial.new()
		sm.shader = load( preview_full_shader )
		sm.set_shader_param( "albedo", Color.white )
		sm.set_shader_param( "animation", fit )
		sm.set_shader_param( "animation_size", full_texture.size )
		sm.set_shader_param( "random_tex", _rt )
		sm.set_shader_param( "random_size", _rs )
		sm.set_shader_param( "speed", speed )
		sm.set_shader_param( "fps", fps )
		
		$preview.mesh = final_mesh
		$preview.material_override = sm
		
		if export_avatar:
			var avatar_path = unique_target+template.name.to_lower()+'.avatar.tscn'
			var avtr:MeshInstance = MeshInstance.new()
			avtr.name = template.name.to_lower()
			avtr.mesh = final_mesh
			avtr.material_override = sm
			var avtr_ps = PackedScene.new()
			avtr_ps.pack(avtr)
			ResourceSaver.save(avatar_path, avtr_ps, ResourceSaver.FLAG_CHANGE_PATH|ResourceSaver.FLAG_REPLACE_SUBRESOURCE_PATHS)
		
		$tester.mesh = final_mesh
		$tester.material_override.set_shader_param( "animation", fit )
		$tester.material_override.set_shader_param( "animation_size", full_texture.size )
		
		$tester.set_slice_a(0)
		$tester.set_anim_a(0)
		$tester.set_variant_a(0)
		$tester.set_slice_b(0)
		$tester.set_anim_b(0)
		$tester.set_variant_b(0)
		$tester.set_transition(0)
		
		$preview/animation_tester.slice_info = []
		for s in slices:
			$preview/animation_tester.slice_info.append( s.animations.size() )
		$preview/animation_tester.generate = true
		$preview/animation_tester.visible = true
		
	else:
		$preview.mesh = null
		$preview.pedestal = null
		$preview.pedestal_material = null
