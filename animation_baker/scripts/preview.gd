tool

extends MeshInstance

export(float,0,1000) var motion_length:float = 1 setget set_motion_length
export(float,0,360) var motion_degree:float = 0 setget set_motion_degree
export(bool) var follow_target:bool = true

var last_target:Vector3 = Vector3.ZERO

func set_motion_length(f:float) -> void:
	motion_length = f
	if is_inside_tree():
		if material_override is ShaderMaterial:
			material_override.set_shader_param( "motion_length", motion_length )
		if $animation_tester.visible:
			$animation_tester/forward.scale = Vector3.ONE * motion_length

func set_motion_degree(f:float) -> void:
	motion_degree = f
	if is_inside_tree():
		if material_override is ShaderMaterial:
			material_override.set_shader_param( "motion_degree", motion_degree )
		if $animation_tester.visible:
			var fwdn:Vector3 = $animation_tester/target.translation.normalized()
			$animation_tester/forward.transform.basis = Basis( Vector3(0,1,0).cross( fwdn ), Vector3(0,1,0), fwdn )
			$animation_tester/forward.scale = Vector3.ONE * motion_length

func reset():
	$animation_tester/target.translation.x = 0
	$animation_tester/target.translation.y = 0
	$animation_tester/target.translation.z = 1
	set_motion_length( 1 )
	set_motion_degree( 0 )
	$animation_tester.slice_info = []
	$animation_tester.generate = true
	$animation_tester.visible = false
	last_target = $animation_tester/target.translation

func _ready() -> void:
	set_motion_length( motion_length )
	set_motion_degree( motion_degree )

func _process(delta) -> void:
	
	if follow_target and mesh != null:
		$ground.material_override.uv1_offset += Vector3(last_target.x,last_target.z,0) * 2 * delta
		if last_target != $animation_tester/target.translation:
			last_target = $animation_tester/target.translation
			var tgt:Vector3 = last_target * Vector3(1,0,1)
			$animation_tester/target.translation = tgt
			var tgtl:float = tgt.length()
			set_motion_length( tgtl )
			if tgtl == 0:
				set_motion_degree(0)
			else:
				tgt /= tgtl
				var a:float = atan2( tgt.z, tgt.x ) - PI * 0.5
				while a < 0:
					a += TAU
				set_motion_degree( a / TAU * 360 )
		
