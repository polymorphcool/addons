tool

extends Spatial

enum FWD_AXIS {
	AXIS_X,
	AXIS_Xi,
	AXIS_Z,
	AXIS_Zi
}

export(Array,int) var slice_info:Array = []
export(FWD_AXIS) var forward:int = FWD_AXIS.AXIS_Z
export(bool) var generate:bool = false setget set_generate

var target_pos:Vector3 = Vector3.ZERO

func new_anim( parent:Spatial, name:String, radius_inner:float, radius_outer:float, start:float, degree:float, c:Color ) -> MeshInstance:
	var a:MeshInstance = $anim_tmpl.duplicate()
	parent.add_child( a )
	a.name = name
	a.owner = parent.owner
	a.radius_outer = radius_outer
	a.radius_inner = radius_inner
	a.start_angle = start
	a.angle = degree
	a.generate = true
	a.visible = true
	a.script = null
	a.material_override = $anim_tmpl.material_override.duplicate()
	a.material_override.set_shader_param( "albedo_0", c )
	var subc:Color = c
	subc.v *= 0.9
	a.material_override.set_shader_param( "albedo_1", subc )
	return a

func set_generate(b:bool) -> void:
	
	generate = false
	
	if b:
		
		while $slices.get_child_count() > 0:
			$slices.remove_child( $slices.get_child(0) )
		
		var tmpl:MeshInstance = $anim_tmpl
		var si:int = 0
			
		for s in slice_info:
			
			var slice:Spatial = Spatial.new()
			$slices.add_child( slice )
			slice.name = 'slice_'+str( si )
			slice.owner = $slices.owner
			slice.translation.y = si * -0.01
			
			var agap:float = 0
			var aprev:float = 0
			var hue:float = 0
			var huegap:float = 1
			agap = 360 / s
			if s > 1:
				if s > 3:
					aprev = agap * -0.5
				huegap = 1.0 / s
			for i in range( 0, s ):
				var c:Color = Color(0,1,0)
				c.h = hue
				var a:MeshInstance = new_anim( 
					slice, 'anim_'+str(i), 
					max( 0, (si-1) ), si + 1,
					aprev, agap, c )
				aprev += agap
				hue += huegap
			si += 1
		
		var r:Vector3 = Vector3.ZERO
		match forward:
			FWD_AXIS.AXIS_X:
				r.y = PI
			FWD_AXIS.AXIS_Z:
				r.y = PI*0.5
			FWD_AXIS.AXIS_Zi:
				r.y = PI*1.5
		for c in $slices.get_children():
			c.rotation = r
