extends MeshInstance

export (float) var variants_fps:float = 25
export (Dictionary) var variants_custom_fps:Dictionary = { 'default':25.0 }
