tool

extends MeshInstance

export(int,0,10) var slice_a:int = 0 		setget set_slice_a
export(int,0,10) var anim_a:int = 0 		setget set_anim_a
export(int,0,30) var variant_a:int = 0 		setget set_variant_a

export(int,0,10) var slice_b:int = 0 		setget set_slice_b
export(int,0,10) var anim_b:int = 0 		setget set_anim_b
export(int,0,30) var variant_b:int = 0 		setget set_variant_b

export(float,0,1) var transition_A_B:float = 0	setget set_transition

export(ImageTexture) var animation:ImageTexture setget set_animation

func set_slice_a(i:int) -> void:
	slice_a = i
	if is_inside_tree() and material_override is ShaderMaterial:
		material_override.set_shader_param( "slice_a", slice_a )

func set_anim_a(i:int) -> void:
	anim_a = i
	if is_inside_tree() and material_override is ShaderMaterial:
		material_override.set_shader_param( "anim_a", anim_a )

func set_variant_a(i:int) -> void:
	variant_a = i
	if is_inside_tree() and material_override is ShaderMaterial:
		material_override.set_shader_param( "variant_a", variant_a )

func set_slice_b(i:int) -> void:
	slice_b = i
	if is_inside_tree() and material_override is ShaderMaterial:
		material_override.set_shader_param( "slice_b", slice_b )

func set_anim_b(i:int) -> void:
	anim_b = i
	if is_inside_tree() and material_override is ShaderMaterial:
		material_override.set_shader_param( "anim_b", anim_b )

func set_variant_b(i:int) -> void:
	variant_b = i
	if is_inside_tree() and material_override is ShaderMaterial:
		material_override.set_shader_param( "variant_b", variant_b )

func set_transition(f:float) -> void:
	transition_A_B = f
	if is_inside_tree() and material_override is ShaderMaterial:
		material_override.set_shader_param( "transition", transition_A_B )

func set_animation( i:ImageTexture ) -> void:
	animation = i
	if is_inside_tree() and material_override is ShaderMaterial:
		material_override.set_shader_param( "animation", animation )
		material_override.set_shader_param( "animation_size", animation.get_size() )

func _ready():
	set_slice_a( slice_a )
	set_anim_a( anim_a )
	set_variant_a( variant_a )
	set_slice_b( slice_b )
	set_anim_b( anim_b )
	set_variant_b( variant_b )
	set_transition( transition_A_B )
	set_animation( animation )
