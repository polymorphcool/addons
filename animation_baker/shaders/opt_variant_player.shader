/*
# optimisation player
this shader shows how to read a texture containing a optimised animation
animation_size has to be specified manually or via script to match the exact size of the texture
shader awaits the data to be organised in this sequence:
	- first pixel contains vertex count on RED channel and frame count on GREEN channel
	- [position, normal] pairs for each vertex in each frame
*/

shader_type spatial;
render_mode blend_mix,depth_draw_opaque,cull_back,diffuse_burley,specular_schlick_ggx;
uniform vec4 albedo : hint_color;

// general params
uniform float speed = 1; 						// time multiplier
uniform float fps = 25;							// animation frame per second
// animation texture specific params
uniform sampler2D animation: hint_black;		// texture containing blendshapes
uniform vec2 animation_size = vec2(1.0);		// exact texture size
uniform int vid : hint_range(0,100);

// extract vertex and normal info for a specific vertex in a specific frame
// interpolation for one frame to the other is linear
void extract_optimised( 
		inout vec3 vert, 
		inout vec3 norm,
		in uint vertex_id,
		in sampler2D anim, 
		in vec2 anim_size, 
		in uint pixel_offset,
		in uint vertex_count,
		in uint frame_count,
		in float time
		) {
	uint asizex = uint(anim_size.x);
	// frame indices computation
	uint frame_0 = uint( time );
	float y_mix = time - float( frame_0 );
	frame_0 %= frame_count;
	// next frame
	uint frame_1 = ( frame_0 + uint(1) ) % frame_count;
	// pixel index containing vertex offset for both frames
	uint pixelid0 = pixel_offset + frame_0*vertex_count*uint(2) + vertex_id*uint(2);
	uint pixelid1 = pixel_offset + frame_1*vertex_count*uint(2) + vertex_id*uint(2);
	// extraction and mix of the vertex offsets
	vert += mix(
		texture( anim, vec2( 0.5 + float(pixelid0%asizex), 0.5 + float(pixelid0/asizex) ) / anim_size ).xyz,
		texture( anim, vec2( 0.5 + float(pixelid1%asizex), 0.5 + float(pixelid1/asizex) ) / anim_size ).xyz,
		y_mix
		);
	// next pixel contains the normal
	pixelid0 += uint(1);
	pixelid1 += uint(1);
	// extraction, mix and normalisation of normals
	norm = normalize( mix(
		texture( anim, vec2( 0.5 + float(pixelid0%asizex), 0.5 + float(pixelid0/asizex) ) / anim_size ).xyz,
		texture( anim, vec2( 0.5 + float(pixelid1%asizex), 0.5 + float(pixelid1/asizex) ) / anim_size ).xyz,
		y_mix
		) );
}

void vertex() {
	
	// same variables for all vertices
	float t = TIME * speed * fps;
	// extracting amount of vertex count and variant count
	vec2 anim_info = texture( animation, vec2(.5) / animation_size ).rg;
	uint vertex_count = uint( anim_info.x );
	uint variant_count = uint( anim_info.y );
	// selecting a variant id
//	int variant_id = int(TIME*0.3) % variant_count;
	uint variant_id = max( uint(0), min( variant_count-uint(1), uint(vid) ) );
	uint variant_pixel = uint(1) + variant_id;
	// extracting variant offset and frame count
	vec2 variant_info = texture( 
		animation, 
		vec2( 
			0.5 + float(variant_pixel%uint(animation_size.x)), 
			0.5 + float(variant_pixel/uint(animation_size.x)) 
		) / animation_size ).rg;
	uint variant_offset = uint(variant_info.x);
	uint variant_frames = uint(variant_info.y);
	// ready to apply animation
	extract_optimised( VERTEX, NORMAL, uint(UV2.y), animation, animation_size, variant_offset, vertex_count, variant_frames, t );

}

void fragment() {
	ALBEDO = albedo.rgb;
	METALLIC = 0.0;
	ROUGHNESS = 1.0;
	SPECULAR = .5;
}
