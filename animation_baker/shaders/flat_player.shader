/*
# animation player
this shader shows how to read a texture containing a single animation
animation_height has to be specified manually or via script to match the exact height of the texture
shader awaits the data to be organised in this sequence:
	- position offsets for each vertex on even lines (starting at 0)
	- normal for each vertex on odd lines
the frames are read from top to bottom (Y axis) and loops depending on animation_frames value
X axis is related to the UV2.x value, different for each vertex
*/

shader_type spatial;
render_mode blend_mix,depth_draw_opaque,cull_back,diffuse_burley,specular_schlick_ggx;
uniform vec4 albedo : hint_color;

// general params
uniform float speed = 1; 						// time multiplier
uniform float fps = 25;							// animation frame per second
// animation texture specific params
uniform sampler2D animation: hint_black;		// texture containing blendshapes
uniform int animation_offset = 0;				// animation starts at pixel (y)
uniform int animation_frames = 10;				// animation ends at pixel (y)
uniform int animation_height = 50;				// total height of animation texture

void extract_frame( 
		inout vec3 vert, 
		inout vec3 norm, 
		in float uvx, 
		in sampler2D anim, 
		in int offset,
		in int frames,
		in int height, 
		in float time ) {
	// frame indices computation
	int frame_0 = int( time );
	float y_mix = time - float( frame_0 );
	frame_0 %= frames;
	int frame_1 = ( frame_0 + 1 ) % frames;
	// applying frame offset
	frame_0 = max(0, min((height/2)-1, frame_0 + offset ));
	frame_1 = max(0, min((height/2)-1, frame_1 + offset ));
	// UV2 preparation
	float y_ratio = 1.0 / float( height );
	float vy_0 = ( float( frame_0 * 2 ) + .5 ) * y_ratio;
	float vy_1 = ( float( frame_1 * 2 ) + .5 ) * y_ratio;
	// getting data from texture & mixing to output
	// vertex
	vert += mix( 
		texture( anim, vec2(uvx, vy_0 ) ).xyz, 
		texture( anim, vec2(uvx, vy_1 ) ).xyz, 
		y_mix );
	// normal
	norm = normalize( mix( 
		texture( anim, vec2(uvx, vy_0 + 1.0 * y_ratio ) ).xyz,
		texture( anim, vec2(uvx, vy_1 + 1.0 * y_ratio ) ).xyz,
		y_mix ));
}

void vertex() {
	float t = TIME * speed * fps;
	extract_frame( 
		VERTEX, NORMAL, UV2.x, // << vertex specific
		animation, 
		animation_offset, 
		animation_frames, 
		animation_height, t );
}

void fragment() {
	ALBEDO = albedo.rgb;
	METALLIC = 0.0;
	ROUGHNESS = 1.0;
	SPECULAR = .5;
}
