shader_type spatial;
render_mode blend_mix,depth_draw_opaque,cull_back,diffuse_burley,specular_schlick_ggx;

const float M_PI = 								3.1415926535897932384626433832795;
// fragment
uniform vec4 albedo : 							hint_color;
// random
uniform sampler2D random_tex : 					hint_black;
uniform float random_pixel_time = 				1;
uniform vec2 random_size = 						vec2(256);
uniform float random_transition = 				0.5;
uniform float random_variation = 				0.5;
// animations
uniform int variant_id =						-1;
uniform float speed = 							1;
uniform float fps = 							25;
uniform sampler2D animation: 					hint_black;
uniform vec2 animation_size = 					vec2(1.0);

varying vec3 variant;
varying float local_y;

// utils
vec3 hsv2rgb(vec3 c) {
  vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
  vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
  return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

// extraction of vertex and normal info from animation texture
void extract_optimised( 
		inout vec3 vert, 
		inout vec3 norm,
		in uint vertex_id,
		in sampler2D anim, 
		in vec2 anim_size, 
		in uint pixel_offset,
		in uint vertex_count,
		in uint frame_count,
		in float time,
		in float framerate
		) {
	uint asizex = uint(anim_size.x);
	// frame indices computation
	float loc_time = time;
	if ( framerate < 0.0 ) {
		loc_time *= float(frame_count);
	} else {
		loc_time *= framerate;
	}
	uint frame_0 = uint( loc_time );
	float y_mix = loc_time - float( frame_0 );

	frame_0 %= frame_count;
	// next frame
	uint frame_1 = ( frame_0 + uint(1) ) % frame_count;
	// pixel index containing vertex offset for both frames
	uint pixelid0 = pixel_offset + frame_0*vertex_count*uint(2) + vertex_id*uint(2);
	uint pixelid1 = pixel_offset + frame_1*vertex_count*uint(2) + vertex_id*uint(2);
	// extraction and mix of the vertex offsets
	vert += mix(
		texture( anim, vec2( 0.5 + float(pixelid0%asizex), 0.5 + float(pixelid0/asizex) ) / anim_size ).xyz,
		texture( anim, vec2( 0.5 + float(pixelid1%asizex), 0.5 + float(pixelid1/asizex) ) / anim_size ).xyz,
		y_mix
		);
	// next pixel contains the normal
	pixelid0 += uint(1);
	pixelid1 += uint(1);
	// extraction, mix and normalisation of normals
	norm = normalize( mix(
		texture( anim, vec2( 0.5 + float(pixelid0%asizex), 0.5 + float(pixelid0/asizex) ) / anim_size ).xyz,
		texture( anim, vec2( 0.5 + float(pixelid1%asizex), 0.5 + float(pixelid1/asizex) ) / anim_size ).xyz,
		y_mix
		) );
}

// extraction of pixel offset and count of an animation
void extract_info( 
	inout uint offset, 
	inout uint count,
	in uint start,
	in uint sID,
	in sampler2D anim, 
	in vec2 anim_size,
	) {
	uint slice_pixel = start + sID;
	vec2 slice_info = texture( 
		anim, 
		vec2( 
			0.5 + float(slice_pixel%uint(anim_size.x)), 
			0.5 + float(slice_pixel/uint(anim_size.x)) 
		) / animation_size ).rg;
	offset = uint(slice_info.x);
	count = uint(slice_info.y);
}

// extraction of variant info: offset, framecount AND framerate
void extract_variant( 
	inout uint offset, 
	inout uint count,
	inout float vfps,
	in uint start,
	in uint ID,
	in sampler2D anim, 
	in vec2 anim_size,
	) {
	uint slice_pixel = start + ID;
	vec3 slice_info = texture( 
		anim, 
		vec2( 
			0.5 + float(slice_pixel%uint(anim_size.x)), 
			0.5 + float(slice_pixel/uint(anim_size.x)) 
		) / animation_size ).rgb;
	offset = uint(slice_info.x);
	count = uint(slice_info.y);
	vfps = slice_info.z;
}

// extraction of values from a repeated random texture
void extract_random( 
	inout vec3 values, 
	in sampler2D tex, 
	in vec2 t_size, 
	in float r_transition, 
	in float r_variation, 
	in float global_t, 
	in float pixel_t 
	) {
	// texture pixel count
	int tw = int(t_size.x);
	int tt = tw * int(t_size.y);
	// local times
	float t0 = global_t / pixel_t;
	values.z = t0 - float( int(t0) );
	// pixel ids
	int pid0 = int(t0) % tt;
	int pid1 = int(t0+1.0) % tt;
	int pid2 = int(t0+t_size.x) % tt;
	// exctracting RED values out of texture
	values.x = texture( tex, vec2( float(pid0%tw), float(pid0/tw) ) / t_size).r;
	values.y = texture( tex, vec2( float(pid1%tw), float(pid1/tw) ) / t_size).r;
	float tv = 0.5 - texture( tex, vec2( float(pid2%tw), float(pid2/tw) ) / t_size).r;
	// mapping transition
	float rt = max( 0.0, min( 0.999,  								// avoiding division by 0!
		r_transition + tv * r_variation 
		) );
	values.z = 
		( sin( M_PI*-.5 + M_PI *									// sinus interpolation
		max( 0.0, min( 1.0, ( values.z - rt ) / ( 1.0 - rt ) ) )	// normalisaton between 0 and 1
		) + 1.0 ) * .5;
}

void vertex() {
	
	// for fragment
	local_y = max( 0.0, min( 1.0, VERTEX.y ) );
	// common variables for this animation
	float t = TIME * speed;
	// extracting amount of vertex count and variant count
	vec2 anim_info = texture( animation, vec2(.5) / animation_size ).rg;
	uint vertex_count = uint( anim_info.x );
	uint variant_count = uint( anim_info.y );
	
	// we can choose whatever variant we want
	if ( variant_id < 0 ) {
	
		// variants from random texture
		extract_random( variant, random_tex, random_size, random_transition, random_variation, TIME, random_pixel_time );
		// turning random values into valid variant IDs
		uint v0 = uint( variant.x * float( variant_count ) ) % variant_count; // avoiding id == variant_count
		uint v1 = uint( variant.y * float( variant_count ) ) % variant_count;
		// extracting offset and frame count for both variants
		uint v0_offset, v0_frames, v1_offset, v1_frames;
		float v0_fps, v1_fps;
		extract_variant( v0_offset, v0_frames, v0_fps, uint(1), v0, animation, animation_size );
		extract_variant( v1_offset, v1_frames, v1_fps, uint(1), v1, animation, animation_size );
		// extracting vertex and normal from both variants
		vec3 vert0 = vec3(0.0);
		vec3 vert1 = vec3(0.0);
		vec3 norm0, norm1;
		extract_optimised( vert0, norm0, uint(UV2.y), animation, animation_size, v0_offset, vertex_count, v0_frames, t, v0_fps );
		extract_optimised( vert1, norm1, uint(UV2.y), animation, animation_size, v1_offset, vertex_count, v1_frames, t, v1_fps );
		// and mixing them with the random transition
		VERTEX += mix( vert0, vert1, variant.z );
		NORMAL = normalize( mix( norm0, norm1, variant.z ) );
		// for fragment
		variant.x = float( v0 ) / float( variant_count );
		variant.y = float( v1 ) / float( variant_count );
	
	} else {
		
		uint v0 = max( uint(0), min( variant_count-uint(1), uint( variant_id ) ) );
		uint v0_offset, v0_frames;
		float v0_fps;
		extract_variant( v0_offset, v0_frames, v0_fps, uint(1), v0, animation, animation_size );
		extract_optimised( VERTEX, NORMAL, uint(UV2.y), animation, animation_size, v0_offset, vertex_count, v0_frames, t, v0_fps );

		variant.x = float( v0 ) / float( variant_count );
		variant.y = variant.x;
		variant.z = 0.0;
		
	}
}

void fragment() {
	ALBEDO = mix( 
		mix( 
		hsv2rgb( vec3( variant.x, 1.0, 1.0 ) ), 
		hsv2rgb( vec3( variant.y, 1.0, 1.0 ) ), 
		variant.z 
		), albedo.rgb, local_y );
	METALLIC = 0.0;
	ROUGHNESS = 1.0;
	SPECULAR = .5;
}
