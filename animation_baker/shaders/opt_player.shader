/*
# optimisation player
this shader shows how to read a texture containing a optimised animation
animation_size has to be specified manually or via script to match the exact size of the texture
shader awaits the data to be organised in this sequence:
	- first pixel contains vertex count on RED channel and frame count on GREEN channel
	- [position, normal] pairs for each vertex in each frame
*/

shader_type spatial;
render_mode blend_mix,depth_draw_opaque,cull_back,diffuse_burley,specular_schlick_ggx;
uniform vec4 albedo : hint_color;

// general params
uniform float speed = 1; 						// time multiplier
uniform float fps = 25;							// animation frame per second
// animation texture specific params
uniform sampler2D animation: hint_black;		// texture containing blendshapes
uniform vec2 animation_size = vec2(1.0);		// exact texture size

// extract vertex and normal info for a specific vertex in a specific frame
// interpolation for one frame to the other is linear
void extract_optimised( 
		inout vec3 vert, 
		inout vec3 norm,
		in int vertex_id,
		in sampler2D anim, 
		in vec2 anim_size, 
		in int pixel_offset,
		in int vertex_count,
		in int frame_count,
		in float time
		) {
	// frame indices computation
	int frame_0 = int( time );
	float y_mix = time - float( frame_0 );
	frame_0 %= frame_count;
	// next frame
	int frame_1 = ( frame_0 + 1 ) % frame_count;
	// pixel index containing vertex offset for both frames
	int pixelid0 = pixel_offset + frame_0*vertex_count*2 + vertex_id*2;
	int pixelid1 = pixel_offset + frame_1*vertex_count*2 + vertex_id*2;
	// extraction and mix of the vertex offsets
	vert += mix(
		texture( anim, vec2( 0.5 + float(pixelid0%int(anim_size.x)), 0.5 + float(pixelid0/int(anim_size.x)) ) / anim_size ).xyz,
		texture( anim, vec2( 0.5 + float(pixelid1%int(anim_size.x)), 0.5 + float(pixelid1/int(anim_size.x)) ) / anim_size ).xyz,
		y_mix
		);
	// next pixel contains the normal
	pixelid0 += 1;
	pixelid1 += 1;
	// extraction, mix and normalisation of normals
	norm = normalize( mix(
		texture( anim, vec2( 0.5 + float(pixelid0%int(anim_size.x)), 0.5 + float(pixelid0/int(anim_size.x)) ) / anim_size ).xyz,
		texture( anim, vec2( 0.5 + float(pixelid1%int(anim_size.x)), 0.5 + float(pixelid1/int(anim_size.x)) ) / anim_size ).xyz,
		y_mix
		) );
}

// extract vertex and normal info for a specific vertex in a specific frame when play backwards
// interpolation for one frame to the other is linear
void extract_optimisedi( 
		inout vec3 vert, 
		inout vec3 norm,
		in int vertex_id,
		in sampler2D anim, 
		in vec2 anim_size, 
		in int pixel_offset,
		in int vertex_count,
		in int frame_count,
		in float time
		) {
	// frame indices computation
	int frame_0 = int( time );
	float y_mix = abs( time - float( frame_0 ) );
	frame_0 %= frame_count;
	// previous frame
	int frame_1 = ( frame_0 + (frame_count-1) ) % frame_count;
	// pixel index containing vertex offset for both frames
	int pixelid0 = pixel_offset + frame_0*vertex_count*2 + vertex_id*2;
	int pixelid1 = pixel_offset + frame_1*vertex_count*2 + vertex_id*2;
	// extraction and mix of the vertex offsets
	vert += mix(
		texture( anim, vec2( 0.5 + float(pixelid0%int(anim_size.x)), 0.5 + float(pixelid0/int(anim_size.x)) ) / anim_size ).xyz,
		texture( anim, vec2( 0.5 + float(pixelid1%int(anim_size.x)), 0.5 + float(pixelid1/int(anim_size.x)) ) / anim_size ).xyz,
		y_mix
		);
	// next pixel contains the normal
	pixelid0 += 1;
	pixelid1 += 1;
	// extraction, mix and normalisation of normals
	norm = normalize( mix(
		texture( anim, vec2( 0.5 + float(pixelid0%int(anim_size.x)), 0.5 + float(pixelid0/int(anim_size.x)) ) / anim_size ).xyz,
		texture( anim, vec2( 0.5 + float(pixelid1%int(anim_size.x)), 0.5 + float(pixelid1/int(anim_size.x)) ) / anim_size ).xyz,
		y_mix
		) );
}

void vertex() {
	// same variables for all vertices
	float t = TIME * speed * fps;
	// extracting amount of vertex and frame counts
	vec2 info = texture( animation, vec2(.5) / animation_size ).rg;
	int vcount = int(info.x);
	int frames = int(info.y);
	// ready to apply animation
	if ( speed >= 0. ) {
		extract_optimised( VERTEX, NORMAL, int(UV2.y), animation, animation_size, 1, vcount, frames, t );
	} else {
		extract_optimisedi( VERTEX, NORMAL, int(UV2.y), animation, animation_size, 1, vcount, frames, t );
	}
}

void fragment() {
	ALBEDO = albedo.rgb;
	METALLIC = 0.0;
	ROUGHNESS = 1.0;
	SPECULAR = .5;
}
