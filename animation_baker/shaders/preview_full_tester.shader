shader_type spatial;
render_mode blend_mix,depth_draw_opaque,cull_back,diffuse_burley,specular_schlick_ggx;

const float M_PI = 							3.1415926535897932384626433832795;
const uint one = uint(1);

// fragment
uniform vec4 albedo : 						hint_color;
// animations
uniform float speed = 						1;
uniform float fps = 						25;
uniform sampler2D animation: 				hint_black;
uniform vec2 animation_size = 				vec2(1.0);

// control
uniform uint slice_a = 0;
uniform uint anim_a = 0;
uniform uint variant_a = 0;
uniform uint slice_b = 0;
uniform uint anim_b = 0;
uniform uint variant_b = 0;
uniform float transition : hint_range(0,1);

uniform float y_boost_min = 1.0;
uniform float y_boost_max = 2.0;
uniform float y_boost_pow = 2.0;

varying vec3 slide0_info;
varying vec3 slide1_info;
varying float local_x;
varying float local_y;

// utils
vec3 hsv2rgb(vec3 c) {
  vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
  vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
  return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

// extraction of vertex and normal info from animation texture
void animation_extract_optimised( 
	inout vec3 		eo_vert, 			// [out] vertex offset
	inout vec3 		eo_norm,			// [out] normal
	in sampler2D 	eo_data,			// animation texture
	in vec2 		eo_size, 			// anmation texture size
	in uint 		eo_pixel_offset,	// first frame position
	in uint 		eo_vcount,			// vertex count in mesh
	in uint 		eo_fcount,			// frame count in animation
	in uint 		eo_vid,				// vertex index
	in float 		eo_t0,				// time for animation with fixed fps
	in float 		eo_t1,				// time for animation with normalised time (fps = -1)
	in float 		eo_fps				// animation fps
	) {

	uint asizex = uint(eo_size.x);

	// frame indices computation
	float loc_time = eo_t0;
	if ( eo_fps < 0.0 ) {
		loc_time = eo_t1 * float( eo_fcount );
	} else {
		loc_time *= eo_fps;
	}
	uint frame_0 = uint( loc_time );
	float y_mix = loc_time - float( frame_0 );

	// current frame
	frame_0 %= eo_fcount;
	// next frame
	uint frame_1 = ( frame_0 + uint(1) ) % eo_fcount;

	// pixel index containing vertex offset for both frames
	uint px0 = eo_pixel_offset + frame_0 * eo_vcount * uint(2) + eo_vid * uint(2);
	uint px1 = eo_pixel_offset + frame_1 * eo_vcount * uint(2) + eo_vid * uint(2);

	// extraction and mix of the vertex offsets
	eo_vert += mix(
		texture( eo_data, vec2( 0.5 + float( px0 % asizex ), 0.5 + float( px0 / asizex ) ) / eo_size ).xyz,
		texture( eo_data, vec2( 0.5 + float( px1 % asizex ), 0.5 + float( px1 / asizex ) ) / eo_size ).xyz,
		y_mix
		);

	// next pixel contains the normal
	px0 += one;
	px1 += one;
	// extraction, mix and normalisation of normals
	eo_norm = normalize( mix(
		texture( eo_data, vec2( 0.5 + float( px0 % asizex ), 0.5 + float( px0 / asizex ) ) / eo_size ).xyz,
		texture( eo_data, vec2( 0.5 + float( px1 % asizex ), 0.5 + float( px1 / asizex ) ) / eo_size ).xyz,
		y_mix
		) );

}


// extraction of pixel info 
void animation_extract_info( 
	inout uint 		ei_red, 			// [out] value of red channel
	inout uint 		ei_green, 			// [out] value of green channel
	inout float 	ei_blue,			// [out] value of blue channel
	in sampler2D 	ei_data, 			// animation texture
	in vec2 		ei_size,			// anmation texture size
	in uint 		ei_pid				// pixel index
	) {
	uint asizex = uint(ei_size.x);
	vec3 slice_info = texture( 
		ei_data, 
		vec2( 
			0.5 + float( ei_pid % asizex ), 
			0.5 + float( ei_pid / asizex ) 
		) / ei_size ).xyz;
	ei_red = 	uint(slice_info.x);
	ei_green = 	uint(slice_info.y);
	ei_blue = 	slice_info.z;
}

// extraction and merge of animation's variants
void animation_extract_variant(
	inout vec3 		ea_vert,			// [out] vertex offset
	inout vec3 		ea_norm,			// [out] normal
	in sampler2D 	ea_data,			// animation texture 
	in vec2 		ea_size, 			// anmation texture size
	in uint 		ea_pixel_offset,	// first variant position
	in uint 		ea_vcount,			// vertex count in mesh
	in uint 		ea_variant,			// variant index
	in uint 		ea_vid,				// vertex index
	in float 		ea_t0,				// time for animation with fixed fps
	in float 		ea_t1,				// time for animation with normalised time (fps = -1)
	) {
	
	// working vars
	uint a_offset, a_variants, v_offset, v_frames;
	
	float v_fps, tmp;
	animation_extract_info( 
		a_offset, a_variants, // out 
		tmp, ea_data, ea_size, ea_pixel_offset );
	
	// turning random values into valid variant IDs
	uint v = min( a_variants - one, ea_variant );
	
	// extracting offset and frame count for both variants
	animation_extract_info( 
		v_offset, v_frames, v_fps, // out
		ea_data, ea_size, a_offset + v );
	
	animation_extract_optimised( 
		ea_vert, ea_norm, // out
		ea_data, ea_size, v_offset, ea_vcount, v_frames, ea_vid, ea_t0, ea_t1, v_fps );
}

void vertex() {
	
	// for fragment
	float original_y = VERTEX.y;
	local_x = max( -1.0, min( 1.0, VERTEX.x ) );
	local_y = max( 0.0, min( 1.0, VERTEX.y ) );
	// preparing vars to mix slices
	vec3 vert_s0 = vec3(0.0); 
	vec3 vert_s1 = vec3(0.0); 
	vec3 norm_s0, norm_s1;
	// preparing vars to mix animations
	vec3 vert_a0 = vec3(0.0); 
	vec3 vert_a1 = vec3(0.0); 
	vec3 norm_a0, norm_a1;
	float tmp;
	
	// common variables for this animation
	float t = TIME * speed;
	// extracting amount of vertex count and slices count
	vec2 anim_info = texture( animation, vec2(.5) / animation_size ).rg;
	uint vertex_count = uint( anim_info.x );
	uint slice_count = uint( anim_info.y );
	
	uint s0 = min( slice_count - one, slice_a );
	uint s1 = min( slice_count - one, slice_b );
	
	uint s0_offset, s0_animations, s1_offset, s1_animations;
	animation_extract_info( s0_offset, s0_animations, tmp, animation, animation_size, one + s0 );
	animation_extract_info( s1_offset, s1_animations, tmp, animation, animation_size, one + s1 );
	
	uint a0, a1;
	
	// computing the 2 animations to blend for slice 0
	a0 = min( s0_animations - one, anim_a );
	a1 = min( s1_animations - one, anim_b );
	
	// slide A, animation A
	animation_extract_variant( vert_a0, norm_a0, animation, animation_size, s0_offset + a0, vertex_count, variant_a, uint(UV2.y), t, t );
	// slide B, animation B
	animation_extract_variant( vert_a1, norm_a1, animation, animation_size, s1_offset + a1, vertex_count, variant_b, uint(UV2.y), t, t );
	
	// and pushing the final mix into vertex & normal
	if ( original_y >= y_boost_min && transition != 0.5 ) {
		float yb_mult = min( 1.0, (original_y-y_boost_min) / (y_boost_max-y_boost_min) );
		float transi = transition;
		if (transi < 0.5) {
			transi = min( 1.0, max( 0.0, pow(transi * 2.0, y_boost_pow) * 0.5 ) );
		} else {
			transi = min( 1.0, max( 0.0, 1.0 - pow( (transi-1.0) * -2.0, y_boost_pow) * 0.5 ) );
		}
		VERTEX += mix( vert_a0, vert_a1, mix( transition, transi, yb_mult ) );
	} else { 
		VERTEX += mix( vert_a0, vert_a1, transition );
	}
	NORMAL = normalize( mix( norm_a0, norm_a1, transition ) );
	
	// for fragment
	uint a_off, a_count;
	uint v_id, v_off, v_count; float v_fps;
	
	slide0_info.x = float(s0) / float(slice_count);
	slide0_info.y = float(a0) / float(s0_animations);
	animation_extract_info( a_off, a_count, tmp, animation, animation_size, s0_offset + a0 );
	v_id = min( a_count - one, variant_a );
	animation_extract_info( v_off, v_count, v_fps, animation, animation_size, a_off + v_id );
	slide0_info.z = float(v_id) / float(v_count);
	
	slide1_info.x = float(s1) / float(slice_count);
	slide1_info.y = float(a1) / float(s1_animations);
	animation_extract_info( a_off, a_count, tmp, animation, animation_size, s1_offset + a0 );
	v_id = min( a_count - one, variant_b );
	animation_extract_info( v_off, v_count, v_fps, animation, animation_size, a_off + v_id );
	slide1_info.z = float(v_id) / float(v_count);
	
}

void fragment() {
	vec3 c = albedo.rgb;
	if ( local_x < 0.0 ) {
		if ( local_y < 0.25 ) {
			c = hsv2rgb( vec3( slide0_info.x, 1.0, 1.0 ) );
		} else if ( local_y < 0.5 ) {
			c = hsv2rgb( vec3( slide0_info.y, 1.0, 1.0 ) );
		} else if ( local_y < 0.75 ) {
			c = hsv2rgb( vec3( slide0_info.z, 1.0, 1.0 ) );
		}
	} else {
		if ( local_y < 0.25 ) {
			c = hsv2rgb( vec3( slide1_info.x, 1.0, 1.0 ) );
		} else if ( local_y < 0.5 ) {
			c = hsv2rgb( vec3( slide1_info.y, 1.0, 1.0 ) );
		} else if ( local_y < 0.75 ) {
			c = hsv2rgb( vec3( slide1_info.z, 1.0, 1.0 ) );
		}
	}
	ALBEDO = c;
	METALLIC = 0.0;
	ROUGHNESS = 1.0;
	SPECULAR = .5;
}
