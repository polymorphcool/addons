shader_type spatial;
render_mode blend_mix,depth_draw_opaque,cull_back,diffuse_burley,specular_schlick_ggx;

const float M_PI = 								3.1415926535897932384626433832795;
// fragment
uniform vec4 albedo : 							hint_color;
// random
uniform sampler2D random_tex : 					hint_black;
uniform float random_pixel_time = 				1;
uniform vec2 random_size = 						vec2(256);
uniform float random_transition = 				0.5;
uniform float random_variation = 				0.5;
// animations
uniform int variant_id =						-1;
uniform float speed = 							1;
uniform float fps = 							25;
uniform sampler2D animation: 					hint_black;
uniform vec2 animation_size = 					vec2(1.0);
uniform bool loop_end = 						true;

varying float local_y;

// utils
vec3 hsv2rgb(vec3 c) {
  vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
  vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
  return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

// extraction of vertex and normal info from animation texture
void extract_optimised( 
		inout vec3 vert, 
		inout vec3 norm,
		in uint vertex_id,
		in sampler2D anim, 
		in vec2 anim_size, 
		in uint pixel_offset,
		in uint vertex_count,
		in uint frame_count,
		in float time,
		in float framerate
		) {
	uint asizex = uint(anim_size.x);
	// frame indices computation
	float loc_time = time;
	if ( framerate < 0.0 ) {
		loc_time *= float(frame_count);
	} else {
		loc_time *= framerate;
	}
	uint frame_0 = uint( loc_time );
	float y_mix = loc_time - float( frame_0 );

	frame_0 %= frame_count;
	// next frame
	uint frame_1 = frame_0;
	if ( loop_end ) {
		frame_1 = ( frame_0 + uint(1) ) % frame_count;
	} else {
		frame_1 = min( frame_count - uint(1), frame_0 + uint(1) );
	}
	// pixel index containing vertex offset for both frames
	uint pixelid0 = pixel_offset + frame_0*vertex_count*uint(2) + vertex_id*uint(2);
	uint pixelid1 = pixel_offset + frame_1*vertex_count*uint(2) + vertex_id*uint(2);
	// extraction and mix of the vertex offsets
	vert += mix(
		texture( anim, vec2( 0.5 + float(pixelid0%asizex), 0.5 + float(pixelid0/asizex) ) / anim_size ).xyz,
		texture( anim, vec2( 0.5 + float(pixelid1%asizex), 0.5 + float(pixelid1/asizex) ) / anim_size ).xyz,
		y_mix
		);
	// next pixel contains the normal
	pixelid0 += uint(1);
	pixelid1 += uint(1);
	// extraction, mix and normalisation of normals
	norm = normalize( mix(
		texture( anim, vec2( 0.5 + float(pixelid0%asizex), 0.5 + float(pixelid0/asizex) ) / anim_size ).xyz,
		texture( anim, vec2( 0.5 + float(pixelid1%asizex), 0.5 + float(pixelid1/asizex) ) / anim_size ).xyz,
		y_mix
		) );
}

// extraction of pixel offset and count of an animation
void extract_info( 
	inout uint offset, 
	inout uint count,
	in uint start,
	in uint sID,
	in sampler2D anim, 
	in vec2 anim_size,
	) {
	uint slice_pixel = start + sID;
	vec2 slice_info = texture( 
		anim, 
		vec2( 
			0.5 + float(slice_pixel%uint(anim_size.x)), 
			0.5 + float(slice_pixel/uint(anim_size.x)) 
		) / anim_size ).rg;
	offset = uint(slice_info.x);
	count = uint(slice_info.y);
}

void vertex() {
	// for fragment
	local_y = max( 0.0, min( 1.0, VERTEX.y ) );
	// common variables for this animation
	float t = TIME * speed;
	uint vertex_count, v_frames;
	extract_info( vertex_count, v_frames, uint(0), uint(0), animation, animation_size );
	extract_optimised( VERTEX, NORMAL, uint(UV2.y), animation, animation_size, uint(1), vertex_count, v_frames, t, fps );
}

void fragment() {
	ALBEDO = albedo.rgb;
	METALLIC = 0.0;
	ROUGHNESS = 1.0;
	SPECULAR = .5;
}
