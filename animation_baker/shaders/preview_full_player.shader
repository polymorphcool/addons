shader_type spatial;
render_mode blend_mix,depth_draw_opaque,cull_back,diffuse_burley,specular_schlick_ggx;

const float M_PI = 							3.141592653589793238462643383279;
const float M_TAU = 						6.283185307179586476925286766558;
const uint one = uint(1);

// fragment
uniform vec4 albedo : 						hint_color;

// random
uniform sampler2D random_tex : 				hint_black;
uniform float random_pixel_time = 			1;
uniform vec2 random_size = 					vec2(256);
uniform float random_transition = 			0.5;
uniform float random_variation = 			0.5;

// animations
uniform float motion_length = 				0;
uniform float motion_degree :				hint_range(0,360);
uniform float speed = 						1;
uniform float fps = 						25;
uniform sampler2D animation: 				hint_black;
uniform vec2 animation_size = 				vec2(1.0);

// internal
varying vec3 variant;
varying vec3 slide0_info;
varying vec3 slide1_info;
varying float local_x;
varying float local_y;

// utils
vec3 hsv2rgb(in vec3 c) {
  vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
  vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
  return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

// extraction of values from a repeated random texture
void extract_random( 
	inout vec3 values, 
	in sampler2D tex, 
	in vec2 t_size, 
	in float r_transition, 
	in float r_variation, 
	in float global_t, 
	in float pixel_t 
	) {
	// texture pixel count
	int tw = int(t_size.x);
	int tt = tw * int(t_size.y);
	// local times
	float t0 = global_t / pixel_t;
	values.z = t0 - float( int(t0) );
	// pixel ids
	int pid0 = int(t0) % tt;
	int pid1 = int(t0+1.0) % tt;
	int pid2 = int(t0+t_size.x) % tt;
	// exctracting RED values out of texture
	values.x = texture( tex, vec2( float(pid0%tw), float(pid0/tw) ) / t_size).r;
	values.y = texture( tex, vec2( float(pid1%tw), float(pid1/tw) ) / t_size).r;
	float tv = 0.5 - texture( tex, vec2( float(pid2%tw), float(pid2/tw) ) / t_size).r;
	// mapping transition
	float rt = max( 0.0, min( 0.999,  								// avoiding division by 0!
		r_transition + tv * r_variation 
		) );
	values.z = 
		( sin( M_PI*-.5 + M_PI *									// sinus interpolation
		max( 0.0, min( 1.0, ( values.z - rt ) / ( 1.0 - rt ) ) )	// normalisaton between 0 and 1
		) + 1.0 ) * .5;
}

// extraction of vertex and normal info from animation texture
void animation_extract_optimised( 
	inout vec3 		eo_vert, 			// [out] vertex offset
	inout vec3 		eo_norm,			// [out] normal
	in sampler2D 	eo_data,			// animation texture
	in vec2 		eo_size, 			// anmation texture size
	in uint 		eo_pixel_offset,	// first frame position
	in uint 		eo_vcount,			// vertex count in mesh
	in uint 		eo_fcount,			// frame count in animation
	in uint 		eo_vid,				// vertex index
	in float 		eo_t0,				// time for animation with fixed fps
	in float 		eo_t1,				// time for animation with normalised time (fps = -1)
	in float 		eo_fps				// animation fps
	) {

	uint asizex = uint(eo_size.x);

	// frame indices computation
	float loc_time = eo_t0;
	if ( eo_fps < 0.0 ) {
		loc_time = eo_t1 * float( eo_fcount );
	} else {
		loc_time *= eo_fps;
	}
	uint frame_0 = uint( loc_time );
	float y_mix = loc_time - float( frame_0 );

	// current frame
	frame_0 %= eo_fcount;
	// next frame
	uint frame_1 = ( frame_0 + uint(1) ) % eo_fcount;

	// pixel index containing vertex offset for both frames
	uint px0 = eo_pixel_offset + frame_0 * eo_vcount * uint(2) + eo_vid * uint(2);
	uint px1 = eo_pixel_offset + frame_1 * eo_vcount * uint(2) + eo_vid * uint(2);

	// extraction and mix of the vertex offsets
	eo_vert += mix(
		texture( eo_data, vec2( 0.5 + float( px0 % asizex ), 0.5 + float( px0 / asizex ) ) / eo_size ).xyz,
		texture( eo_data, vec2( 0.5 + float( px1 % asizex ), 0.5 + float( px1 / asizex ) ) / eo_size ).xyz,
		y_mix
		);

	// next pixel contains the normal
	px0 += one;
	px1 += one;
	// extraction, mix and normalisation of normals
	eo_norm = normalize( mix(
		texture( eo_data, vec2( 0.5 + float( px0 % asizex ), 0.5 + float( px0 / asizex ) ) / eo_size ).xyz,
		texture( eo_data, vec2( 0.5 + float( px1 % asizex ), 0.5 + float( px1 / asizex ) ) / eo_size ).xyz,
		y_mix
		) );

}

// extraction of pixel info 
void animation_extract_info( 
	inout uint 		ei_red, 			// [out] value of red channel
	inout uint 		ei_green, 			// [out] value of green channel
	inout float 	ei_blue,			// [out] value of blue channel
	in sampler2D 	ei_data, 			// animation texture
	in vec2 		ei_size,			// anmation texture size
	in uint 		ei_pid				// pixel index
	) {
	uint asizex = uint(ei_size.x);
	vec3 slice_info = texture( 
		ei_data, 
		vec2( 
			0.5 + float( ei_pid % asizex ), 
			0.5 + float( ei_pid / asizex ) 
		) / ei_size ).xyz;
	ei_red = 	uint(slice_info.x);
	ei_green = 	uint(slice_info.y);
	ei_blue = 	slice_info.z;
}

// extraction and merge of slice's animation
void animation_extract(
	inout vec3 		ea_vert, 			// [out] vertex offset
	inout vec3 		ea_norm,			// [out] normal
	in sampler2D 	ea_data,			// animation texture
	in vec2 		ea_size, 			// anmation texture size
	in uint 		ea_pixel_offset,	// first animation position
	in uint 		ea_vcount,			// vertex count in mesh
	in uint 		ea_aid,				// animation index
	in uint 		ea_vid,				// vertex index
	in vec3 		ea_variant,			// data	to control animation variants
	in float 		ea_t0,				// time for animation with fixed fps
	in float 		ea_t1,				// time for animation with normalised time (fps = -1)
	) {

	// working vars
	uint a_offset, a_variants, v, v_offset, v_frames;
	float tmp, v_fps;
	vec3 vert_v0 = vec3(0.0); 
	vec3 vert_v1 = vec3(0.0);
	vec3 norm_v0, norm_v1;

	// extracting animation info
	animation_extract_info( 
		a_offset, a_variants, tmp, // out
		ea_data, ea_size, ea_pixel_offset+ea_aid );

	//#1 extracting data for first variant
	v = uint( ea_variant.x * float( a_variants ) ) % a_variants; // avoiding id == variant_count
	// variant info
	animation_extract_info( 
		v_offset, v_frames, v_fps, // out 
		ea_data, ea_size, a_offset + v );
	// variant data
	animation_extract_optimised( 
		vert_v0, norm_v0, // out 
		ea_data, ea_size, v_offset, ea_vcount, v_frames, ea_vid, ea_t0, ea_t1, v_fps );

	//#2 extracting data for second variant
	v = uint( ea_variant.y * float( a_variants ) ) % a_variants; // avoiding id == variant_count
	// variant info
	animation_extract_info( 
		v_offset, v_frames, v_fps, // out 
		ea_data, ea_size, a_offset + v );
	// variant data
	animation_extract_optimised( 
		vert_v1, norm_v1, // out 
		ea_data, ea_size, v_offset, ea_vcount, v_frames, ea_vid, ea_t0, ea_t1, v_fps );

	ea_vert = mix( vert_v0, vert_v1, ea_variant.z );
	ea_norm = normalize( mix( norm_v0, norm_v1, ea_variant.z ) );

}

void animate(
	inout vec3 		an_vert, 			// [out] vertex offset
	inout vec3 		an_norm,			// [out] normal
	in sampler2D 	an_data,			// animation texture
	in vec2 		an_size, 			// anmation texture size
	in uint 		an_vid,				// vertex index
	in float 		an_radius,			// determines wich slices are used
	in float 		an_degree,			// detremines wich animations are used
	in vec3 		an_variant,			// data	to control animation variants
	in float 		an_t0,				// time for animation with fixed fps
	in float 		an_t1				// time for animation with normalised time (fps = -1)
	) {
	
	// extracting amount of vertex count and slices count
	vec2 anim_info = texture( an_data, vec2(.5) / an_size ).rg;
	uint vertex_count = uint( anim_info.x );
	uint slice_count = uint( anim_info.y );
	uint slice_max = slice_count - one;
	
	// locate slices
	float mol = max( 0.0, min( float(slice_max), an_radius ) );
	uint s0 = uint(mol);
	uint s1 = min( slice_max, s0 + one );
	float smix = mol - float(s0);
	if ( smix == 0.0 ) {
		s1 = s0;
	} else if ( smix == 1.0 ) {
		s0 = s1;
	}
	// preparing vars to mix slices
	uint px_offset, anim_count, a0, a1;
	vec3 vert_a0 = vec3(0.0); 
	vec3 vert_a1 = vec3(0.0); 
	vec3 vert_s0 = vec3(0.0); 
	vec3 vert_s1 = vec3(0.0); 
	vec3 norm_a0, norm_a1, norm_s0, norm_s1;
	float mor = mod( an_degree, 360.0 );
	float tmp, gap, amix;
	
	// prefectly on a slice
	if ( s0 == s1 ) {
		
		// getting animation from one slice
		animation_extract_info( 
			px_offset, anim_count, tmp, // out
			an_data, an_size, one + s0 );
		
		gap = 360.0 / float( anim_count );
		a0 = uint( mor / gap ) % anim_count;
		a1 = ( a0 + one ) % anim_count;
		amix = ( mor - float( a0 ) * gap ) / gap;

		animation_extract( 
			vert_a0, norm_a0, // out
			an_data, an_size,
			px_offset, vertex_count, a0, an_vid, an_variant, an_t0, an_t1 );
		
		animation_extract( 
			vert_a1, norm_a1, // out
			an_data, an_size,
			px_offset, vertex_count, a1, an_vid, an_variant, an_t0, an_t1 );
		
		an_vert = mix( vert_a0, vert_a1, amix );
		an_norm = normalize( mix( norm_a0, norm_a1, amix ) );
		
	} else {
		
		//#1 getting animation from first slice
		animation_extract_info( 
			px_offset, anim_count, tmp, // out
			an_data, an_size, one + s0 );
		
		gap = 360.0 / float( anim_count );
		a0 = uint( mor / gap ) % anim_count;
		a1 = ( a0 + one ) % anim_count;
		amix = ( mor - float(a0) * gap ) / gap;

		animation_extract( 
			vert_a0, norm_a0, // out
			an_data, an_size,
			px_offset, vertex_count, a0, an_vid, an_variant, an_t0, an_t1 );
		
		animation_extract( 
			vert_a1, norm_a1, // out
			an_data, an_size,
			px_offset, vertex_count, a1, an_vid, an_variant, an_t0, an_t1 );
		
		vert_s0 = mix( vert_a0, vert_a1, amix );
		norm_s0 = mix( norm_a0, norm_a1, amix );
		
		//#2 getting animation from second slice
		animation_extract_info( 
			px_offset, anim_count, tmp, // out
			an_data, an_size, one + s1 );
		
		gap = 360.0 / float( anim_count );
		a0 = uint( mor / gap ) % anim_count;
		a1 = ( a0 + one ) % anim_count;
		amix = ( mor - float(a0) * gap ) / gap;

		animation_extract( 
			vert_a0, norm_a0, // out
			an_data, an_size,
			px_offset, vertex_count, a0, an_vid, an_variant, an_t0, an_t1 );
		
		animation_extract( 
			vert_a1, norm_a1, // out
			an_data, an_size,
			px_offset, vertex_count, a1, an_vid, an_variant, an_t0, an_t1 );
		
		vert_s1 = mix( vert_a0, vert_a1, amix );
		norm_s1 = mix( norm_a0, norm_a1, amix );

		an_vert = mix( vert_s0, vert_s1, smix );
		an_norm = normalize( mix( norm_s0, norm_s1, smix ) );
		
	}
}

void animate_info(
	inout vec3 		an_info0, 			// [out] vertex offset
	inout vec3 		an_info1,			// [out] normal
	in sampler2D 	an_data,			// animation texture
	in vec2 		an_size, 			// anmation texture size
	in uint 		an_vid,				// vertex index
	in float 		an_radius,			// determines wich slices are used
	in float 		an_degree,			// detremines wich animations are used
	in float 		an_t0,				// time for animation with fixed fps
	in float 		an_t1				// time for animation with normalised time (fps = -1)
	) {
	
	// extracting amount of vertex count and slices count
	vec2 anim_info = texture( an_data, vec2(.5) / an_size ).rg;
	uint vertex_count = uint( anim_info.x );
	uint slice_count = uint( anim_info.y );
	uint slice_max = slice_count - one;
	
	// locate slices
	float mol = max( 0.0, min( float(slice_max), an_radius ) );
	uint s0 = uint(mol);
	uint s1 = min( slice_max, s0 + one );
	float smix = mol - float(s0);
	if ( smix == 0.0 ) {
		s1 = s0;
	} else if ( smix == 1.0 ) {
		s0 = s1;
	}
	// preparing vars to mix slices
	uint px_offset, anim_count, a0;
	float mor = mod( an_degree, 360.0 );
	float tmp, gap, amix;
	
	// prefectly on a slice
	if ( s0 == s1 ) {
		
		// getting animation from one slice
		animation_extract_info( 
			px_offset, anim_count, tmp, // out
			an_data, an_size, one + s0 );
		
		gap = 360.0 / float( anim_count );
		a0 = uint( mor / gap ) % anim_count;
		an_info0.x = float(a0) / float(anim_count);
		an_info0.y = float( ( a0 + one ) % anim_count ) / float(anim_count);
		an_info0.z = ( mor - float( a0 ) * gap ) / gap;
		// no info for slide0
		an_info1.z = -1.0;
		
	} else {
		
		//#1 getting animation from first slice
		animation_extract_info( 
			px_offset, anim_count, tmp, // out
			an_data, an_size, one + s0 );
		
		gap = 360.0 / float( anim_count );
		a0 = uint( mor / gap ) % anim_count;
		an_info0.x = float(a0) / float(anim_count);
		an_info0.y = float( ( a0 + one ) % anim_count ) / float(anim_count);
		an_info0.z = ( mor - float(a0) * gap ) / gap;
		
		//#2 getting animation from second slice
		animation_extract_info( 
			px_offset, anim_count, tmp, // out
			an_data, an_size, one + s1 );
		gap = 360.0 / float( anim_count );
		a0 = uint( mor / gap ) % anim_count;
		an_info1.x = float(a0) / float(anim_count);
		an_info1.y = float( ( a0 + one ) % anim_count ) / float(anim_count);
		an_info1.z = ( mor - float(a0) * gap ) / gap;
		
	}
}

void vertex() {
	
	// for fragment
	local_x = VERTEX.x;
	local_y = VERTEX.y * 1.5;
	
	// loading variants from random texture
	extract_random( variant, random_tex, random_size, random_transition, random_variation, TIME, random_pixel_time );
	// common variables for this animation
	float t = TIME * speed;
	vec3 v,n;
	
	animate( v, n, animation, animation_size, uint(UV2.y), motion_length, motion_degree, variant, t, t );
	animate( v, n, animation, animation_size, uint(UV2.y), motion_length, motion_degree, variant, t, t );
	
	animate_info( slide0_info, slide1_info, animation, animation_size, uint(UV2.y), motion_length, motion_degree, t, t );
	VERTEX += v;
	NORMAL = normalize( n );
}

void fragment() {
	if ( local_x < 0.0 && local_y < 1.0 ) {
		if ( local_y > slide0_info.z ) {
			ALBEDO = hsv2rgb( vec3( slide0_info.x, 1.0, 1.0 ) );
		} else {
			ALBEDO = hsv2rgb( vec3( slide0_info.y, 1.0, 1.0 ) );
		}
	} else if ( local_x > 0.0 && local_y < 1.0 && slide1_info.z > 0.0 ) {
		if ( local_y > slide1_info.z ) {
			ALBEDO = hsv2rgb( vec3( slide1_info.x, 1.0, 1.0 ) );
		} else {
			ALBEDO = hsv2rgb( vec3( slide1_info.y, 1.0, 1.0 ) );
		}
	} else {
		ALBEDO = albedo.xyz;
	}
	METALLIC = 0.0;
	ROUGHNESS = 1.0;
	SPECULAR = .5;
}
