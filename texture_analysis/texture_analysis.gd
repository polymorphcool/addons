tool

extends Sprite

const float_precision:int = 512

enum etype {
	HISTOGRAM
}

enum escale {
	LINEAR,
	POW2
}

export(etype) var type:int = etype.HISTOGRAM
export(escale) var scaling:int = escale.LINEAR
export var analyse:bool = false setget set_analyse
export var clear:bool = false setget set_clear

func set_analyse(b:bool) -> void:
	analyse = false
	if b:
		initialised = false

func set_clear(b:bool,clear_tex:bool = true) -> void:
	clear = false
	if b:
		if clear_tex:
			texture = null
		while get_child_count() > 0:
			remove_child(get_child(0))

var initialised:bool = true
var channels:int = 0
var precision:int = 0

func get_channels() -> void:
	
	var iformat:int = -1
	
	if texture is NoiseTexture:
		iformat = texture.get_data().get_format()
	elif texture is ImageTexture:
		iformat = texture.get_format()
	
	match iformat:
		Image.FORMAT_L8:
			channels = 1
			precision = 256
		Image.FORMAT_R8:
			channels = 1
			precision = 256
		Image.FORMAT_RG8:
			channels = 2
			precision = 256
		Image.FORMAT_RGB8:
			channels = 3
			precision = 256
		Image.FORMAT_RGBA8:
			channels = 4
			precision = 256
		Image.FORMAT_RF:
			channels = 1
			precision = float_precision
		Image.FORMAT_RGF:
			channels = 2
			precision = float_precision
		Image.FORMAT_RGBF:
			channels = 3
			precision = float_precision
		Image.FORMAT_RGBAF:
			channels = 4
			precision = float_precision
		_:
			channels = 0
			precision = 0

func _process(delta):
	
	if !initialised:
	
		initialised = true
		
		set_clear(true,false)
		
		if texture == null:
			return
	
		#getting channels
		get_channels()
		if channels == 0:
			printerr("unsupported color format")
			return
		
		var im:Image = texture.get_data()
		var isi:Vector2 = im.get_size()
	
		#preparing data
		var data = []
		for c in range(0,channels):
			var channel:Array = []
			channel.resize( isi.x*isi.y )
			data.append(channel)
		
		#loading pixels
		im.lock()
		for y in range(0,isi.y):
			for x in range(0,isi.x):
				var id:int = x + y * isi.x
				var col:Color = im.get_pixel(x,y)
				for c in range(0,channels):
					match c:
						0:
							data[0][id] = col.r
						1:
							data[1][id] = col.g
						2:
							data[2][id] = col.b
						3:
							data[3][id] = col.a
		im.unlock()
		
		# data loaded, time to analyse
		match type:
			etype.HISTOGRAM:
				histogram( data )

func histogram( data:Array ) -> void:
	
	var hdata:Array = []
	var hmax:Array = []
	var channel:Array = []
	for i in range(0,precision):
		channel.append(0.0)
	for c in range(0,channels):
		hdata.append(channel.duplicate())
		hmax.append(0)
	
	var num:int = data[0].size()
	for c in range(0,channels):
		for i in range(0,num):
			var id:int = int(data[c][i]*(precision-1))
			hdata[c][id] += 1
			if hmax[c] < hdata[c][id]:
				hmax[c] = hdata[c][id]
	
	# render
	var base:Node2D = Node2D.new()
	var tmpl:Line2D = Line2D.new()
	tmpl.default_color = Color.white
	tmpl.width = 1
	add_child(base)
	base.position = Vector2( texture.get_width() + 20, 0 )
	for c in range(0,channels):
		var border:Line2D = tmpl.duplicate()
		border.default_color = Color.black
		border.add_point(Vector2(0,0))
		border.add_point(Vector2(precision,0))
		border.add_point(Vector2(precision,200))
		border.add_point(Vector2(0,200))
		border.add_point(Vector2(0,0))
		base.add_child(border)
		border.position = Vector2( c*(precision+20), 20 )
		for i in range(0,precision):
			var l:Line2D = tmpl.duplicate()
			if channels != 1:
				match c:
					0:
						l.default_color = Color.red
					1:
						l.default_color = Color.green
					2:
						l.default_color = Color.blue
			base.add_child(l)
			l.position = Vector2( c*(precision+20) + i, 220 )
			l.add_point(Vector2(0,0))
			var y:float = hdata[c][i] / hmax[c]
			match scaling:
				escale.POW2:
					y = 1.0 - y
					y = 1.0 - pow(y,2)
			l.add_point(Vector2(0,-y*200))
	
	print( 'image: ', num )
	for c in range(0,channels):
		print(c, ' max: ', hmax[c])
