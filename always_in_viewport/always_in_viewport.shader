shader_type spatial;
render_mode blend_mix,depth_draw_opaque,cull_back,diffuse_burley,specular_schlick_ggx;
uniform vec4 albedo : hint_color;
uniform sampler2D texture_albedo : hint_albedo;
uniform float specular;
uniform float metallic;
uniform float roughness : hint_range(0,1);
uniform float point_size : hint_range(0,128);
uniform vec3 uv1_scale;
uniform vec3 uv1_offset;

uniform mat4 vertex_correction;

varying float vheight;

void vertex() {
	vheight = max( 0.0, min(1.0, (1.0+VERTEX.z*2.0)*0.5 ));
	VERTEX = (vertex_correction*vec4(VERTEX,1.0)).xyz;
	UV=UV*uv1_scale.xy+uv1_offset.xy;
}

void fragment() {
	vec2 base_uv = UV;
	vec4 albedo_tex = texture(texture_albedo,base_uv);
	ALBEDO = mix( albedo.rgb, vec3(1.0-albedo.r,1.0-albedo.g,1.0-albedo.b), vheight) * albedo_tex.rgb;
	METALLIC = metallic;
	ROUGHNESS = roughness;
	SPECULAR = specular;
}
