extends MeshInstance

export(bool) var fixed_children:bool = true
export(bool) var fixed_mesh:bool = true

var initial_transforms:Dictionary = {}
onready var initial_transform:Transform = global_transform

func _ready():
	for c in get_children():
		initial_transforms[c] = c.global_transform

func _notification(what):
	if what == NOTIFICATION_TRANSFORM_CHANGED:
		if fixed_children:
			for c in get_children():
				if not c in initial_transforms:
					continue
				c.global_transform = initial_transforms[c]
		var mat:ShaderMaterial = material_override
		if fixed_mesh and mat != null:
			var corr_mat:Transform = global_transform.inverse() * initial_transform
			mat.set_shader_param( "vertex_correction", corr_mat )
